/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.morph.kiosk;

import com.morph.kiosk.persistence.entity.KioskUser;
import com.morph.kiosk.persistence.entity.portal.PortalPage;
import com.morph.kiosk.persistence.service.PortalPersistence;
import com.morph.kiosk.persistence.service.UserPersistence;
import com.morph.util.ProjectConstants;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import org.omnifaces.util.Faces;

/**
 *
 * @author Tobi-Morph-PC
 */
@Named(value = "portalPermissionUtil")
@SessionScoped
public class PortalPermissionUtil extends BaseController implements Serializable {

    @Resource(lookup = "java:global/KioskEJBModule-1.2/UserPersistence!com.morph.kiosk.persistence.service.UserPersistence")
    private UserPersistence userPersistence;

    @Resource(lookup = "java:global/KioskEJBModule-1.2/PortalPersistence!com.morph.kiosk.persistence.service.PortalPersistence")
    private PortalPersistence portalPersistence;

    final String contextPath = "/manager/";

    private KioskUser user;

    private List<PortalPage> pages = new ArrayList<>();
    
    public PortalPermissionUtil() {
    }

    @Override
    void initUser(KioskUser user) {
        this.user = user;
    }
    
    private boolean hasPermission(String permission) {
        //get user viewid and parse it with the permission to generate the complete query
        //to optimize the code, get the permission from the user user session Faces.getSessionAttribute
        //System.out.println("Has Permissions URI :::: " + Faces.getRequestURI());
        String permissionUrl = Faces.getRequestURI().replace(contextPath, "");

        permissionUrl = permissionUrl.startsWith("/", 0) ? (permissionUrl.substring(1).replaceAll("/", "_")
                .replace(ProjectConstants.VIEWID_EXT, "")
                .concat(":" + permission)) : (permissionUrl.replaceAll("/", "_").replace(ProjectConstants.VIEWID_EXT, "")
                .concat(":" + permission));

        System.out.println("Constructed Permission ::::::::: " + permissionUrl);
        //PortalPermission pp = portalPersistence.getUserPagePermissionWithPageViewId(permissionUrl);
        List<String> permissions = Faces.getSessionAttribute(ProjectConstants.SESSION_PERMISSION_PARAM);
        System.out.println("Has Permissions ::: " + permissions);
        System.out.println("Has Permissions Return :::: " + (!permissions.contains(permissionUrl)));
        return (!permissions.contains(permissionUrl));
        //return SecurityUtils.getSubject().isPermitted(userBean.getModuleName() + "_" + userBean.getTargetPage() + ":" + permission);
    }

    public boolean canCreate() {
        return hasPermission(ProjectConstants.PERMISSION_CREATE);
    }

    public boolean canRead() {
        return hasPermission(ProjectConstants.PERMISSION_READ);
    }

    public boolean canUpdate() {
        return hasPermission(ProjectConstants.PERMISSION_UPDATE);
    }

    public boolean canDelete() {
        return hasPermission(ProjectConstants.PERMISSION_DELETE);
    }

    public KioskUser getUser() {
        return user;
    }

    public void setUser(KioskUser user) {
        this.user = user;
    }

    public List<PortalPage> getPages() {
        return pages;
    }

    public void setPages(List<PortalPage> pages) {
        this.pages = pages;
    }
    
    
    
}
