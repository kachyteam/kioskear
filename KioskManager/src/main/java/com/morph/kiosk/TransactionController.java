/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.morph.kiosk;

import com.lowagie.text.BadElementException;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.morph.kiosk.persistence.entity.Business;
import com.morph.kiosk.persistence.entity.Kiosk;
import com.morph.kiosk.persistence.entity.KioskUser;
import com.morph.kiosk.persistence.entity.order.Cart;
import com.morph.kiosk.persistence.entity.order.KioskDeliveryOption;
import com.morph.kiosk.persistence.entity.order.KioskPaymentOption;
import com.morph.kiosk.persistence.entity.order.OrderedItem;
import com.morph.kiosk.persistence.entity.order.OrderedItemStatusLog;
import com.morph.kiosk.persistence.entity.order.SubItemCart;
import com.morph.kiosk.persistence.entity.portal.BusinessStaff;
import com.morph.kiosk.persistence.entity.portal.BusinessStaffRole;
import com.morph.kiosk.persistence.service.PortalPersistence;
import com.morph.kiosk.persistence.service.UserPersistence;
import com.morph.kiosk.util.JsfUtil;
import com.morph.util.ProjectConstants;
import java.awt.Color;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.annotation.Resource;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;

/**
 *
 * @author Tobi-Morph-PC
 */
@Named(value = "transactionController")
@ViewScoped
public class TransactionController extends BaseController {

    @Resource(lookup = "java:global/KioskEJBModule-1.2/UserPersistence!com.morph.kiosk.persistence.service.UserPersistence")
    private UserPersistence userPersistence;

    @Resource(lookup = "java:global/KioskEJBModule-1.2/PortalPersistence!com.morph.kiosk.persistence.service.PortalPersistence")
    private PortalPersistence portalPersistence;

    private List<OrderedItem> orderedItems = new ArrayList<>();
    private List<OrderedItemStatusLog> orderItemLogs = new ArrayList<>();
    private OrderedItem orderedItem;

    private OrderedItem.Status status;
    private KioskDeliveryOption.Option deliveryOption;
    private List<KioskPaymentOption.Option> paymentModes = new ArrayList<>();
    private KioskPaymentOption.Option paymentMode;
    private KioskPaymentOption.Option paymentMethod;

    private List<String> filterStatus = new ArrayList<>();

    private KioskUser user;
    private KioskUser dispatcher;

    private List<Cart> carts = new ArrayList<>();
    private List<SubItemCart> subItemCarts = new ArrayList<>();
    private List<OrderedItem> filteredOrderedItem;

    private Business business;
    private Kiosk kiosk;
    private List<Business> businesses = new ArrayList<>();
    private List<Kiosk> kiosks = new ArrayList<>();
    private List<OrderedItem.Status> orderStatus = new ArrayList<>();
    private List<KioskDeliveryOption.Option> deliveryOptions = new ArrayList<>();
    private Date tranxDate1;
    private Date tranxDate2;
    private String orderId;
    private Integer reloadTime;
    private Integer pendingOrderCount = 0;
    private Integer receivedOrderCount = 0;
    private Integer processedOrderCount = 0;
    private Integer shippedOrderCount = 0;

    private String exportTitle;

    public TransactionController() {
    }

    @Override
    void initUser(KioskUser user) {
        this.user = user;
    }

    public void editOrderStatus(OrderedItem orderedItem) {
        this.orderedItem = orderedItem;
    }

    public void saveOrderStatusUpdate() {
        try {
            this.orderedItem.setOrderStatus(status);
            this.orderedItem.setLastModifiedDate(new Date());
            this.orderedItem.setLastModifiedBy(user);
            if (paymentMethod != null) {
                this.orderedItem.setPaymentMethod(paymentMethod);
            }
            portalPersistence.update(this.orderedItem);
            logOrderedItemStatusUpdate(this.orderedItem);
            JsfUtil.addInfoFacesMessage(ProjectConstants.OPERATION_SUCCESSFUL);
        } catch (Exception e) {
            e.getMessage();
            JsfUtil.addErrorFacesMessage(ProjectConstants.OPERATION_UNSUCCESSFUL);
        }
    }

    public List<OrderedItem.Status> getOrderedItemOption() {
        return Arrays.asList(OrderedItem.Status.values());
    }

    private void logOrderedItemStatusUpdate(OrderedItem item) {
        OrderedItemStatusLog log = new OrderedItemStatusLog();
        log.setItem(item);
        log.setCreatedDate(new Date());
        log.setLastModifiedDate(new Date());
        log.setOrderStatus(status);
        log.setUpdatedby(user);
        log.setKiosk(item.getKiosk());
        if (dispatcher != null) {
            log.setNextUpdateBy(dispatcher);
        }
        portalPersistence.create(log);
    }

    public List<OrderedItemStatusLog> getOrderedItemStatusLog(Long itemId) {
        if (null != itemId) {
            return portalPersistence.getOrderedItemStatusLogByItemId(itemId);
        } else {
            return Collections.EMPTY_LIST;
        }
    }

    public void populateKiosk(String role) {
        kiosks.clear();
        if (role.isEmpty()) {
            kiosks.addAll(portalPersistence.getActiveKioskByUser(user));
            if (kiosks.isEmpty()) {
                BusinessStaff bs = portalPersistence.getBusinessStaffByKioskUser(user);
                List<BusinessStaffRole> bsrs = portalPersistence.getBusinessStaffRoleByBusinessStaff(bs);
                Set<Kiosk> ks = new HashSet<>();
                for (BusinessStaffRole bsr : bsrs) {
                    ks.add(bsr.getKiosk());
                }
                kiosks.addAll(ks);
            }
        } else {
            List<BusinessStaffRole> bsrs = portalPersistence.getUserRole(user, role);
            Set<Kiosk> ks = new HashSet<>();
            for (BusinessStaffRole bsr : bsrs) {
                ks.add(bsr.getKiosk());
            }
            kiosks.addAll(ks);
        }

    }

    private void populateCarts() {
        if (this.orderedItem != null) {
            final List<Cart> s = portalPersistence.getAllCartsByBatchId(this.orderedItem.getBatchId());
            carts = s;
        } else {
            carts = Collections.EMPTY_LIST;
        }
        // System.out.println("Carts :::: " + carts);
    }

    public List<SubItemCart> populateSubItemCart(Cart cart) {
        subItemCarts.clear();
        if (cart != null) {
            subItemCarts = portalPersistence.getAllSubItemCartByCartId(cart.getId());
        }
        return subItemCarts;
    }

    public List<Cart> getCarts() {
        if (!carts.isEmpty()) {
            carts.clear();
        }
        populateCarts();
        return carts;
    }

    public List<OrderedItemStatusLog> populateOrderLog() {
        orderItemLogs.clear();
        if (this.orderedItem != null) {
            orderItemLogs = portalPersistence.getOrderedItemStatusLogByItemId(this.orderedItem.getId());
        }
        return orderItemLogs;
    }

    public void populateBusiness() {
        businesses.clear();
        List<Business> businessz = portalPersistence.geAllActiveBusinessByUser(user.getId());
        List<BusinessStaffRole> bsrs = portalPersistence.getBusinessStaffRoleByStaff(user.getId());
        for (BusinessStaffRole bsr : bsrs) {
            businessz.add(bsr.getBusiness());
        }
        businesses.addAll(new HashSet<>(businessz));
    }

    public void loadKiosk() {
        kiosks.clear();
        kiosks = portalPersistence.getAllKioskByBusinessId(business != null ? business.getId() : null);
    }

    public String generateFileName(String fileName) {
        Long d = new Date().getTime();
        String s = fileName + "-" + d.toString();
        setExportTitle(fileName);
        return s;
    }

    private static void addEmptyLine(Paragraph paragraph, int number) {
        for (int i = 0; i < number; i++) {
            paragraph.add(new Paragraph(" "));
        }
    }

    public void processPDF(Object document) throws IOException, BadElementException, DocumentException {
        Document pdf = (Document) document;
        pdf.setPageSize(PageSize.A4.rotate());
        pdf.open();

        Font titleFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 20, Font.BOLD, Color.BLACK);
        Font subTitleFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 14, Font.BOLD, Color.BLACK);

        Paragraph title = new Paragraph();
        addEmptyLine(title, 1);
        title.add(new Paragraph(this.exportTitle, titleFont));

        SimpleDateFormat sdf = new SimpleDateFormat("MMMM dd, yyyy");
        addEmptyLine(title, 1);
        title.add(new Paragraph("Date: " + sdf.format(new Date()), subTitleFont));

        if (kiosk != null) {
            addEmptyLine(title, 1);
            title.add(new Paragraph("Kiosk: " + kiosk.getName(), subTitleFont));
        }
        if (paymentMode != null) {
            addEmptyLine(title, 1);
            title.add(new Paragraph("Payment Option: " + paymentMode.getOption(), subTitleFont));
        }

        if (deliveryOption != null) {
            addEmptyLine(title, 1);
            title.add(new Paragraph("Delivery Option: " + deliveryOption, subTitleFont));
        }

        if (tranxDate1 != null && tranxDate2 != null) {
            addEmptyLine(title, 1);
            title.add(new Paragraph("From " + sdf.format(tranxDate1) + " To " + sdf.format(tranxDate2), subTitleFont));
        }

        addEmptyLine(title, 2);
        pdf.add(title);
    }

    public void postProcessXLS(Object document) {
        SimpleDateFormat sdf = new SimpleDateFormat("MMMM dd, yyyy");
        String dateRange = "";
        if (tranxDate1 != null && tranxDate2 != null) {
            dateRange = new StringBuilder(": " + sdf.format(tranxDate1) + " - " + sdf.format(tranxDate2)).toString();
        }
        HSSFWorkbook wb = (HSSFWorkbook) document;
        HSSFSheet sheet = wb.getSheetAt(0);
        sheet.shiftRows(0, sheet.getLastRowNum() + 3, 3);

        HSSFRow businessRow = sheet.getRow(0);
        businessRow.createCell(1).setCellValue("Date: " + sdf.format(new Date()));

        if (kiosk != null) {
            businessRow.createCell(2).setCellValue("Kiosk: " + kiosk.getName());
        }
        if (paymentMode != null) {
            businessRow.createCell(3).setCellValue("Payment: " + paymentMode.getOption());
        }
        if (deliveryOption != null) {
            businessRow.createCell(4).setCellValue("Delivery Option: " + deliveryOption.getOption());
        }

        if (!dateRange.isEmpty()) {
            businessRow.createCell(5).setCellValue("Date Range" + dateRange);
        }
        HSSFRow header = sheet.getRow(3);

        HSSFCellStyle cellStyle = wb.createCellStyle();
        cellStyle.setFillForegroundColor(HSSFColor.GREEN.index);
        cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);

        for (int i = 0; i < header.getPhysicalNumberOfCells(); i++) {
            HSSFCell cell = header.getCell(i);

            cell.setCellStyle(cellStyle);
        }
    }

    public void loadOrderCounts() {
        List<OrderedItem> orders = new ArrayList<>();
        List<OrderedItem> pendingOrders = new ArrayList<>();
        List<OrderedItem> processedOrders = new ArrayList<>();
        List<OrderedItem> receivedOrders = new ArrayList<>();
        List<OrderedItem> shippededOrders = new ArrayList<>();

        List<BusinessStaffRole> bsrs = portalPersistence.getUserRole(user, ProjectConstants.ATTENDANT);
        Set<Kiosk> ks = new HashSet<>();
        for (BusinessStaffRole bsr : bsrs) {
            ks.add(bsr.getKiosk());
        }
        for (Kiosk k : ks) {
            orders.addAll(portalPersistence.getOrderedItemsByKiosk(k));
        }

        for (OrderedItem oi : orders) {
            if (oi.getOrderStatus().equals(OrderedItem.Status.PENDING)) {
                pendingOrders.add(oi);
            }
            if (oi.getOrderStatus().equals(OrderedItem.Status.RECEIVED)) {
                receivedOrders.add(oi);
            }
        }

        bsrs.clear();
        ks.clear();
        orders.clear();

        bsrs.addAll(portalPersistence.getUserRole(user, ProjectConstants.LOGISTICS_MANAGER));
        for (BusinessStaffRole bsr : bsrs) {
            ks.add(bsr.getKiosk());
        }
        for (Kiosk k : ks) {
            orders.addAll(portalPersistence.getOrderedItemsByKiosk(k));
        }

        for (OrderedItem oi : orders) {
            if (oi.getOrderStatus().equals(OrderedItem.Status.PROCESSING)) {
                processedOrders.add(oi);
            }

            if (oi.getOrderStatus().equals(OrderedItem.Status.SHIPPED)) {
                shippededOrders.add(oi);
            }
        }

        setPendingOrderCount(pendingOrders.size());
        setShippedOrderCount(shippededOrders.size());
        setProcessedOrderCount(processedOrders.size());
        setReceivedOrderCount(receivedOrders.size());
    }

    public void filterOrderTable(String status) {
        orderedItems.clear();

        if (kiosk != null) {
            orderedItems.addAll(portalPersistence.getOrderedItemsByKiosk(kiosk));
        } else {
            for (Kiosk k : kiosks) {
                orderedItems.addAll(portalPersistence.getOrderedItemsByKiosk(k));
            }
        }
        OrderedItem.Status s = OrderedItem.Status.valueOf(status.toUpperCase());
        List<OrderedItem> filtered = new ArrayList<>();
        for (OrderedItem oi : orderedItems) {
            if (oi.getOrderStatus() != null) {
                if (oi.getOrderStatus().equals(s)) {
                    filtered.add(oi);
                }
            }
        }
        orderedItems.clear();
        orderedItems.addAll(filtered);
        filtered.clear();

        if (this.tranxDate1 != null && this.tranxDate2 != null) {
            Long date1 = this.tranxDate1.getTime();
            Long date2 = this.tranxDate2.getTime();
            for (OrderedItem oi : orderedItems) {
                if (s.equals(OrderedItem.Status.PENDING)) {
                    if (oi.getCheckedOutDate() != null) {
                        Long oiDate = dateConverter(oi.getCheckedOutDate().toString()).getTime();
                        if (oiDate >= date1 && oiDate <= date2) {
                            filtered.add(oi);
                        }
                    }
                } else {
                    if (oi.getLastModifiedDate() != null) {
                        Long oiDate = dateConverter(oi.getLastModifiedDate().toString()).getTime();
                        if (oiDate >= date1 && oiDate <= date2) {
                            filtered.add(oi);
                        }
                    }
                }

            }
            orderedItems.clear();
            orderedItems.addAll(filtered);
            filtered.clear();
        }

        if (deliveryOption != null) {
            for (OrderedItem oi : orderedItems) {
                if (oi.getDeliveryOption().getDeliveryOption() == deliveryOption) {
                    filtered.add(oi);
                }
            }
            orderedItems.clear();
            orderedItems.addAll(filtered);
            filtered.clear();
        }

        if (paymentMode != null) {
            for (OrderedItem oi : orderedItems) {
                if (oi.getPaymentMethod() == paymentMode) {
                    filtered.add(oi);
                }
            }
            orderedItems.clear();
            orderedItems.addAll(filtered);
            filtered.clear();
        }

        if (orderId != null) {
            try {
                Long orderedLong = Long.valueOf(orderId.trim());
                for (OrderedItem oi : orderedItems) {
                    if (oi.getId().equals(orderedLong)) {
                        filtered.add(oi);
                    }
                }
                orderedItems.clear();
                orderedItems.addAll(filtered);
                filtered.clear();
            } catch (NumberFormatException e) {
                e.getMessage();
            }
        }

    }

    public boolean checkNewOrder(OrderedItem oi) {
        boolean neworder = false;
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        Long date1 = c.getTimeInMillis();

        Calendar c2 = Calendar.getInstance();
        c2.set(Calendar.HOUR_OF_DAY, 23);
        c2.set(Calendar.MINUTE, 59);
        c2.set(Calendar.SECOND, 59);
        c2.set(Calendar.MILLISECOND, 59);
        Long date2 = c2.getTimeInMillis();

        if (oi.getCheckedOutDate() != null) {
            Long oiDate = oi.getCheckedOutDate().getTime();
            if (oiDate >= date1 && oiDate <= date2) {
                neworder = true;
            }
        }
        return neworder;
    }

    public void loadOrderStatus(String stage) {
        orderStatus.clear();
        if (stage.equalsIgnoreCase("Pending")) {
            orderStatus.add(OrderedItem.Status.RECEIVED);
            orderStatus.add(OrderedItem.Status.CANCELLED);
        }

        if (stage.equalsIgnoreCase("History")) {
            orderStatus.add(OrderedItem.Status.DELIVERED);
        }

        if (stage.equalsIgnoreCase("Received")) {
            orderStatus.add(OrderedItem.Status.PROCESSING);
        }

        if (stage.equalsIgnoreCase("Processed")) {
            orderStatus.add(OrderedItem.Status.SHIPPED);
        }
    }


    public void loadDeliveryOption() {
        deliveryOptions = Arrays.asList(KioskDeliveryOption.Option.values());
    }

    public void loadPaymentMode() {
        paymentModes = Arrays.asList(KioskPaymentOption.Option.values());
    }

    public List<KioskPaymentOption.Option> offlinePayment() {
        List<KioskPaymentOption.Option> modes = new ArrayList<>();
        modes.add(KioskPaymentOption.Option.CASH);
        modes.add(KioskPaymentOption.Option.POS);
        return modes;
    }

    public boolean isPayOffline() {
        boolean offline = false;
        if (orderedItem != null) {
            if (orderedItem.getPaymentMethod() != null) {
                if (orderedItem.getPaymentMethod().equals(KioskPaymentOption.Option.PAYONDELIVERY)) {
                    offline = true;
                }
            }
        }
        return offline;
    }

    public String formatCurrency(Double amount) {
        double currency = 0;
        if (amount != null) {
            currency = amount;
        }
        return new DecimalFormat("###,###.###").format(currency);
    }

    public KioskUser getUser() {
        return user;
    }

    public void setUser(KioskUser user) {
        this.user = user;
    }

    public List<OrderedItem> getOrderedItems() {
        return orderedItems;
    }

    public List<KioskUser> dispatchers() {
        OrderedItem item = this.orderedItem;
        List<KioskUser> kus = new ArrayList<>();
        if (item != null) {
            Kiosk k = item.getKiosk();
            kus = portalPersistence.getUserByKioskAndUsergroup(k, ProjectConstants.DISPATCH_RIDER);
        }
        return kus;
    }

    public String findDispatcher(OrderedItem oi) {
        KioskUser ku = null;
        String dispatchGuy = null;
        OrderedItemStatusLog log = portalPersistence.getLogByOrderItemAndStatus(oi, "Shipped");
        if (log != null) {
            ku = log.getNextUpdateBy();
        }
        if (ku != null) {
            dispatchGuy = ku.getFirstName() + " " + ku.getSurname();
        }
        return dispatchGuy;
    }

    public static Date dateConverter(String sDate) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        try {
            date = formatter.parse(sDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public void setOrderedItems(List<OrderedItem> orderedItems) {
        this.orderedItems = orderedItems;
    }

    public OrderedItem getOrderedItem() {
        return orderedItem;
    }

    public void setOrderedItem(OrderedItem orderedItem) {
        this.orderedItem = orderedItem;
    }

    public OrderedItem.Status getStatus() {
        return status;
    }

    public void setStatus(OrderedItem.Status status) {
        this.status = status;
    }

    public List<OrderedItemStatusLog> getOrderItemLogs() {
        return orderItemLogs;
    }

    public void setOrderItemLogs(List<OrderedItemStatusLog> orderItemLogs) {
        this.orderItemLogs = orderItemLogs;
    }

    public KioskDeliveryOption.Option getDeliveryOption() {
        return deliveryOption;
    }

    public void setDeliveryOption(KioskDeliveryOption.Option deliveryOption) {
        this.deliveryOption = deliveryOption;
    }

    public List<KioskPaymentOption.Option> getPaymentModes() {
        return paymentModes;
    }

    public void setPaymentModes(List<KioskPaymentOption.Option> paymentModes) {
        this.paymentModes = paymentModes;
    }

    public KioskPaymentOption.Option getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(KioskPaymentOption.Option paymentMode) {
        this.paymentMode = paymentMode;
    }

    public KioskPaymentOption.Option getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(KioskPaymentOption.Option paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public List<String> getFilterStatus() {
        return filterStatus;
    }

    public void setFilterStatus(List<String> filterStatus) {
        this.filterStatus = filterStatus;
    }

    public KioskUser getDispatcher() {
        return dispatcher;
    }

    public void setDispatcher(KioskUser dispatcher) {
        this.dispatcher = dispatcher;
    }

    public List<SubItemCart> getSubItemCarts() {
        return subItemCarts;
    }

    public void setSubItemCarts(List<SubItemCart> subItemCarts) {
        this.subItemCarts = subItemCarts;
    }

    public List<OrderedItem> getFilteredOrderedItem() {
        return filteredOrderedItem;
    }

    public void setFilteredOrderedItem(List<OrderedItem> filteredOrderedItem) {
        this.filteredOrderedItem = filteredOrderedItem;
    }

    public Business getBusiness() {
        return business;
    }

    public void setBusiness(Business business) {
        this.business = business;
    }

    public Kiosk getKiosk() {
        return kiosk;
    }

    public void setKiosk(Kiosk kiosk) {
        this.kiosk = kiosk;
    }

    public List<Business> getBusinesses() {
        return businesses;
    }

    public void setBusinesses(List<Business> businesses) {
        this.businesses = businesses;
    }

    public List<Kiosk> getKiosks() {
        return kiosks;
    }

    public void setKiosks(List<Kiosk> kiosks) {
        this.kiosks = kiosks;
    }

    public List<OrderedItem.Status> getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(List<OrderedItem.Status> orderStatus) {
        this.orderStatus = orderStatus;
    }

    public List<KioskDeliveryOption.Option> getDeliveryOptions() {
        return deliveryOptions;
    }

    public void setDeliveryOptions(List<KioskDeliveryOption.Option> deliveryOptions) {
        this.deliveryOptions = deliveryOptions;
    }

    public Date getTranxDate1() {
        return tranxDate1;
    }

    public void setTranxDate1(Date tranxDate1) {
        this.tranxDate1 = tranxDate1;
    }

    public Date getTranxDate2() {
        return tranxDate2;
    }

    public void setTranxDate2(Date tranxDate2) {
        this.tranxDate2 = tranxDate2;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public void setCarts(List<Cart> carts) {
        this.carts = carts;
    }

    public Integer getReloadTime() {
        return Integer.valueOf(portalPersistence.getSettingByName(ProjectConstants.TIME_BEFORE_RELOAD).getSettingValue());
    }

    public void setReloadTime(Integer reloadTime) {
        this.reloadTime = reloadTime;
    }

    public Integer getPendingOrderCount() {
        return pendingOrderCount;
    }

    public void setPendingOrderCount(Integer pendingOrderCount) {
        this.pendingOrderCount = pendingOrderCount;
    }

    public Integer getReceivedOrderCount() {
        return receivedOrderCount;
    }

    public void setReceivedOrderCount(Integer receivedOrderCount) {
        this.receivedOrderCount = receivedOrderCount;
    }

    public Integer getProcessedOrderCount() {
        return processedOrderCount;
    }

    public void setProcessedOrderCount(Integer processedOrderCount) {
        this.processedOrderCount = processedOrderCount;
    }

    public Integer getShippedOrderCount() {
        return shippedOrderCount;
    }

    public void setShippedOrderCount(Integer shippedOrderCount) {
        this.shippedOrderCount = shippedOrderCount;
    }

    public String getExportTitle() {
        return exportTitle;
    }

    public void setExportTitle(String exportTitle) {
        this.exportTitle = exportTitle;
    }

}
