/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.morph.kiosk;

import com.morph.kiosk.persistence.entity.Kiosk;
import com.morph.kiosk.persistence.entity.KioskUser;
import com.morph.kiosk.persistence.entity.portal.BusinessStaffRole;
import com.morph.kiosk.persistence.entity.portal.PortalPage;
import com.morph.kiosk.persistence.entity.portal.PortalPermission;
import com.morph.kiosk.persistence.entity.portal.PortalUserGroup;
import com.morph.kiosk.persistence.entity.portal.PortalUserGroup_AccessGroup;
import com.morph.kiosk.persistence.service.PortalPersistence;
import com.morph.kiosk.persistence.service.UserPersistence;
import com.morph.kiosk.util.SessionUtils;
import com.morph.util.ProjectConstants;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import org.omnifaces.util.Faces;

/**
 *
 * @author Tobi-Morph-PC
 */
public abstract class BaseController implements Serializable {

    @Resource(lookup = "java:global/KioskEJBModule-1.2/UserPersistence!com.morph.kiosk.persistence.service.UserPersistence")
    private UserPersistence userPersistence;

    @Resource(lookup = "java:global/KioskEJBModule-1.2/PortalPersistence!com.morph.kiosk.persistence.service.PortalPersistence")
    private PortalPersistence portalPersistence;

    @PostConstruct
    public void init() {
        KioskUser user = (KioskUser) SessionUtils.getSession().getAttribute(ProjectConstants.SESSION_KIOSK_USER);
        //PortalUser portalUser = (PortalUser) JsfUtil.getHttpServletRequest().getSession().getAttribute(ProjectConstants.SESSION_PORTAL_USER);

        //portalUser = (PortalUser) Faces.getSessionAttribute(ProjectConstants.SESSION_PORTAL_USER);
        //KioskUser user = userPersistence.find(KioskUser.class, Long.valueOf("2231"));
        System.out.println("Kiosk User ::::: " + user);
        //PortalUser portalUser = persistence.getPortalUserByKioskUser(user.getId());
        initUser(user);
        //System.out.println("Business Module Init Kiosk User :::::::: " + user);
        //System.out.println("Business Module Init Portal User :::::::: " + portalUser);

        //BusinessStaff staff = userPersistence.getBusinessStaffByKioskUser(user.getId());
//        System.out.println("Business Staff ::::::::: " + staff);
        
        List<Kiosk> kiosks = new ArrayList<>();
        List<PortalUserGroup> userGroups = new ArrayList<>();

        //i-get user group
        //ii-get user group's access group
        //iii-get access_group permssions
        List<PortalPermission> l = new LinkedList<>();
        List<String> permissions = new LinkedList<>();
        Set<PortalPage> userPortalMenu = new HashSet<>();

        if (null != user) {
            List<BusinessStaffRole> staffRoles = portalPersistence.getBusinessStaffRoleByStaff(user.getId());
//            System.out.println("Business Staff Roles ::::::: " + staffRoles);
            staffRoles.forEach(i -> {
                kiosks.add(i.getKiosk());
                userGroups.add(i.getUserGroup());
            });

//            System.out.println("Business Staff Kiosk :::: " + kiosks);
//            System.out.println("User Group ::::: " + userGroups);
            
            userGroups.forEach(i -> {
                List<PortalUserGroup_AccessGroup> userGroup_AccessGroups = userPersistence.getUserGroupAccessGroupByUserGroup(i.getId());
                userGroup_AccessGroups.forEach((userGroup_AccessGroup) -> {
                    l.addAll(userPersistence.getPortalPermissionByAccessGroup(userGroup_AccessGroup.getAccessGroup().getId()));
                });
            });
            
            permissions = l.stream().map(PortalPermission::getPermissionName).collect(Collectors.toList());
            userPortalMenu = l.stream().filter(i -> i.getPage().isMenu()).map(PortalPermission::getPage).collect(Collectors.toSet());
        }
        
        Faces.setSessionAttribute(ProjectConstants.SESSION_PERMISSION_PARAM, permissions);
        Faces.setSessionAttribute(ProjectConstants.SESSION_PORTAL_MENU_PARAM, userPortalMenu);
        Faces.setSessionAttribute(ProjectConstants.SESSION_KIOSK_PARAM, kiosks);
        
        System.out.println(ProjectConstants.SESSION_PERMISSION_PARAM + " ::::: " + permissions);
        System.out.println(ProjectConstants.SESSION_PORTAL_MENU_PARAM + " :::: " + userPortalMenu);
        System.out.println(ProjectConstants.SESSION_KIOSK_PARAM + " :::: " + kiosks);

    }

    abstract void initUser(KioskUser user);

}
