/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.morph.kiosk;

import com.morph.kiosk.persistence.entity.Kiosk;
import com.morph.kiosk.persistence.entity.KioskUser;
import com.morph.kiosk.persistence.service.PortalPersistence;
import com.morph.util.ProjectConstants;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import org.omnifaces.util.Faces;

/**
 *
 * @author Tobi-Morph-PC
 */
@Named(value = "kioskController")
@SessionScoped
public class KioskController extends BaseController implements Serializable {

    @Resource(lookup = "java:global/KioskEJBModule-1.2/PortalPersistence!com.morph.kiosk.persistence.service.PortalPersistence")
    private PortalPersistence persistence;
    
    private List<Kiosk> kiosks = new ArrayList<>();
    private KioskUser user = new KioskUser();
    
    public KioskController() {
    }
    
    @Override
    void initUser(KioskUser user) {
        this.user = user;
        //this.kiosks = Faces.getSessionAttribute(ProjectConstants.SESSION_KIOSK_PARAM);
        System.out.println("Kiosk Controller Session User ::::: " + user);
    }

    public List<Kiosk> getKiosks() {
        this.kiosks = Faces.getSessionAttribute(ProjectConstants.SESSION_KIOSK_PARAM);
        System.out.println("Kiosk Controller Session Kiosks ::::: " + this.kiosks);
        return kiosks;
    }

    public void setKiosks(List<Kiosk> kiosks) {
        this.kiosks = kiosks;
    }

    public KioskUser getUser() {
        return user;
    }

    public void setUser(KioskUser user) {
        this.user = user;
    }

    
    
}
