/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.morph.kiosk.util.filter;

import com.morph.kiosk.persistence.entity.KioskUser;
import com.morph.kiosk.persistence.service.UserPersistence;
import com.morph.util.ProjectConstants;
import java.io.IOException;
import javax.annotation.Resource;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Tobi-Morph-PC
 */
public class PortalUserFilter  implements Filter {

    @Resource(lookup = "java:global/KioskEJBModule-1.2/UserPersistence!com.morph.kiosk.persistence.service.UserPersistence")
    private UserPersistence persistence;
    
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        KioskUser user = (KioskUser) ((HttpServletRequest) request).getSession().getAttribute(ProjectConstants.SESSION_KIOSK_USER);
      //  System.out.println("Manager User Filter :::: " + user);
        //PortalUser user = persistence.getPortalUserByKioskUser(Long.valueOf("30"));
        if (null == user || !KioskUser.Designation.STAFF.equals(user.getDesignation())) {
            //String contextPath = ((HttpServletRequest) request).getContextPath();
            String requestURI = ((HttpServletRequest) request).getRequestURI();
            String queryString = ((HttpServletRequest) request).getQueryString();
                        
            if(null != queryString && !queryString.isEmpty()){
                requestURI = requestURI + "?" + queryString;
            }else{
                
            }
            
         //   System.out.println("Manager PortalUserFilter Redirect url :::::::::::: " + requestURI);
            
            HttpSession session = ((HttpServletRequest) request).getSession();
            session.setAttribute(ProjectConstants.SESSION_REDIRECT_SOURCE, requestURI);
            
            final String redirectUrl =(
                    request.getScheme() +"://" + ((HttpServletRequest)request).getServerName() + ":" +request.getServerPort()
                    + "/user/dashboard.xhtml");
          //  System.out.println("Manager Portal Filter Redirect ::::: " + redirectUrl);
            
            ((HttpServletResponse) response).sendRedirect(redirectUrl);
            //((HttpServletResponse) response).sendRedirect(contextPath + "/signin.xhtml?url=" + URLEncoder.encode(requestURI));
        }
        chain.doFilter(request, response);
    }

    @Override
    public void init(FilterConfig config) throws ServletException {
    }

    @Override
    public void destroy() {
    }

}
