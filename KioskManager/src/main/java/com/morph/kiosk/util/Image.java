/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.morph.kiosk.util;

import java.io.Serializable;
import java.util.Arrays;

/**
 *
 * @author root
 */
public class Image implements Serializable {

    private String fileName;
    private byte[] content;
    private String filePath;
    private Long itemId;

    public Image() {
    }
    
    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public Long getItemId() {
        return itemId;
    }

    public void setItemId(Long itemId) {
        this.itemId = itemId;
    }

    @Override
    public String toString() {
        return "Image{" + "fileName=" + fileName + ", content=" + Arrays.toString(content) + ", filePath=" + filePath + ", itemId=" + itemId + '}';
    }

    
}