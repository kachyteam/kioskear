/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.morph.kiosk.util;

import com.morph.kiosk.persistence.entity.order.OrderedItem;
import com.morph.kiosk.persistence.service.PortalPersistence;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

/**
 *
 * @author Tobi-Morph-PC
 */
public class OrderedItemLazyDataModel extends LazyDataModel<OrderedItem>{

    //@Resource(lookup = "java:global/KioskEJBModule-1.2/PortalPersistence!com.morph.kiosk.persistence.service.PortalPersistence")
    PortalPersistence portalPersistence;
    
    private List<OrderedItem> orderedItems = new ArrayList<>();
    private Long filter;

    @PostConstruct
    public void init(){
        try {
            InitialContext ic = new InitialContext();  // JNDI initial context
            portalPersistence = (PortalPersistence) ic.lookup("java:global/KioskEJBModule-1.2/PortalPersistence!com.morph.kiosk.persistence.service.PortalPersistence"); // JNDI lookup
            System.out.println("Init Portal Persistence :::::: " + portalPersistence);
        } catch (NamingException ex) {
            System.err.println("OrderedItemLazyDataModel init Error ::::" + ex);
        }
    }
    public OrderedItemLazyDataModel(PortalPersistence portalPersistence, Long filter) {
        super();
        this.portalPersistence = portalPersistence;
        this.filter = filter;
        System.out.println("Portal Persistence :::::: " + portalPersistence);
    }

    
    
    @Override
    public List<OrderedItem> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
        System.out.println("Load :::: " + first + " ::::: " + pageSize);
        orderedItems = (null == filter) ? portalPersistence.getLazyOrderedItems(first, pageSize) : portalPersistence.getLazyOrderedItemsByBusinessOwnerId(filter, first, pageSize);
        //orderedItems = portalPersistence.getLazyOrderedItems(first, pageSize);
        if(getRowCount() <= 0){
            setRowCount(Integer.valueOf(String.valueOf(portalPersistence.getLazyOrderedItemsCount(filter))));
        }
        System.out.println("Row Count ::: " + getRowCount());
        //setRowCount(orderedItems.size());
        setPageSize(pageSize);
        return orderedItems;
    }

    @Override
    public OrderedItem getRowData(String rowKey) {
        Long id = Long.valueOf(rowKey);
        return orderedItems.stream().filter(i -> i.getId().equals(id)).findFirst().get();
    }

    @Override
    public Object getRowKey(OrderedItem object) {
        return object.getId();
    }

    public Long getFilter() {
        return filter;
    }

    public void setFilter(Long filter) {
        this.filter = filter;
    }

    public List<OrderedItem> getOrderedItems() {
        return orderedItems;
    }

    public void setOrderedItems(List<OrderedItem> orderedItems) {
        this.orderedItems = orderedItems;
    }
    
}
