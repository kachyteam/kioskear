/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.morph.util;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import javax.xml.bind.DatatypeConverter;

/**
 *
 * @author Tobi-Morph-PC
 */
public class ProjectConstants {
    
    //Business
    public final static String PAGE_NOT_FOUND = "Requested page cannot be found";
    public final static String DUPLICATE_ENTRY = "This name has been used before. Please fill in another name";
    public final static String UPDATE_SUCCESSFUL = "Record have been updated";
    public final static String UPDATE_UNSUCCESSFUL = "Update operation was not successful. Kindly try again or contact portal administrator";
    public final static String OPERATION_SUCCESSFUL = "Operation was successful";
    public final static String OPERATION_UNSUCCESSFUL = "Operation was not successful";
    public final static String GENERIC_ERROR = "An error has occured. Kindly contact system administrator.";
    public final static String KIOSK_CREATED = "Kiosk was successfully created.";
    public final static String BUSINESS_CREATED = "Business was successfully created.";
    public final static String ITEM_CREATED = "Item was successfully created.";
    public final static String SUBITEM_CREATED = "SubItem was successfully created.";
    public final static String SUBITEM_GROUP_CREATED = "SubItem Group was successfully created.";
    public final static String ITEM_ADDED = "Item has been added.";
    public final static String ITEM_REMOVED = "Item has been removed.";
    public final static String ACCOUNT_CREATED = "Account has been created.";
    public final static String BUSINESS_DISABLED = "Business has been disabled.";
    public final static String BUSINESS_ENABLED = "Business has been enabled.";
    public final static String KIOSK_DISABLED = "Kiosk has been disabled.";
    public final static String ITEM_REGISTER_PRICE = "Kindly set Item Price";
    public final static String ITEM_DISABLED = "Item has been disabled.";
    public final static String SUBITEM_DISABLED = "SubItem has been disabled.";
    public final static String SUBITEM_ENABLED = "SubItem has been enabled.";
    public final static String KIOSK_ENABLED = "Kiosk has been enabled.";
    public final static String ITEM_ENABLED = "Item has been enabled.";
    public final static String REQUEST_SENT ="Request have been sent. Kindly check your email address to continue registration process";
    public final static String REQUEST_UNSENT = "Request cannot be sent now. kindly try again later";
    public final static String EMAIL_DUPLICATE = "Email Address already exist. Duplicate email are not allowed.";
    public final static String PHONENUMBER_DUPLICATE = "Phone number already exist. Duplicate phone numbers are not allowed.";
    public final static String WRONG_CREDENTIAL = "No account found for your credential. Kindly verify and try again";
    
    public final static String PORTAL_USER_REG_EMAIL_MESSAGE = "Hello Business Owner, Thank you for registering in the Kiosk Platform. Kindy verify this your request by clicking on the link below";
    public final static String PORTAL_USER_REG_EMAIL_MESSAGE2 = "We need to confirm your email address. To complete the subscription process, please click the link in the email we just sent you.";
    public final static String PORTAL_USER_REG_EMAIL_SUBJECT = "Business Request Verification";
    
    public final static String PASSWORD_CHARGE_SUBJECT = "Chef Tent - Password Reset";
    
    public final static String SESSION_BUSINESS_STAFF = "businessStaff";
    public final static String SESSION_KIOSK_USER = "kioskUser";
    public final static String SESSION_USER_CHANGE_PASSWORD = "kioskUserChangePassword";
    public final static String SESSION_PORTAL_USER = "portalUser";
    public final static String SESSION_SUPER_PORTAL_USER = "superPortalUser";
    public final static String SESSION_REDIRECT_SOURCE = "redirectsource";
    public final static String SESSION_REQUEST_MESSAGE = "requestMessage";
    public final static String SESSION_EMAIL_AUTH_MESSAGE = "emailAuthMessage";
    public final static String CHANGE_PASSWORD_CODE = "changePasswordCode";
    
    
    public final static String PAGE_QUERY_PARAM_BUSINESS = "bquery";
    public final static String PAGE_QUERY_PARAM_KIOSK = "kquery";
    public final static String PAGE_QUERY_PARAM_ITEM = "iquery";
    public final static String PAGE_QUERY_PARAM_USER = "uquery";
    public final static String PAGE_QUERY_PARAM_SUBITEM = "Squery";
    
    
    
    
    public final static String CONTEXT_BUSINESS_MODULE = "/business";
    public final static String CONTEXT_DEFAULT_MODULE = "/";
    
    public static final String PERMISSION_CREATE = "CREATE";
    public static final String PERMISSION_READ = "READ";
    public static final String PERMISSION_UPDATE = "UPDATE";
    public static final String PERMISSION_DELETE = "DELETE";
    
    
    public static final String SESSION_PERMISSION_PARAM = "permissions";
    public static final String SESSION_PORTAL_MENU_PARAM = "menu";
    
    
    public static final String SESSION_KIOSK_PARAM = "kiosks";
    
    
    public final static String VIEWID_EXT =".xhtml";
    
    public final static String REQUEST_EMAIL_EMPTY ="Kindly fill the email in your profile.";
    
    //User Groups
    public static final String ATTENDANT = "Attendant";
    public static final String ACCOUNTANT = "Accountant";
    public static final String LOGISTICS_MANAGER = "Logistics Manager";
    public static final String DISPATCH_RIDER = "Dispatch Rider";
    
    public static final String TIME_BEFORE_RELOAD = "Time Before Page Reload";
    
    
    
    public static String generateSHA256Token(String s){
        String result = null;
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(s.getBytes("UTF-8"));
            return bytesToHex(hash); // make it printable
        }catch(UnsupportedEncodingException | NoSuchAlgorithmException ex) {
            ex.getMessage();
        }
        return result;
    }
    
    
    private static String  bytesToHex(byte[] hash) {
        return DatatypeConverter.printHexBinary(hash);
    }
    
    
    
}
