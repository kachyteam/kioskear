/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.morph.util;

import com.morph.kiosk.persistence.service.PortalPersistence;
import com.morph.kiosk.persistence.service.UserPersistence;
import javax.annotation.Resource;

/**
 *
 * @author Tobi-Morph-PC
 */
public class PortalPersistenceUtil {
    
    @Resource(lookup = "java:global/KioskEJBModule-1.2/UserPersistence!com.morph.kiosk.persistence.service.UserPersistence")
    private static UserPersistence userPersistence;

    @Resource(lookup = "java:global/KioskEJBModule-1.2/PortalPersistence!com.morph.kiosk.persistence.service.PortalPersistence")
    private static PortalPersistence portalPersistence;
    
    public static UserPersistence getUserPersistence(){
        return userPersistence;
    }
    
    public static PortalPersistence getPortalPersistence(){
        return portalPersistence;
    
    }
}
