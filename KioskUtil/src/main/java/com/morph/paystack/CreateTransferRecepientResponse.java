/**
 * Class Name: CreateTransferRecepient Project Name: KioskUtil Developer:
 * Onyedika Okafor (ookafor@morphinnovations.com) Version Info: Create Date: Feb
 * 17, 2017 12:22:14 PM (C)Morph Innovations Limited 2017. Morph Innovations
 * Limited Asserts its right to be known as the author and owner of this file
 * and its contents.
 */
package com.morph.paystack;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;

/**
 * @author Onyedika Okafor (ookafor@morphinnovations.com)
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class CreateTransferRecepientResponse implements Serializable {

    private boolean status;
    private String message;
    private Data data;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

}
