/**
*Class Name: Metadata
*Project Name: KioskUtil
*Developer: Onyedika Okafor (ookafor@morphinnovations.com)
*Version Info:
*Create Date: Feb 17, 2017 12:25:24 PM
*(C)Morph Innovations Limited 2017. Morph Innovations Limited Asserts its right to be known
*as the author and owner of this file and its contents.
*/

package com.morph.paystack;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author Onyedika Okafor (ookafor@morphinnovations.com)
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Metadata {
    
    private String job;
    
    private String businessName;
    
    private String settledOn;

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getSettledOn() {
        return settledOn;
    }

    public void setSettledOn(String settledOn) {
        this.settledOn = settledOn;
    }
    
    

}
