/**
 * Class Name: Details Project Name: KioskUtil Developer: Onyedika Okafor
 * (ookafor@morphinnovations.com) Version Info: Create Date: Feb 17, 2017
 * 12:26:58 PM (C)Morph Innovations Limited 2017. Morph Innovations Limited
 * Asserts its right to be known as the author and owner of this file and its
 * contents.
 */
package com.morph.paystack;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author Onyedika Okafor (ookafor@morphinnovations.com)
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Details {

    private String account_number;
    private String account_name;
    private String bank_code;
    private String bank_name;

    public String getAccount_number() {
        return account_number;
    }

    public void setAccount_number(String account_number) {
        this.account_number = account_number;
    }

    public String getAccount_name() {
        return account_name;
    }

    public void setAccount_name(String account_name) {
        this.account_name = account_name;
    }

    public String getBank_code() {
        return bank_code;
    }

    public void setBank_code(String bank_code) {
        this.bank_code = bank_code;
    }

    public String getBank_name() {
        return bank_name;
    }

    public void setBank_name(String bank_name) {
        this.bank_name = bank_name;
    }

}
