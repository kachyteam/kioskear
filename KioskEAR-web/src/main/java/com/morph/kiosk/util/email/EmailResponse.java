/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.morph.kiosk.util.email;

import com.morph.kiosk.persistence.entity.KioskUser;
import com.morph.kiosk.persistence.entity.portal.PasswordChange;
import com.morph.kiosk.persistence.entity.portal.PortalRequest;
import com.morph.kiosk.persistence.entity.portal.PortalUserRequestLog;
import com.morph.kiosk.persistence.service.PortalPersistence;
import com.morph.kiosk.persistence.service.UserPersistence;
import com.morph.util.ProjectConstants;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Onyedika
 */
public class EmailResponse extends HttpServlet {

    private UserPersistence userPersistence;
    private PortalPersistence portalPersistence;

    @Override
    public void init() throws ServletException {
        try {
            InitialContext ic = new InitialContext();  // JNDI initial context
            userPersistence = (UserPersistence) ic.lookup("java:global/KioskEJBModule-1.2/UserPersistence!com.morph.kiosk.persistence.service.UserPersistence"); // JNDI lookup
            portalPersistence = (PortalPersistence) ic.lookup("java:global/KioskEJBModule-1.2/PortalPersistence!com.morph.kiosk.persistence.service.PortalPersistence");
        } catch (NamingException ex) {
            Logger.getLogger(EmailResponse.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet EmailResponse</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet EmailResponse at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void portalRegistrationResponse(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        //retrieve user request log with user id and activationCode param. Then use the result to retrieve request and edit the pending to precess
        //Long userId = Long.valueOf(request.getParameter("user"));
        String activationCode = request.getParameter("activationCode");
        //KioskUser user = (KioskUser) ((HttpServletRequest) request).getSession().getAttribute("user");
        if (activationCode != null && !activationCode.isEmpty()) {
            KioskUser user = portalPersistence.find(KioskUser.class, Long.valueOf("9"));
            PortalUserRequestLog requestLog = portalPersistence.getPortalUserRequestLogByIdAndActCoce(user.getId(), activationCode);
            if (null == requestLog || !requestLog.isCodeStatus()) {
                //feedback..requw
                System.out.println("Portal Request Doesn't exist.....");
                HttpSession session = ((HttpServletRequest) request).getSession();
                session.setAttribute(ProjectConstants.SESSION_EMAIL_AUTH_MESSAGE, "Request cannot be processed (Invalid Token).");
                response.sendRedirect(request.getServletContext().getContextPath() + "/user/dashboard.xhtml");

            } else {
                PortalRequest portalRequest = portalPersistence.find(PortalRequest.class, requestLog.getRequest().getId());
                portalRequest.setStatus(PortalRequest.RequestStatus.PROCESSING);
                portalRequest.setLastModifiedDate(new Date());
                portalPersistence.update(portalRequest);
                requestLog.setCodeStatus(false);
                portalPersistence.update(requestLog);
                HttpSession session = ((HttpServletRequest) request).getSession();
                session.setAttribute(ProjectConstants.SESSION_EMAIL_AUTH_MESSAGE, "Request is being processed");
                response.sendRedirect(request.getServletContext().getContextPath() + "/user/userRequests.xhtml");
            }
        }
    }

    protected void passwordResponse(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            response.setContentType("text/html;charset=UTF-8");
            String validationCode = request.getParameter("code");
            String emailAddress = request.getParameter("emailAddress");
            if (validationCode != null && emailAddress != null & !validationCode.isEmpty() && !emailAddress.isEmpty()) {
                
                PasswordChange pc = portalPersistence.getPasswordChangeByCode(validationCode);

                if (pc == null) {
                    HttpSession session = ((HttpServletRequest) request).getSession();
                    session.setAttribute(ProjectConstants.SESSION_EMAIL_AUTH_MESSAGE, "Validation Code is not Valid");
                    response.sendRedirect(request.getServletContext().getContextPath() + "/signin.xhtml");
                }

                if (new Date().getTime() > pc.getRequestExpirationTime().getTime()) {
                    HttpSession session = ((HttpServletRequest) request).getSession();
                    session.setAttribute(ProjectConstants.SESSION_EMAIL_AUTH_MESSAGE, "This Password Request has Expired");
                    response.sendRedirect(request.getServletContext().getContextPath() + "/signin.xhtml");
                }

                if (pc.getCompletedTime() != null) {
                    HttpSession session = ((HttpServletRequest) request).getSession();
                    session.setAttribute(ProjectConstants.SESSION_EMAIL_AUTH_MESSAGE, "This Password Request has been Fulfilled");
                    response.sendRedirect(request.getServletContext().getContextPath() + "/signin.xhtml");
                }

                KioskUser user = userPersistence.getKioskUserByEmailAddress(emailAddress);

                if (!user.equals(pc.getKioskUser())) {
                    System.out.println("User Doesn't exist.....");
                    HttpSession session = ((HttpServletRequest) request).getSession();
                    session.setAttribute(ProjectConstants.SESSION_EMAIL_AUTH_MESSAGE, "Password Cannot be Changed at this moment.");
                    response.sendRedirect(request.getServletContext().getContextPath() + "/signin.xhtml");

                } else {
                    System.out.println("User exists");
                    HttpSession session = ((HttpServletRequest) request).getSession();
                    session.setAttribute(ProjectConstants.SESSION_USER_CHANGE_PASSWORD, user);
                    session.setAttribute(ProjectConstants.CHANGE_PASSWORD_CODE, validationCode);
                    response.sendRedirect(request.getServletContext().getContextPath() + "/changePassword.xhtml");

                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // processRequest(request, response);
        passwordResponse(request, response);
        portalRegistrationResponse(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // processRequest(request, response);
        passwordResponse(request, response);
        portalRegistrationResponse(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
