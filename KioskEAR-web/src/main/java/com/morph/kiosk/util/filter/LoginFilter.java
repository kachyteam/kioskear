/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.morph.kiosk.util.filter;

import com.morph.kiosk.persistence.entity.KioskUser;
import com.morph.kiosk.persistence.service.UserPersistence;
import com.morph.util.ProjectConstants;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Filter checks if LoginBean has loginIn property set to true. If it is not set
 * then request is being redirected to the login.xhml page.
 *
 * @author itcuties
 *
 */
public class LoginFilter implements Filter {

    private UserPersistence userPersistence;

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        // Get the loginBean from session attribute
        String emailAddress = request.getParameter("emailAddress");
        KioskUser u = userPersistence.getKioskUserByEmailAddress(emailAddress);
        ((HttpServletRequest) request).getSession().setAttribute(ProjectConstants.SESSION_USER_CHANGE_PASSWORD, u);
        KioskUser user = (KioskUser) ((HttpServletRequest) request).getSession().getAttribute(ProjectConstants.SESSION_KIOSK_USER);
       
        //KioskUser user = (KioskUser)SessionUtils.getSession().getAttribute(ProjectConstants.SESSION_KIOSK_USER);
        // System.out.println("Login Filter Kiosk User ===>> " + user);
        //SessionUtils.getSession().setAttribute("requestURI", requestURI);
        // For the first application request there is no loginBean in the session so user needs to log in
        // For other requests loginBean is present but we need to check if user has logged in successfully
        if (user == null && u == null) {

            HttpSession session = ((HttpServletRequest) request).getSession();
            String requestURI = (String) session.getAttribute(ProjectConstants.SESSION_REDIRECT_SOURCE);
            //   System.out.println("Login Filter Redirect before evaluation ===============>> " + requestURI);
            if (requestURI == null) {
                //String contextPath = ((HttpServletRequest) request).getContextPath();
                requestURI = ((HttpServletRequest) request).getRequestURI();
                String queryString = ((HttpServletRequest) request).getQueryString();

                if (null != queryString && !queryString.isEmpty()) {
                    requestURI = requestURI + "?" + queryString;
                }
            }

            // System.out.println("LoginFilter Redirect url :::::::::::: " + requestURI);
            session.setAttribute(ProjectConstants.SESSION_REDIRECT_SOURCE, requestURI);

            ((HttpServletResponse) response).sendRedirect(((HttpServletRequest) request).getContextPath() + "/signin.xhtml");
            //((HttpServletResponse) response).sendRedirect(contextPath + "/signin.xhtml);
        }
        chain.doFilter(request, response);
    }

    @Override
    public void init(FilterConfig config) throws ServletException {
        try {
            InitialContext ic = new InitialContext();  // JNDI initial context
            userPersistence = (UserPersistence) ic.lookup("java:global/KioskEJBModule-1.2/UserPersistence!com.morph.kiosk.persistence.service.UserPersistence"); // JNDI lookup
        } catch (NamingException ex) {
            Logger.getLogger(LoginFilter.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void destroy() {
    }

}
