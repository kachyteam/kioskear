/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.morph.kiosk.controller;

import com.morph.kiosk.email.EmailService;
import com.morph.kiosk.persistence.entity.KioskUser;
import com.morph.kiosk.persistence.entity.portal.PortalRequest;
import com.morph.kiosk.persistence.entity.portal.PortalRequestType;
import com.morph.kiosk.persistence.service.PortalPersistence;
import com.morph.util.JsfUtil;
import com.morph.util.ProjectConstants;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.faces.context.FacesContext;
import org.omnifaces.util.Faces;

/**
 *
 * @author Tobi-Morph-PC
 */
@Named("userRequestController")
@SessionScoped
public class UserRequestController extends BaseController {

    @Resource(lookup = "java:global/KioskEJBModule-1.2/PortalPersistence!com.morph.kiosk.persistence.service.PortalPersistence")
    private PortalPersistence portalPersistence;

    @Resource(lookup = "java:global/KioskEJBModule-1.2/EmailService!com.morph.kiosk.email.EmailService")
    private EmailService emailService;

    final Logger log = Logger.getAnonymousLogger();

    private KioskUser user;

    private PortalRequest request = new PortalRequest();

    private List<PortalRequest> userPortalRequests = new ArrayList<>();

    @PostConstruct
    public void initialize() {
        this.user = (KioskUser) JsfUtil.getHttpSession().getAttribute(ProjectConstants.SESSION_KIOSK_USER);
        System.out.println("In class Post Constructor ...................");
    }

    public UserRequestController() {
        System.out.println("Requests Constructor ::::: " + this.user);

    }

    @Override
    public void initUser(KioskUser user) {
        this.user = user;
        System.out.println("User Request Controller InitUser ::::::::: " + this.user);
    }

    public void sendRequest() {
        System.out.println("Send Request Get Called :::::::::::::::::::::: ");
        if (user != null) {

            if (null != user.getEmailAddress()) {
                try {
                    PortalRequest pr = portalPersistence.getUserRequest(this.user.getId());
                    if (pr != null && (pr.isActive() == true || pr.getStatus() == PortalRequest.RequestStatus.PENDING)) {
                        this.request = new PortalRequest();
                        JsfUtil.addWarningFacesMessage(ProjectConstants.REQUEST_UNSENT);
                    } else {
                        this.request.setCreatedDate(new Date());
                        this.request.setActive(false);
                        this.request.setStatus(PortalRequest.RequestStatus.PENDING);
                        this.request.setUser(user);
                        portalPersistence.create(this.request);
                        Faces.setSessionAttribute(ProjectConstants.SESSION_REQUEST_MESSAGE, ProjectConstants.REQUEST_SENT);
                        FacesContext.getCurrentInstance().getExternalContext().redirect(JsfUtil.getServletContext().getContextPath() + "/user/userRequest.xhtml");
                        JsfUtil.addInfoFacesMessage(ProjectConstants.REQUEST_SENT);

                        String uuid = String.valueOf(UUID.randomUUID());
                        String message = ProjectConstants.PORTAL_USER_REG_EMAIL_MESSAGE + "<br />"
                                + "Copy the following to a browser to complete request. <br /><br />"
                                + JsfUtil.getHttpServletRequest().getLocalAddr()
                                + ":" + JsfUtil.getHttpServletRequest().getServerPort()
                                + "/user/EmailResponse?activationCode="
                                + uuid
                                + "<br />"
                                + "Thank you!";

//                        portalUserRegistrationEmailEvt.fire(new PortalUserRegistrationEmail(
//                                ProjectConstants.PORTAL_USER_REG_EMAIL_MESSAGE,
//                                ProjectConstants.PORTAL_USER_REG_EMAIL_SUBJECT,
//                                user.getEmailAddress(), this.request));
                        // emailerEvent.fire(new Emailer(message, ProjectConstants.PORTAL_USER_REG_EMAIL_SUBJECT, user.getEmailAddress().trim(), uuid, this.request));
                        emailService.prepareEmail(message, ProjectConstants.PORTAL_USER_REG_EMAIL_SUBJECT, user.getEmailAddress().trim(), uuid, this.request);
                        this.request = new PortalRequest();
                    }
                } catch (Exception e) {
                    JsfUtil.addErrorFacesMessage(ProjectConstants.GENERIC_ERROR);
                    e.getLocalizedMessage();
                }
            } else {
                log.log(Level.WARNING, "user email address is empty");
                JsfUtil.addWarningFacesMessage(ProjectConstants.REQUEST_EMAIL_EMPTY);
            }

        } else {
            System.out.println("User is null .....");
        }
    }

    public void updateRequestComment() {
        try {
            PortalRequest pr = portalPersistence.find(PortalRequest.class, this.request.getId());
            if (pr != null && !pr.getStatus().equals(PortalRequest.RequestStatus.PENDING)) {
                JsfUtil.addErrorFacesMessage(ProjectConstants.UPDATE_UNSUCCESSFUL);
            } else {
                this.request.setLastModifiedDate(new Date());
                portalPersistence.update(this.request);
                JsfUtil.addInfoFacesMessage(ProjectConstants.UPDATE_SUCCESSFUL);
            }
        } catch (Exception e) {
            e.getLocalizedMessage();
        }
    }

    public void loadUserRequest() {
        log.log(Level.WARNING, String.valueOf(this.request));
    }

    public String edit(PortalRequest r) {
        if (r.getStatus().equals(PortalRequest.RequestStatus.PROCESSING)) {
            JsfUtil.addWarningFacesMessage("Request is been processed.");
            return null;
        } else {
            this.request = r;
            return "userRequests";
        }
    }

    public String newRequest() {
        request = new PortalRequest();
        return "userRequest";
    }

    public void displayAuthResponse() {
        String message = (String) Faces.getExternalContext()
                .getSessionMap().remove(ProjectConstants.SESSION_EMAIL_AUTH_MESSAGE);
        if (message != null) {
            JsfUtil.addInfoFacesMessage(message);
        }
    }

    public void requestMessageAlertDisplay() {
        String message = (String) Faces.getExternalContext()
                .getSessionMap().remove(ProjectConstants.SESSION_REQUEST_MESSAGE);
        if (message != null) {
            JsfUtil.addInfoFacesMessage(message);
        }
    }

    public void reset() {
        this.user = (KioskUser) JsfUtil.getHttpSession().getAttribute(ProjectConstants.SESSION_KIOSK_USER);
        this.request = new PortalRequest();
    }

    public KioskUser getUser() {
        return user;
    }

    public void setUser(KioskUser user) {
        this.user = user;
    }

    public List<PortalRequestType> getRequestTypes() {
        return portalPersistence.getActivePortalRequestType();
    }

    public PortalRequest getRequest() {
        return request;
    }

    public void setRequest(PortalRequest request) {
        this.request = request;
    }

    private void populateUserPortalRequests() {
        userPortalRequests = portalPersistence.getPortalRequestsByKioskUser(this.user.getId());
    }

    public List<PortalRequest> getUserPortalRequests() {
        //this.user = (KioskUser) JsfUtil.getHttpSession().getAttribute(ProjectConstants.SESSION_KIOSK_USER);
        //userPortalRequests = portalPersistence.getPortalRequestsByKioskUser(this.user.getId());
        if (!userPortalRequests.isEmpty()) {
            userPortalRequests.clear();
        }
        populateUserPortalRequests();
        return userPortalRequests;
    }

    public void setUserPortalRequests(List<PortalRequest> userPortalRequests) {
        this.userPortalRequests = userPortalRequests;
    }

}
