/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.morph.kiosk.controller;

import com.morph.kiosk.email.EmailService;
import com.morph.kiosk.persistence.entity.Business;
import com.morph.util.JsfUtil;
import com.morph.util.SessionUtils;
import com.morph.kiosk.persistence.entity.KioskUser;
import com.morph.kiosk.persistence.entity.portal.BusinessStaff;
import com.morph.kiosk.persistence.entity.portal.PasswordChange;
import com.morph.kiosk.persistence.entity.portal.PortalRole;
import com.morph.kiosk.persistence.entity.portal.PortalSuperAdminUser;
import com.morph.kiosk.persistence.entity.portal.PortalUser;
import com.morph.kiosk.persistence.entity.portal.PortalUserSessionLog;
import com.morph.kiosk.persistence.service.PortalPersistence;
import com.morph.kiosk.persistence.service.UserPersistence;
import com.morph.util.ProjectConstants;
import com.morph.util.UserSessionPojo;
import java.io.IOException;
import java.io.Serializable;
import java.net.URI;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.Asynchronous;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import org.omnifaces.util.Faces;

/**
 *
 * @author Tobi-Morph-PC
 */
@Named("userController")
@SessionScoped
public class UserController implements Serializable {

    @Resource(lookup = "java:global/KioskEJBModule-1.2/UserPersistence!com.morph.kiosk.persistence.service.UserPersistence")
    private UserPersistence persistence;

    @Resource(lookup = "java:global/KioskEJBModule-1.2/PortalPersistence!com.morph.kiosk.persistence.service.PortalPersistence")
    private PortalPersistence portalPersistence;

    @Resource(lookup = "java:global/KioskEJBModule-1.2/EmailService!com.morph.kiosk.email.EmailService")
    private EmailService emailService;

    private KioskUser user;
    private PortalUser portalUser = new PortalUser();
    private List<PortalRole> userPortalRoles = new ArrayList<>();
    private BusinessStaff businessStaff = new BusinessStaff();
    private String businessName;
    private byte[] businessLogo;
    private boolean resetDone = false;
    private String currentPassword;

    public UserController() {

    }

    @PostConstruct
    public void initialize() {
        this.user = new KioskUser();
    }

    public void emptyDetailPageRedirect() throws IOException {
        if (null != this.user.getId()) {
            System.out.println("User is not null ========> " + this.user);
            ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
            ec.redirect(ec.getRequestContextPath() + "/user/dashboard.xhtml");
        }
        resetDone = false;
    }

    public void userFromSession() {
        if (resetDone) {
            this.user = new KioskUser();
        } else {
            this.user = (KioskUser) JsfUtil.getHttpSession().getAttribute(ProjectConstants.SESSION_USER_CHANGE_PASSWORD);
        }
    }

    public void passwordChange() {
        String hashedPassword = persistence.hashPassword(this.currentPassword);
        KioskUser ku = persistence.getKioskUserByEmailAddressAndPassword(this.user.getEmailAddress(), hashedPassword);
        if (ku == null) {
            JsfUtil.addErrorFacesMessage("Current Password is not Correct!!!");
            return;
        }

        this.user.setModifiedDate(new Date());
        this.user.setPassword(persistence.hashPassword(this.user.getPassword()));
        persistence.update(this.user);
        JsfUtil.addInfoFacesMessage("Your Password Change was Successful.");
    }

    public void changePassword() {
        try {
            if (this.user != null) {
                String hashedPassword = persistence.hashPassword(this.user.getPassword());

                this.user.setPassword(hashedPassword);
                this.user.setModifiedDate(new Date());
                persistence.update(this.user);

                String code = (String) JsfUtil.getHttpSession().getAttribute(ProjectConstants.CHANGE_PASSWORD_CODE);
                PasswordChange pc = portalPersistence.getPasswordChangeByCode(code);
                pc.setCompletedTime(new Date());
                persistence.update(pc);

                JsfUtil.getHttpSession().removeAttribute(ProjectConstants.SESSION_USER_CHANGE_PASSWORD);
                JsfUtil.getHttpSession().removeAttribute(ProjectConstants.CHANGE_PASSWORD_CODE);
                JsfUtil.addInfoFacesMessage("Your Password Change was Successful.");
                resetDone = true;
            } else {
                Faces.setSessionAttribute(ProjectConstants.SESSION_USER_CHANGE_PASSWORD, this.user);
                JsfUtil.addErrorFacesMessage(ProjectConstants.OPERATION_UNSUCCESSFUL);
            }
        } catch (Exception e) {
            JsfUtil.addErrorFacesMessage(ProjectConstants.OPERATION_UNSUCCESSFUL);
            e.getLocalizedMessage();
            e.printStackTrace();
        }

    }

    public void checkEmailForPasswordReset() {
        try {
            KioskUser u = persistence.getKioskUserByEmailAddress(this.user.getEmailAddress());
            if (u != null) {
                String code = UUID.randomUUID().toString().substring(0, 8).toUpperCase();
                Calendar cal = Calendar.getInstance();
                cal.setTime(new Date());
                cal.add(Calendar.DATE, 1);
                Date expireDate = cal.getTime();

                PasswordChange pc = new PasswordChange();
                pc.setChangeCode(code);
                pc.setKioskUser(u);
                pc.setRequestTime(new Date());
                pc.setRequestExpirationTime(expireDate);

                persistence.create(pc);

                URI contextUrl = URI.create(JsfUtil.getHttpServletRequest().getRequestURL().toString()).resolve(JsfUtil.getHttpServletRequest().getContextPath());
                StringBuilder s = new StringBuilder(contextUrl.toString());
                s.append("user/EmailResponse?emailAddress=").append(this.user.getEmailAddress().trim()).append("&code=").append(code);

                String message = "Dear " + this.user.getFirstName() + " " + this.user.getSurname() + "  <br />"
                        + "You requested for a password reset in your Chef Tent account. "
                        + "If this was a mistake, just ignore this email and nothing will happen. To reset your password, click on this:"
                        + s + "</a>"
                        + "<br />"
                        + "Thank you!";
                emailService.prepareEmail(message, ProjectConstants.PASSWORD_CHARGE_SUBJECT, this.user.getEmailAddress().trim(), null, null);
                JsfUtil.getHttpSession().removeAttribute(ProjectConstants.SESSION_USER_CHANGE_PASSWORD);
                JsfUtil.addInfoFacesMessage("An email has been sent to you. Check your mail box to proceed further.");
                this.user = new KioskUser();
            } else {
                JsfUtil.addErrorFacesMessage("The email does not exist");
            }
        } catch (Exception e) {
            JsfUtil.addErrorFacesMessage(ProjectConstants.OPERATION_UNSUCCESSFUL);
            e.getLocalizedMessage();
            e.printStackTrace();
        }
    }

    public void signup() {
        try {
            //KioskUser u = persistence.getKioskUserByEmailAddress(this.user.getEmailAddress());
            KioskUser u = persistence.getKioskUserByEmailOrPhoneNumber(this.user.getEmailAddress(), this.user.getPhoneNumber());
            if (u != null) {
                String message = u.getEmailAddress().equalsIgnoreCase(this.user.getEmailAddress()) ? ProjectConstants.EMAIL_DUPLICATE : ProjectConstants.PHONENUMBER_DUPLICATE;
                JsfUtil.addWarningFacesMessage(message);
                //  this.user = new KioskUser();
            } else {
                String hashedPassword = persistence.hashPassword(this.user.getPassword());

                this.user.setPassword(hashedPassword);
                this.user.setCreatedDate(new Date());
                this.user.setModifiedDate(new Date());
                this.user.setActive(true);
                this.user.setToken(ProjectConstants.generateSHA256Token(this.user.getEmailAddress()));
                persistence.create(this.user);
                Faces.setSessionAttribute(ProjectConstants.SESSION_KIOSK_USER, user);

                FacesContext.getCurrentInstance().getExternalContext().redirect(JsfUtil.getServletContext().getContextPath() + "/user/dashboard.xhtml");
                //FacesContext.getCurrentInstance().getExternalContext().redirect(JsfUtil.getServletContext().getContextPath() + "/user/dashboard.xhtml");
                JsfUtil.addInfoFacesMessage(ProjectConstants.ACCOUNT_CREATED);
            }
        } catch (IOException e) {
            JsfUtil.addErrorFacesMessage(ProjectConstants.OPERATION_UNSUCCESSFUL);
            e.getLocalizedMessage();
            this.user = new KioskUser();
        }
    }

    public void updateKioskUser() {
        try {
            this.user.setModifiedDate(new Date());
            persistence.update(this.user);
        } catch (Exception e) {
            JsfUtil.addErrorFacesMessage(ProjectConstants.OPERATION_UNSUCCESSFUL);
            e.getLocalizedMessage();
        }
    }

    public void login() {
        // FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
        try {
            // KioskUser u = persistence.getKioskUserByEmailAddressAndPassword(this.user.getEmailAddress(), this.user.getPassword());
            KioskUser u = persistence.login(this.user.getEmailAddress(), this.user.getPassword());

            if (u != null && u.getId() != null) {
                this.user = u;
                HttpSession session = SessionUtils.getSession();
                session.setAttribute(ProjectConstants.SESSION_KIOSK_USER, u);
                //Faces.setSessionAttribute("user", u);

                this.portalUser = persistence.getPortalUserByKioskUser(u.getId());
                System.out.println("Portal User Login Detail ::::: " + this.portalUser);

                if (null != this.portalUser) {
                    session.setAttribute(ProjectConstants.SESSION_PORTAL_USER, this.portalUser);
                    PortalSuperAdminUser adminUser = persistence.getSuperAdminUserByPortalUser(portalUser.getId());
                    if (adminUser != null) {
                        session.setAttribute(ProjectConstants.SESSION_SUPER_PORTAL_USER, adminUser);
                    }
                }

                String redirectsource = (String) SessionUtils.getSession().getAttribute(ProjectConstants.SESSION_REDIRECT_SOURCE);
                System.out.println("Login Controller Source URL :::::::::::::::::::: " + redirectsource);
                System.out.println("login controller kiosk user session ===========>>> " + session.getAttribute(ProjectConstants.SESSION_KIOSK_USER));
                System.out.println("login controller portal user session ===========>>> " + session.getAttribute(ProjectConstants.SESSION_PORTAL_USER));
                if (redirectsource != null) {
                    FacesContext.getCurrentInstance().getExternalContext().redirect(redirectsource);
                } else {
                    FacesContext.getCurrentInstance().getExternalContext().redirect(JsfUtil.getServletContext().getContextPath() + "/user/dashboard.xhtml");
                    //FacesContext.getCurrentInstance().getExternalContext().redirect(JsfUtil.getServletContext().getContextPath() + "/user/dashboard.xhtml");
                }

                UserSessionPojo pojo = new UserSessionPojo();
                pojo.setEmailAddress(user.getEmailAddress());
                pojo.setIpAddress(Faces.getRequest().getRemoteAddr());
                pojo.setSessionid(session.getId());
                persistUserSessionDetail(pojo);

            } else {
                JsfUtil.addWarningFacesMessage(ProjectConstants.WRONG_CREDENTIAL);
                //FacesContext.getCurrentInstance().getExternalContext().redirect(null);
            }
        } catch (IOException e) {
            JsfUtil.addWarningFacesMessage(ProjectConstants.GENERIC_ERROR);
            e.getLocalizedMessage();
        }
    }

    public void logout() {
        try {
            JsfUtil.getHttpSession().removeAttribute(ProjectConstants.SESSION_KIOSK_USER);
            JsfUtil.getHttpSession().removeAttribute(ProjectConstants.SESSION_PORTAL_USER);
            JsfUtil.getHttpSession().removeAttribute(ProjectConstants.SESSION_SUPER_PORTAL_USER);
            JsfUtil.getHttpSession().removeAttribute(ProjectConstants.SESSION_PERMISSION_PARAM);
            JsfUtil.getHttpSession().removeAttribute(ProjectConstants.SESSION_PORTAL_MENU_PARAM);
            FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
            FacesContext.getCurrentInstance().getExternalContext().redirect(
                    JsfUtil.getHttpServletRequest().getScheme()
                    + "://" + JsfUtil.getHttpServletRequest().getServerName()
                    + ":" + JsfUtil.getHttpServletRequest().getServerPort());
            //FacesContext.getCurrentInstance().getExternalContext().redirect(JsfUtil.getServletContext().getContextPath() + "/signin.xhtml");
        } catch (IOException ex) {
            System.err.println("Error Occured !!!!!!!!!!!!!!!! " + ex.getMessage());
        }
    }

    public void resetCredential() {
        //If there is a Red Hat account associated with this email address, we've sent your login to the email address you've entered.
    }

    public void getBusinessIdentity() {
        if (this.user != null) {
            Business b = null;
            List<Business> list = portalPersistence.getAllBusinessByUserId(this.user.getId());
            if (!list.isEmpty()) {
                for (Business b1 : list) {
                    b = b1;
                    break;
                }
            } else {
                BusinessStaff bs = persistence.getBusinessStaffByKioskUser(this.user.getId());
                if (bs != null) {
                    KioskUser k = bs.getCreator();
                    List<Business> list2 = portalPersistence.getAllBusinessByUserId(k.getId());
                    for (Business b1 : list2) {
                        b = b1;
                        break;
                    }
                }
            }
            if (b != null) {
                businessName = b.getName();
                businessLogo = b.getLogo();
//                if (b.getLogoName() != null && b.getLogoPath() != null) {
//                    try (FileInputStream fis = new FileInputStream(new File(b.getLogoPath(), b.getLogoName()))) {
//                        this.businessLogo = Utils.toByteArray(fis);
//                        fis.close();
//                    } catch (Exception e) {
//                        System.err.println("Error occured while load file ::: " + e.getLocalizedMessage());
//                    }
//                }
            } else {
                businessName = "Chef Tent";
                businessLogo = null;
            }
        }
    }

    @Asynchronous
    private void persistUserSessionDetail(UserSessionPojo sessionPojo) {
        try {
            PortalUserSessionLog log = new PortalUserSessionLog();
            log.setActive(true);
            log.setDateCreated(new Date());
            log.setIpaddress(sessionPojo.getIpAddress());
            log.setSessionid(sessionPojo.getSessionid());
            log.setUserEmail(sessionPojo.getEmailAddress());
            persistence.create(log);
            System.out.println("PortalUserSessionLog has been logged !!!!!");
        } catch (Exception e) {
            e.getLocalizedMessage();
        }
    }

    public PortalUser getPortalUser() {
        return portalUser;
    }

    public void setPortalUser(PortalUser portalUser) {
        this.portalUser = portalUser;
    }

    public List<PortalRole> getUserPortalRoles() {
        return userPortalRoles;
    }

    public void setUserPortalRoles(List<PortalRole> userPortalRoles) {
        this.userPortalRoles = userPortalRoles;
    }

    public KioskUser getUser() {
        return user;
    }

    public void setUser(KioskUser user) {
        this.user = user;
    }

    public BusinessStaff getBusinessStaff() {
        return businessStaff;
    }

    public void setBusinessStaff(BusinessStaff businessStaff) {
        this.businessStaff = businessStaff;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public byte[] getBusinessLogo() {
        return businessLogo;
    }

    public void setBusinessLogo(byte[] businessLogo) {
        this.businessLogo = businessLogo;
    }

    public boolean isResetDone() {
        return resetDone;
    }

    public void setResetDone(boolean resetDone) {
        this.resetDone = resetDone;
    }

    public String getCurrentPassword() {
        return currentPassword;
    }

    public void setCurrentPassword(String currentPassword) {
        this.currentPassword = currentPassword;
    }

}
