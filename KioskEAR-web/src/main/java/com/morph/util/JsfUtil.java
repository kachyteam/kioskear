/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.morph.util;

import java.security.SecureRandom;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import javax.el.ExpressionFactory;
import javax.el.ValueExpression;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Tobi-Morph-PC
 */
public class JsfUtil {

    private static void addFacesMessage(String componentId, String summary, String detail) {
        UIComponent root = FacesContext.getCurrentInstance().getViewRoot();
        UIComponent component = root.findComponent(componentId);
        FacesContext.getCurrentInstance().addMessage(component.getClientId(FacesContext.getCurrentInstance()),
                new FacesMessage(FacesMessage.SEVERITY_ERROR, summary, detail));
    }

    public static HttpSession getHttpSession() {
        FacesContext facescontext = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) facescontext.getExternalContext().getSession(true);
        return session;
    }

    public static HttpServletResponse getHttpServletResponse() {
        FacesContext facescontext = FacesContext.getCurrentInstance();
        HttpServletResponse response = (HttpServletResponse) facescontext.getExternalContext().getResponse();
        return response;
    }

    public static HttpServletRequest getHttpServletRequest() {
        FacesContext facescontext = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest) facescontext.getExternalContext().getRequest();
        return request;
    }

    public static ServletContext getServletContext() {
        ServletContext context = getHttpSession().getServletContext();
        return context;
    }

    private static void addFacesMessage(FacesMessage message) {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, message);
    }

    public static void addInfoFacesMessage(String summary) {
        addFacesMessage(new FacesMessage(FacesMessage.SEVERITY_INFO, summary, summary));
        System.out.println(summary);
    }

    public static void addWarningFacesMessage(String summary) {
        addFacesMessage(new FacesMessage(FacesMessage.SEVERITY_WARN, summary, summary));
        System.out.println(summary);
    }

    public static void addErrorFacesMessage(String summary) {
        addFacesMessage(new FacesMessage(FacesMessage.SEVERITY_ERROR, summary, summary));
        System.err.println(summary);
    }

    /**
     * Reset Messages On Page
     */
    public static void resetFacesMessages() {
        FacesContext context = FacesContext.getCurrentInstance();
        Iterator iter = context.getMessages();
        while (iter.hasNext()) {
            iter.next();
            iter.remove();
        }
    }
    
   
    private static Object getBackingBean(String name) {
        Object bean = getHttpSession().getAttribute(name);
        if (bean == null) {
            FacesContext facescontext = FacesContext.getCurrentInstance();
            bean = facescontext.getApplication().createValueBinding("#{" + name + "}").getValue(facescontext); //$NON-NLS-1$ //$NON-NLS-2$
        }
        return bean;
    }

    static Map<String, String> pageMap = new HashMap<>();

    static {
        pageMap.put("register", "/registerUser");
    }

    public Object getObjectInSession(String beanSessionRep) {
        // Get a session bean  
        FacesContext ctx = FacesContext.getCurrentInstance();
        ExpressionFactory ef = ctx.getApplication().getExpressionFactory();
        //ValueExpression ve = ef.createValueExpression(ctx.getELContext(), "#{testSessionBean}", Object.class);  
        ValueExpression ve = ef.createValueExpression(ctx.getELContext(), beanSessionRep, Object.class);
        Object test = ve.getValue(ctx.getELContext());
        return test;
    }

    public static boolean testObject(Object obj) {
        if (obj == null) {
            throw new IllegalArgumentException("object was null");
        }
        return false;
    }

    static final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    static SecureRandom rnd = new SecureRandom();

    public static String randomString(int len) {
        StringBuilder sb = new StringBuilder(len);
        for (int i = 0; i < len; i++) {
            sb.append(AB.charAt(rnd.nextInt(AB.length())));
        }
        return sb.toString();
    }

}