/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.morph.util;

import com.morph.kiosk.persistence.entity.KioskUser;
import com.morph.kiosk.persistence.entity.portal.PortalAccessGroup;
import com.morph.kiosk.persistence.entity.portal.PortalAccessGroupPortalRole;
import com.morph.kiosk.persistence.entity.portal.PortalRequest;
import com.morph.kiosk.persistence.entity.portal.PortalRole;
import com.morph.kiosk.persistence.entity.portal.PortalUser;
import com.morph.kiosk.persistence.entity.portal.PortalUserAccessGroup;
import com.morph.kiosk.persistence.service.PortalPersistence;
import com.morph.kiosk.persistence.service.UserPersistence;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;

/**
 *
 * @author Tobi-Morph-PC
 */
public class UserPageManager extends PageManager{

   @Resource(lookup = "java:global/KioskEJBModule-1.2/UserPersistence!com.morph.kiosk.persistence.service.UserPersistence")
    private UserPersistence persistence;
    
    @Resource(lookup = "java:global/KioskEJBModule-1.2/PortalPersistence!com.morph.kiosk.persistence.service.PortalPersistence")
    private PortalPersistence portalPersistence;
    
    public UserPageManager(KioskUser user) {
        super(user);
    }

    @Override
    public List<String> getUserPages() {
        List<PortalAccessGroup> groups = new ArrayList<>();
        PortalUser portalUser = persistence.getPortalUserByKioskUser(user.getId());
        //retrieve portal user business request and if not null use request's request type to retrieve the portal access group
        PortalRequest request = portalPersistence.getUserRequest(portalUser.getId());
        if (request != null && request.isActive() != false) {
            PortalAccessGroup group = portalPersistence.getPortalAccessGroupByPortalRequestType(request.getRequestType().getId());
            groups.add(group);
        }
        //retrieve user's direct access group and retrieve the portal roles and add to the one retrieve earlier
//        List<PortalUserAccessGroup> puags = persistence.getPortalUserAccessGroupByUser(user.getId());
//        for (PortalUserAccessGroup puag : puags) {
//            if (!groups.contains(puag.getAccessGroup())) {
//                groups.add(puag.getAccessGroup());
//            }
//        }

        List<PortalRole> userPortalRoles = new ArrayList<>();
        //retrieve all roles associated with groups
        for (PortalAccessGroup group : groups) {
            List<PortalAccessGroupPortalRole> pagprs = portalPersistence.getAccessGroupPortalRolesByAccessGroupId(group.getId());
            if (!pagprs.isEmpty()) {
                for (PortalAccessGroupPortalRole pagpr : pagprs) {
                    userPortalRoles.add(pagpr.getRole());
                }
            }
        }

        List<String> userPortalPages = new ArrayList<>();
        if (!userPortalPages.isEmpty()) {
            for (PortalRole role : userPortalRoles) {
                userPortalPages.add(portalPersistence.getPortalPagesNameByRole(role.getId()));
            }
        }

        return userPortalPages;
    }
    
}
