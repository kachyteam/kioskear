/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.morph.util;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Tobi-Morph-PC
 */
public class PortalLogger extends Logger{

    public PortalLogger(String name, String resourceBundleName) {
        super(name, resourceBundleName);
    }
    
    public void error(String msg){
        super.log(Level.SEVERE, msg);
    }
    
    public void warn(String msg){
        super.log(Level.WARNING, msg);
    }
    
    //@Override
    public void log(String msg){
        super.log(Level.INFO, msg);
    }
    
    public void custom(String level_desc, int level_int, String msg){
        super.log(new CustomLevel(level_desc, level_int), msg);
    }
}
