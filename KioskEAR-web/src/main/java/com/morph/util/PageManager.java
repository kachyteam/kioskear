/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.morph.util;

import com.morph.kiosk.persistence.entity.KioskUser;
import java.util.List;

/**
 *
 * @author Tobi-Morph-PC
 */
public abstract class PageManager {

    
    KioskUser user;

    public PageManager(KioskUser user) {
        this.user = user;
    }

    abstract List<String> getUserPages();
}
