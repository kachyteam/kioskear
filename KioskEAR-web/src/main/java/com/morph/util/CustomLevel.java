/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.morph.util;

import java.util.logging.Level;

/**
 *
 * @author Tobi-Morph-PC
 */
public class CustomLevel extends Level{

    public CustomLevel(String name, int value) {
        super(name, value);
    }

    public CustomLevel() {
        super("ERROR", 404);
    }
}
