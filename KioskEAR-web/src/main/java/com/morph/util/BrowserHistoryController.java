/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.morph.util;

import java.io.Serializable;
import java.util.Stack;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Tobi-Morph-PC
 */
@Named("browserHistoryController")
@SessionScoped
public class BrowserHistoryController implements Serializable{

    private final Logger logger = Logger.getLogger(getClass().getName());
    
    private static final long serialVersionUID = 1L;
    
    private String back;

    private Stack<String> pageStack = new Stack<>();
    private final Integer maxHistoryStackSize = 20;

    public void addPageUrl() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        if (!facesContext.isPostback() && !facesContext.isValidationFailed()) {
            HttpServletRequest servletRequest = (HttpServletRequest) facesContext.getExternalContext().getRequest();
            String fullUrl = servletRequest.getRequestURL() + "?" + servletRequest.getQueryString();
            updatePageStack(fullUrl);
        }
    }

    public String returnBackUrl() {
        logger.info("Back url called........  ");
        Integer stackSize = pageStack.size();
        if (stackSize > 1) {
            logger.fine(pageStack.get(stackSize - 2));
            return pageStack.get(stackSize - 2);
        }
        // Just in case hasPageBack was not implemented (be safe)
        logger.fine(pageStack.get(stackSize - 1));
        return pageStack.get(stackSize - 1);
    }

    public Stack<String> getPageStack() {
        return pageStack;
    }

    public boolean hasPageBack() {
        return pageStack.size() > 1;
    }

    public void setPageStack(Stack<String> pageStack) {
        this.pageStack = pageStack;
    }

    private void updatePageStack(String navigationCase) {
        logger.log(Level.INFO, "updating navigation stack {0}", navigationCase);
        Integer stackSize = pageStack.size();

        // If stack is full, then make room by removing the oldest item
        if (stackSize >= maxHistoryStackSize) {
            pageStack.remove(0);
            stackSize = pageStack.size();
        }

        // If the first page visiting, add to stack
        if (stackSize == 0) {
            pageStack.push(navigationCase);
            return;
        }

        // If it appears the back button has been pressed, in other words:
        // If the A -> B -> C, and user navigates from C -> B, then remove C
        if (stackSize > 1 && pageStack.get(stackSize - 2).equals(navigationCase)) {
            pageStack.remove(stackSize - 1);
            return;
        }

        // If we are on the same page
        // If A == A then ignore
        if (pageStack.get(stackSize - 1).equals(navigationCase)) {
            return;
        }

        // In a normal case, we add the item to the stack
        if (stackSize >= 1) {
            pageStack.push(navigationCase);
        }

    }

    public String getBack() {
        logger.log(Level.INFO, "Returning back url ::: {0}", returnBackUrl());
        return back;
    }

    public void setBack(String back) {
        this.back = back;
    }

    
}
