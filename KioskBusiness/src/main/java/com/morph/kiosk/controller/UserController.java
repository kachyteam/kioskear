/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.morph.kiosk.controller;

import com.morph.kiosk.persistence.entity.KioskUser;
import com.morph.kiosk.persistence.service.UserPersistence;
import com.morph.kiosk.util.JsfUtil;
import com.morph.util.ProjectConstants;
import java.io.IOException;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.Date;
import javax.annotation.Resource;
import javax.faces.context.FacesContext;

/**
 *
 * @author Tobi-Morph-PC
 */
@Named(value = "userController")
@SessionScoped
public class UserController extends BaseController implements Serializable {
    @Resource(lookup = "java:global/KioskEJBModule-1.2/UserPersistence!com.morph.kiosk.persistence.service.UserPersistence")
    private UserPersistence persistence;
    /**
     * Creates a new instance of UserController
     */
    
    private KioskUser user;
    
    private String currentPassword;
    
    public UserController() {
    }

    @Override
    void initUser(KioskUser user) {
        this.user = user;
    }
    
    public void loadUser(){
        System.out.println("Business User :::::: " + this.user);
    }

    public String getCurrentPassword() {
        return currentPassword;
    }

    public void setCurrentPassword(String currentPassword) {
        this.currentPassword = currentPassword;
    }
    
    public void passwordChange() {
        String hashedPassword = persistence.hashPassword(this.currentPassword);
        KioskUser ku = persistence.getKioskUserByEmailAddressAndPassword(this.user.getEmailAddress(), hashedPassword);
        if (ku == null) {
            JsfUtil.addErrorFacesMessage("Current Password is not Correct!!!");
            return;
        }

        this.user.setModifiedDate(new Date());
        this.user.setPassword(persistence.hashPassword(this.user.getPassword()));
        persistence.update(this.user);
        JsfUtil.addInfoFacesMessage("Your Password Change was Successful.");
    }
    
    public void logout() {
        try {
            JsfUtil.getHttpSession().removeAttribute(ProjectConstants.SESSION_KIOSK_USER);
            JsfUtil.getHttpSession().removeAttribute(ProjectConstants.SESSION_PORTAL_USER);
            JsfUtil.getHttpSession().removeAttribute(ProjectConstants.SESSION_PERMISSION_PARAM);
            JsfUtil.getHttpSession().removeAttribute(ProjectConstants.SESSION_PORTAL_MENU_PARAM);
            FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
            FacesContext.getCurrentInstance().getExternalContext().redirect(
                    JsfUtil.getHttpServletRequest().getScheme() + 
                            "://" + JsfUtil.getHttpServletRequest().getServerName() + 
                            ":" + JsfUtil.getHttpServletRequest().getServerPort());
            //FacesContext.getCurrentInstance().getExternalContext().redirect(JsfUtil.getServletContext().getContextPath() + "/signin.xhtml");
        } catch (IOException ex) {
            System.err.println("Error Occured !!!!!!!!!!!!!!!! " + ex.getMessage());
        }
    }
    

    public KioskUser getUser() {
        return user;
    }

    public void setUser(KioskUser user) {
        this.user = user;
    }
    
    
}
