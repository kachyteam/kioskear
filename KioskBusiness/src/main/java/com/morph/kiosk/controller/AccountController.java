/**
 * Class Name: AccountController Project Name: KioskBusiness Developer: Onyedika
 * Okafor (ookafor@morphinnovations.com) Version Info: Create Date: Dec 18, 2016
 * 3:03:46 PM (C)Morph Innovations Limited 2016. Morph Innovations Limited
 * Asserts its right to be known as the author and owner of this file and its
 * contents.
 */
package com.morph.kiosk.controller;

import com.morph.kiosk.persistence.entity.Business;
import com.morph.kiosk.persistence.entity.Kiosk;
import com.morph.kiosk.persistence.entity.KioskUser;
import com.morph.kiosk.persistence.entity.order.KioskPaymentOption;
import com.morph.kiosk.persistence.entity.order.KioskPaymentOption.Option;
import com.morph.kiosk.persistence.entity.order.PaymentTransaction;
import com.morph.kiosk.persistence.entity.order.PaymentTransaction.PaymentTractionStatus;
import com.morph.kiosk.persistence.entity.portal.BusinessStaffRole;
import com.morph.kiosk.persistence.service.PortalPersistence;
import java.io.IOException;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import javax.annotation.Resource;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import com.lowagie.text.BadElementException;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.morph.kiosk.persistence.entity.order.Coupon;
import com.morph.kiosk.persistence.entity.order.OrderedItem;
import com.morph.kiosk.persistence.entity.payment.Settlement;
import com.morph.kiosk.persistence.entity.payment.SplitLog;
import com.morph.kiosk.util.JsfUtil;
import com.morph.util.ProjectConstants;
import java.awt.Color;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;

/**
 * @author Onyedika Okafor (ookafor@morphinnovations.com)
 */
@Named(value = "accountController")
@ViewScoped
public class AccountController extends BaseController implements Serializable {

    @Resource(lookup = "java:global/KioskEJBModule-1.2/PortalPersistence!com.morph.kiosk.persistence.service.PortalPersistence")
    private PortalPersistence persistence;

    public AccountController() {
    }

    private KioskUser user = new KioskUser();
    private Business business;
    private Kiosk kiosk;
    private Settlement settlement;
    private List<Business> businesses = new ArrayList<>();
    private List<Kiosk> kiosks = new ArrayList<>();
    private List<Settlement> settlements = new ArrayList<>();
    private List<PaymentTransaction> tranx = new ArrayList<>();
    private List<SplitLog> splitLogs = new ArrayList<>();
    private List<PaymentTractionStatus> transactionStatus = new ArrayList<>();
    private List<Option> paymentModes = new ArrayList<>();
    private Date tranxDate1;
    private Date tranxDate2;
    private int tranxAmount;
    private PaymentTractionStatus paymentStatus;
    private Option paymentMode;
    private String orderId;
    private String transactionRef;
    private Boolean showAccountProceedButton = false;
    private String iquery;

    @Override
    void initUser(KioskUser user) {
        this.user = user;
    }

    public void populateBusiness() {
        businesses.clear();
        List<Business> businessz = persistence.geAllActiveBusinessByUser(user.getId());
        List<BusinessStaffRole> bsrs = persistence.getUserRole(user, "Accountant");
        for (BusinessStaffRole bsr : bsrs) {
            businessz.add(bsr.getBusiness());
        }
        businesses.addAll(new HashSet<>(businessz));
    }

    public void showProceedButton() {
        showAccountProceedButton = false;
        if (business != null) {
            showAccountProceedButton = true;
        }
    }

    public void loadKiosk() {
        kiosks.clear();
        kiosks = persistence.getAllKioskByBusinessId(business != null ? business.getId() : null);
    }

    public void loadTransactionStatus() {
        transactionStatus = Arrays.asList(PaymentTransaction.PaymentTractionStatus.values());
    }

    public void loadPaymentMode() {
        paymentModes = Arrays.asList(KioskPaymentOption.Option.values());
    }

    public void loadBusiness() {
        if (null == iquery || "".equalsIgnoreCase(iquery)) {
            JsfUtil.addErrorFacesMessage(ProjectConstants.PAGE_NOT_FOUND);
            throw new IllegalArgumentException(ProjectConstants.PAGE_NOT_FOUND);
        } else {
            this.business = persistence.find(Business.class, Long.valueOf(iquery));

            if (this.business == null) {
                JsfUtil.addErrorFacesMessage(ProjectConstants.PAGE_NOT_FOUND);
            }

        }

    }

    public void filterSales() {
        tranx.clear();
        tranx.addAll(persistence.getAllSuccessfulTransactionByBusiness(business));
        List<PaymentTransaction> filtered = new ArrayList<>();

        if (tranxDate1 != null && tranxDate2 != null) {
            Long date1 = tranxDate1.getTime();
            Long date2 = tranxDate2.getTime();
            for (PaymentTransaction pt : tranx) {
                if (pt.getTranxDate() != null) {
                    Long ptDate = pt.getTranxDate().getTime();
                    if (ptDate >= date1 && ptDate <= date2) {
                        filtered.add(pt);
                    }
                }
            }
            tranx.clear();
            tranx.addAll(filtered);
            filtered.clear();
        }

        if (transactionRef != null) {
            for (PaymentTransaction pt : tranx) {
                if (pt.getTransactionRef().equalsIgnoreCase(transactionRef)) {
                    filtered.add(pt);
                }
            }
            tranx.clear();
            tranx.addAll(filtered);
            filtered.clear();
        }

        if (tranxDate1 == null && tranxDate2 == null & transactionRef == null) {
            Long date1 = upperTime().getTimeInMillis();
            Long date2 = lowerTime().getTimeInMillis();

            for (PaymentTransaction pt : tranx) {
                if (pt.getTranxDate() != null) {
                    Long ptDate = pt.getTranxDate().getTime();
                    if (ptDate >= date1 && ptDate <= date2) {
                        filtered.add(pt);
                    }
                }
            }

            tranx.clear();
            tranx.addAll(filtered);
        }

    }

//    public static Date dateConverter(D sDate) {
//        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
//        Date date = null;
//        try {
//            date = formatter.parse(sDate);
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//        return date;
//    }
    public String transactionTotal() {
        int total = 0;

        for (PaymentTransaction pt : tranx) {
            total += pt.getAmount();
        }
        return new DecimalFormat("###,###.###").format(total);
    }

    public String generateFileName(String fileName) {
        Long d = new Date().getTime();
        String s = fileName + "-" + d.toString();
        return s;
    }

    private static void addEmptyLine(Paragraph paragraph, int number) {
        for (int i = 0; i < number; i++) {
            paragraph.add(new Paragraph(" "));
        }
    }

    public Document createNewPDFDoc(Object document) {
        Document pdf = (Document) document;
        pdf.setPageSize(PageSize.A4.rotate());
        pdf.open();
        return pdf;
    }

    public void includePDFSubtitle(Paragraph title, String page) {
        Font subTitleFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 16, Font.BOLD, Color.BLACK);
        Font dateFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 12, Font.BOLD, Color.BLACK);
        Font titleFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 18, Font.BOLD, Color.BLACK);

        SimpleDateFormat sdf = new SimpleDateFormat("MMMM dd, yyyy");
        if (business != null) {
            try {
                if (business.getLogo() != null) {
                    Image logo = Image.getInstance(business.getLogo());
                    logo.scalePercent(25);
                    // logo.setAlignment(Element.ALIGN_MIDDLE);
                    title.add(logo);
                }
            } catch (BadElementException | IOException ex) {
                Logger.getLogger(AccountController.class.getName()).log(Level.SEVERE, null, ex);
            }

            // addEmptyLine(title, 1);
            Paragraph p = new Paragraph(business.getName(), subTitleFont);
            // p.setAlignment(Element.ALIGN_MIDDLE);
            title.add(p);
        }

        addEmptyLine(title, 1);
        title.add(new Paragraph("Date: " + sdf.format(new Date()), dateFont));

        String dateRange = "";
        if (tranxDate1 != null && tranxDate2 != null) {
            dateRange = new StringBuilder(": " + sdf.format(tranxDate1) + " - " + sdf.format(tranxDate2)).toString();
        }
        addEmptyLine(title, 2);
        title.add(new Paragraph(page + dateRange, titleFont));
    }

    public void preProcessSettlementPDF(Object document) throws IOException, BadElementException, DocumentException {
        Document pdf = createNewPDFDoc(document);
        Paragraph title = new Paragraph();
        includePDFSubtitle(title, "Settlement");
        addEmptyLine(title, 1);
        pdf.add(title);
    }

    public void preProcessPaymentPDF(Object document) throws IOException, BadElementException, DocumentException {
        Document pdf = createNewPDFDoc(document);
        Paragraph title = new Paragraph();
        includePDFSubtitle(title, "Payment");
        addEmptyLine(title, 1);
        pdf.add(title);
    }
    
    public void preProcessCouponOrderPDF(Object document) throws IOException, BadElementException, DocumentException {
        Document pdf = createNewPDFDoc(document);
        Paragraph title = new Paragraph();
        includePDFSubtitle(title, "CouponOrder");
        addEmptyLine(title, 1);
        pdf.add(title);
    }

    public void preProcessSalesPDF(Object document) throws IOException, BadElementException, DocumentException {
        Document pdf = createNewPDFDoc(document);
        Paragraph title = new Paragraph();
        includePDFSubtitle(title, "Sales");
        addEmptyLine(title, 1);
        pdf.add(title);
    }

    public void postProcessXLS(Object document) {
        SimpleDateFormat sdf = new SimpleDateFormat("MMMM dd, yyyy");
        String dateRange = "";
        if (tranxDate1 != null && tranxDate2 != null) {
            dateRange = new StringBuilder(": " + sdf.format(tranxDate1) + " - " + sdf.format(tranxDate2)).toString();
        }
        HSSFWorkbook wb = (HSSFWorkbook) document;
        HSSFSheet sheet = wb.getSheetAt(0);
        sheet.shiftRows(0, sheet.getLastRowNum() + 3, 3);

        HSSFRow businessRow = sheet.getRow(0);
        businessRow.createCell(1).setCellValue("Business: " + business.getName());
        businessRow.createCell(2).setCellValue("Date: " + sdf.format(new Date()));
        if (!dateRange.isEmpty()) {
            businessRow.createCell(3).setCellValue("Date Range" + dateRange);
        }
        HSSFRow header = sheet.getRow(3);
        HSSFCellStyle cellStyle = wb.createCellStyle();
        cellStyle.setFillForegroundColor(HSSFColor.GREEN.index);
        cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);

        for (int i = 0; i < header.getPhysicalNumberOfCells(); i++) {
            HSSFCell cell = header.getCell(i);

            cell.setCellStyle(cellStyle);
        }
    }

    public String todaySettlementAmount() {
        List<Settlement> ses = persistence.getAllSettlementsByBusiness(business);
        Double amount = 0.0;

        Long date1 = upperTime().getTimeInMillis();
        Long date2 = lowerTime().getTimeInMillis();

        for (Settlement s : ses) {
            if (s.getCreatedDate() != null) {
                Long sDate = s.getCreatedDate().getTime();
                if (sDate >= date1 && sDate <= date2) {
                    amount += s.getAmountAfterSplit();
                }
            }
        }
        return formatCurrency(amount);
    }

    public String todaySettlement() {
        List<Settlement> ses = persistence.getAllSettlementsByBusiness(business);
        Integer count = 0;

        Long date1 = upperTime().getTimeInMillis();
        Long date2 = lowerTime().getTimeInMillis();

        for (Settlement s : ses) {
            if (s.getCreatedDate() != null) {
                Long sDate = s.getCreatedDate().getTime();
                if (sDate >= date1 && sDate <= date2) {
                    count++;
                }
            }
        }
        return String.valueOf(count);
    }

    public String pendingSettlement() {
        List<Settlement> ses = persistence.getAllPendingSettlementsByBusiness(this.business);
        Integer count = ses.size();
        return String.valueOf(count);
    }

    public String pendingPayment() {
        List<Settlement> ses = persistence.getAllPendingSettlementsByBusiness(this.business);
        Double amount = 0.0;
        for (Settlement se : ses) {
            amount += se.getAmountAfterSplit();
        }
        return formatCurrency(amount);
    }

    public String paidToday() {
        List<Settlement> ses = persistence.getAllSettlementsByBusiness(this.business);
        Double amount = 0.0;

        Long date1 = upperTime().getTimeInMillis();
        Long date2 = lowerTime().getTimeInMillis();

        for (Settlement s : ses) {
            if (s.getPaymentDate() != null) {
                Long sDate = s.getPaymentDate().getTime();
                if (sDate >= date1 && sDate <= date2) {
                    if (s.getPaid()) {
                        amount += s.getAmountAfterSplit();
                    }
                }
            }
        }

        return formatCurrency(amount);

    }

    public String usedCouponAmount() {
        List<PaymentTransaction> pts = persistence.getAllSuccessfulTransactionByBusiness(this.business);
        Double amount = 0.0;
        for (PaymentTransaction pt : pts) {
            if (pt.getOrderItem().getPromoCode() != null) {
                amount += pt.getOrderItem().getPromotionDiscount();
            }
        }
        return formatCurrency(amount);
    }

    public String usedCouponCount() {
        List<PaymentTransaction> pts = persistence.getAllSuccessfulTransactionByBusiness(this.business);
        List<OrderedItem> items = new ArrayList<>();
        for (PaymentTransaction pt : pts) {
            if (pt.getOrderItem().getPromoCode() != null) {
                items.add(pt.getOrderItem());
            }
        }
        int count = items.size();
        return String.valueOf(count);
    }

    public String todayIncome() {

        List<PaymentTransaction> pts = persistence.getAllSuccessfulTransactionByBusiness(this.business);
        Double totalAmount = 0.0;

        Long date1 = upperTime().getTimeInMillis();
        Long date2 = lowerTime().getTimeInMillis();

        for (PaymentTransaction pt : pts) {
            if (pt.getTranxDate() != null) {
                Long ptDate = pt.getTranxDate().getTime();
                if (ptDate >= date1 && ptDate <= date2) {
                    totalAmount += pt.getAmount();
                }
            }
        }

        return formatCurrency(totalAmount);
    }

    private Calendar upperTime() {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        return c;
    }

    private Calendar lowerTime() {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, 23);
        c.set(Calendar.MINUTE, 59);
        c.set(Calendar.SECOND, 59);
        c.set(Calendar.MILLISECOND, 59);
        return c;
    }

    public void filterCouponTable() {
        tranx.clear();
        List<PaymentTransaction> pts = persistence.getAllSuccessfulTransactionByBusiness(this.business);
        List<PaymentTransaction> filteredTranx = new ArrayList<>();
        for (PaymentTransaction pt : pts) {
            if (pt.getOrderItem().getPromoCode() != null) {
                tranx.add(pt);
            }
        }
        
        if (kiosk != null) {
            for (PaymentTransaction pt : tranx) {
                if (pt.getOrderItem().getKiosk() == kiosk) {
                    filteredTranx.add(pt);
                }
            }
            tranx.clear();
            tranx.addAll(filteredTranx);
            filteredTranx.clear();
        }

        if (tranxDate1 != null) {
            try {
                DateFormat createdDateFormatter = new SimpleDateFormat("yyyy/MM/dd");

                String sDate = createdDateFormatter.format(tranxDate1);
                Date searchDate = createdDateFormatter.parse(sDate);

                for (PaymentTransaction pt : tranx) {
                    String cDate = createdDateFormatter.format(pt.getTranxDate());
                    Date createdDate = createdDateFormatter.parse(cDate);
                    if (searchDate.getTime() == createdDate.getTime()) {
                        filteredTranx.add(pt);
                    }
                }
            } catch (ParseException ex) {
                Logger.getLogger(PromotionController.class.getName()).log(Level.SEVERE, null, ex);
            }
            tranx.clear();
            tranx.addAll(filteredTranx);
            filteredTranx.clear();
        }
    }

    public void filterPaidTable(String accountStage) {
        this.settlements.clear();
        List<Settlement> ses = persistence.getAllSettlementsByBusiness(business);
        if (accountStage.equalsIgnoreCase("Paid")) {
            for (Settlement se : ses) {
                if (se.getPaid()) {
                    this.settlements.add(se);
                }
            }
            if (this.tranxDate1 != null && this.tranxDate2 != null) {
                List<Settlement> filtered = new ArrayList<>();
                Long date1 = this.tranxDate1.getTime();
                Long date2 = this.tranxDate2.getTime();
                for (Settlement s : this.settlements) {
                    if (s.getPaymentDate() != null) {
                        Long sDate = s.getPaymentDate().getTime();
                        if (sDate >= date1 && sDate <= date2) {
                            filtered.add(s);
                        }
                    }
                }
                this.settlements.clear();
                this.settlements.addAll(filtered);
            }
        }

        if (accountStage.equalsIgnoreCase("Pending")) {
            for (Settlement se : ses) {
                if (!se.getPaid()) {
                    this.settlements.add(se);
                }
            }

            if (this.tranxDate1 != null && this.tranxDate2 != null) {
                List<Settlement> filtered = new ArrayList<>();
                Long date1 = this.tranxDate1.getTime();
                Long date2 = this.tranxDate2.getTime();
                for (Settlement s : this.settlements) {
                    if (s.getSettledDate() != null) {
                        Long sDate = s.getSettledDate().getTime();
                        if (sDate >= date1 && sDate <= date2) {
                            filtered.add(s);
                        }
                    }
                }
                this.settlements.clear();
                this.settlements.addAll(filtered);
            }
        }

    }

    public void fetchTransaction(Settlement s) {
        this.tranx.clear();
        this.settlement = s;
        this.tranx = persistence.getTransactionBySettlement(s);
    }

    public void fetchSplitLog(Settlement s) {
        this.splitLogs.clear();
        this.settlement = s;
        this.splitLogs = persistence.getSplitLogBySettlement(s);
    }

    public String formatCurrency(Double amount) {
        double currency = 0.00;
        if (amount != null) {
            currency = amount;
        }
        return new DecimalFormat("###,###.###").format(currency);
    }

    public KioskUser getUser() {
        return user;
    }

    public void setUser(KioskUser user) {
        this.user = user;
    }

    public PortalPersistence getPersistence() {
        return persistence;
    }

    public void setPersistence(PortalPersistence persistence) {
        this.persistence = persistence;
    }

    public List<Business> getBusinesses() {
        return businesses;
    }

    public void setBusinesses(List<Business> businesses) {
        this.businesses = businesses;
    }

    public Business getBusiness() {
        return business;
    }

    public void setBusiness(Business business) {
        this.business = business;
    }

    public Kiosk getKiosk() {
        return kiosk;
    }

    public void setKiosk(Kiosk kiosk) {
        this.kiosk = kiosk;
    }

    public List<Kiosk> getKiosks() {
        return kiosks;
    }

    public void setKiosks(List<Kiosk> kiosks) {
        this.kiosks = kiosks;
    }

    public List<PaymentTransaction> getTranx() {
        return tranx;
    }

    public void setTranx(List<PaymentTransaction> tranx) {
        this.tranx = tranx;
    }

    public Date getTranxDate1() {
        return tranxDate1;
    }

    public void setTranxDate1(Date tranxDate1) {
        this.tranxDate1 = tranxDate1;
    }

    public Date getTranxDate2() {
        return tranxDate2;
    }

    public void setTranxDate2(Date tranxDate2) {
        this.tranxDate2 = tranxDate2;
    }

    public int getTranxAmount() {
        return tranxAmount;
    }

    public void setTranxAmount(int tranxAmount) {
        this.tranxAmount = tranxAmount;
    }

    public List<PaymentTractionStatus> getTransactionStatus() {
        return transactionStatus;
    }

    public void setTransactionStatus(List<PaymentTractionStatus> transactionStatus) {
        this.transactionStatus = transactionStatus;
    }

    public PaymentTractionStatus getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(PaymentTractionStatus paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getTransactionRef() {
        return transactionRef;
    }

    public void setTransactionRef(String transactionRef) {
        this.transactionRef = transactionRef;
    }

    public Option getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(Option paymentMode) {
        this.paymentMode = paymentMode;
    }

    public List<Option> getPaymentModes() {
        return paymentModes;
    }

    public void setPaymentModes(List<Option> paymentModes) {
        this.paymentModes = paymentModes;
    }

    public Boolean getShowAccountProceedButton() {
        return showAccountProceedButton;
    }

    public void setShowAccountProceedButton(Boolean showAccountProceedButton) {
        this.showAccountProceedButton = showAccountProceedButton;
    }

    public String getIquery() {
        return iquery;
    }

    public void setIquery(String iquery) {
        this.iquery = iquery;
    }

    public List<Settlement> getSettlements() {
        return settlements;
    }

    public void setSettlements(List<Settlement> settlements) {
        this.settlements = settlements;
    }

    public List<SplitLog> getSplitLogs() {
        return splitLogs;
    }

    public void setSplitLogs(List<SplitLog> splitLogs) {
        this.splitLogs = splitLogs;
    }

    public Settlement getSettlement() {
        return settlement;
    }

    public void setSettlement(Settlement settlement) {
        this.settlement = settlement;
    }

}
