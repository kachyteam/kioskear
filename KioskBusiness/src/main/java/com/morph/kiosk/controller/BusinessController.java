/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.morph.kiosk.controller;

import com.morph.kiosk.util.JsfUtil;
import com.morph.kiosk.persistence.entity.Business;
import com.morph.kiosk.persistence.entity.BusinessActivityLog;
import com.morph.kiosk.persistence.entity.KioskUser;
import com.morph.kiosk.persistence.entity.payment.PaymentProvider;
import com.morph.kiosk.persistence.entity.portal.BusinessStaff;
import com.morph.kiosk.persistence.service.PortalPersistence;
import com.morph.kiosk.persistence.service.UserPersistence;
import com.morph.util.ProjectConstants;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Serializable;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.Asynchronous;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.servlet.http.Part;
import org.omnifaces.util.Faces;
import org.omnifaces.util.Utils;

/**
 *
 * @author Tobi-Morph-PC
 */
@Named(value = "businessController")
@ViewScoped
public class BusinessController extends BaseController implements Serializable {

    @Resource(lookup = "java:global/KioskEJBModule-1.2/UserPersistence!com.morph.kiosk.persistence.service.UserPersistence")
    private UserPersistence userPersistence;

    @Resource(lookup = "java:global/KioskEJBModule-1.2/PortalPersistence!com.morph.kiosk.persistence.service.PortalPersistence")
    private PortalPersistence portalPersistence;

    private KioskUser user;
    private Business business = new Business();
    private List<Business> businesses = new ArrayList<>();

    private String bquery;
    private Part file;
    private byte[] fileContent;

    private List<BusinessActivityLog> businessActivityLogs = new ArrayList<>();
    private PaymentProvider paymentProvider = new PaymentProvider();
    private String businessName;
    private byte[] businessLogo;

    public BusinessController() {
    }

    @Override
    public void initUser(KioskUser user) {
        this.user = user;
        getBusinessIdentity(this.user);
    }

    @PostConstruct
    public void initialize() {
//        System.out.println("Business Controller Initialization .................");
        //businesses = portalPersistence.getAllActiveBusinessByUserId(Long.valueOf("4"));
//        Enumeration enames = Faces.getSession().getAttributeNames();
//        while (enames.hasMoreElements()) {
//            String key = (String) enames.nextElement();
//            String value = "" + Faces.getSession().getAttribute(key);
//            System.out.println(key + " - " + value);
//        }

//        Map<String, String> map = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
//        for (Map.Entry<String, String> entry : map.entrySet()) {
//            String key = entry.getKey();
//            Object value = entry.getValue();
//            System.out.println(key + " --> " + String.valueOf(value));
//        }
//        bquery = map.get(ProjectConstants.PAGE_QUERY_PARAM_BUSINESS);
//        System.out.println("Business Param :::::; " + bquery);
//        this.user = userPersistence.find(KioskUser.class, Long.valueOf("4"));
//        System.out.println("user ............... " + user);
        //System.out.println("Servlet context ................... " + Faces.getServletContext().getContextPath());
    }

    public String registerBusiness() {
        System.out.println("Register a business got called......");
        try {
            Business b = portalPersistence.getBusinessByName(this.business.getName());
            if (b == null) {
                this.business.setCreatedDate(new Date());
                this.business.setModifiedDate(new Date());
                this.business.setOwner(this.user);
//                if (file != null) {
//                    uploadBusinesslogo(file, this.business);
//                }
                userPersistence.create(this.business);
                bquery = String.valueOf(this.business.getId());
                Faces.setViewAttribute(ProjectConstants.PAGE_QUERY_PARAM_BUSINESS, bquery);
                JsfUtil.addInfoFacesMessage(ProjectConstants.BUSINESS_CREATED);
                return "businessDetail?faces-redirect=true&includeViewParams=true";

            } else {
                this.business = new Business();
                JsfUtil.addWarningFacesMessage(ProjectConstants.DUPLICATE_ENTRY);
                return null;
            }
        } catch (Exception e) {
            System.err.println("Error RegisterBusiness :::: " + e.getLocalizedMessage());
            this.business = new Business();
            JsfUtil.addErrorFacesMessage(ProjectConstants.GENERIC_ERROR + " " + e.getLocalizedMessage());
            return null;
        }
    }

    public void editBusiness() {
        bquery = Faces.getRequestParameter(ProjectConstants.PAGE_QUERY_PARAM_BUSINESS);
        business = portalPersistence.getBusinessByUserIdAndBusinessId(user.getId(), Long.valueOf(bquery));
        if (null == business) {
            JsfUtil.addWarningFacesMessage("Business resource was not found.");
        }
    }

    public void loadBusinessDetail() {
        this.business = portalPersistence.getBusinessByUserIdAndBusinessId(user.getId(), Long.valueOf(bquery));
        if (null == this.business) {
            JsfUtil.addWarningFacesMessage(ProjectConstants.PAGE_NOT_FOUND);
        }
//        else {
//            if ((null != business.getLogoName()) && null != business.getLogoPath()) {
//                displayLogoImage();
//            }
//            userPaymentProvider = portalPersistence.getUserPaymentProviderbyBusinessOwnerAndBusiness(user.getId(), business.getId());
//        }
    }

    public void resetBusiness() {
        this.business = new Business();
    }

    public void updateBusiness() {
        try {
            this.business.setModifiedDate(new Date());
//            if (null != file) {
//                uploadBusinesslogo(file, this.business);
//            }
            userPersistence.update(this.business);
            JsfUtil.addInfoFacesMessage(ProjectConstants.UPDATE_SUCCESSFUL);
            logChange(business);
        } catch (Exception e) {
            JsfUtil.addErrorFacesMessage(ProjectConstants.UPDATE_UNSUCCESSFUL);
            e.getLocalizedMessage();
        }
    }

    public void disableBusiness() {
        try {
            this.business.setModifiedDate(new Date());
            this.business.setStatus(false);
            userPersistence.update(this.business);
            JsfUtil.addInfoFacesMessage(ProjectConstants.BUSINESS_DISABLED);
            logChange(business);
        } catch (Exception e) {
            JsfUtil.addErrorFacesMessage(ProjectConstants.UPDATE_UNSUCCESSFUL);
            e.getLocalizedMessage();
        }
    }

    public void enableBusiness() {
        try {
            this.business.setModifiedDate(new Date());
            this.business.setStatus(true);
            userPersistence.update(this.business);
            JsfUtil.addInfoFacesMessage(ProjectConstants.BUSINESS_ENABLED);
            logChange(business);
        } catch (Exception e) {
            JsfUtil.addErrorFacesMessage(ProjectConstants.UPDATE_UNSUCCESSFUL);
            e.getLocalizedMessage();
        }
    }

    @Asynchronous
    private void logChange(Business b) {
        try {
            BusinessActivityLog log = new BusinessActivityLog(b);
            log.setModifiedby(user);
            portalPersistence.update(log);
        } catch (Exception e) {
            System.err.println("Log Failed..................." + e.getLocalizedMessage());
        }
    }

    public void uploadBusinesslogo() throws IOException {
        if (file != null) {
            if (file.getSize() != 0) {
                fileContent = Utils.toByteArray(file.getInputStream());
                final String path = System.getProperty("jboss.server.data.dir");
                final String pathAttache = path + File.separator + "img" + File.separator;
                System.out.println("Image Path ::::: " + pathAttache);
                try {
                    final String fname = UUID.randomUUID().toString().split("-", 0)[0] + file.getSubmittedFileName();
                    File outputFile = new File(pathAttache, fname);

                    try (OutputStream os = new FileOutputStream(outputFile, true)) {
                        os.write(Utils.toByteArray(this.file.getInputStream()));
                        os.flush();
                        os.close();
                    }

                    this.business.setLogoPath(path);
                    this.business.setLogoName(File.separator + "img" + File.separator + fname);
                    this.business.setLogo(fileContent);

                    userPersistence.update(this.business);
                    JsfUtil.addInfoFacesMessage(ProjectConstants.OPERATION_SUCCESSFUL);

                } catch (IOException e) {
                    e.getLocalizedMessage();
                    JsfUtil.addErrorFacesMessage(ProjectConstants.OPERATION_UNSUCCESSFUL);
                }
            }
        } else {
            JsfUtil.addErrorFacesMessage("Please Select an Image to Upload");
        }
        // displayLogoImage();
    }

//    public void uploadBusinesslogo(Part part, Business business) {
//        final String path = System.getProperty("jboss.server.data.dir");
//        final String pathAttache = path + File.separator + "img" + File.separator;
//        try {
//            final String fname = UUID.randomUUID().toString().split("-", 0)[0] + part.getSubmittedFileName();
//            System.out.println("Upload Path ::::::::::: " + pathAttache + " " + fname);
//            File outputFile = new File(pathAttache, fname);
//            try (OutputStream os = new FileOutputStream(outputFile, true)) {
//                os.write(Utils.toByteArray(this.file.getInputStream()));
//                os.flush();
//            }
//            business.setLogoPath(path);
//            business.setLogoName(File.separator + "img" + File.separator + fname);
//        } catch (IOException e) {
//            e.getLocalizedMessage();
//            JsfUtil.addErrorFacesMessage(ProjectConstants.OPERATION_UNSUCCESSFUL);
//        }
//    }
//    private void displayLogoImage() {
//        System.out.println(business.getId() + " ::::: Image File Path :::: " + business.getLogoPath() + " :::: " + business.getLogoName());
//        if (this.business.getLogoName() != null && this.business.getLogoPath() != null) {
//            try (FileInputStream fis = new FileInputStream(new File(this.business.getLogoPath(), this.business.getLogoName()))) {
//                byte[] b = Utils.toByteArray(fis);
//                this.business.setLogo(b);
//                fis.close();
//            } catch (Exception e) {
//                System.err.println("Error occured while load file ::: " + e.getLocalizedMessage());
//                //JsfUtil.addErrorFacesMessage(ProjectConstants.OPERATION_UNSUCCESSFUL);
//            }
//        }
//    }
    public void deleteBusinessLogo() {
        if (business != null) {
            try {
                Business b = userPersistence.find(Business.class, business.getId());
                b.setLogo(null);
                b.setLogoName(null);
                b.setLogoPath(null);
                userPersistence.update(b);
                Path path = FileSystems.getDefault().getPath(this.business.getLogoPath(), this.business.getLogoName());
                Files.deleteIfExists(path);
                this.business = b;
                // displayLogoImage();
                JsfUtil.addInfoFacesMessage(ProjectConstants.OPERATION_SUCCESSFUL);
            } catch (IOException e) {
                System.err.println("Error deleteItemImage ::::: " + e.getMessage());
                JsfUtil.addErrorFacesMessage(ProjectConstants.OPERATION_UNSUCCESSFUL);
            }
        }
    }

//    public void registerUserPaymentProvider() {
//        if (paymentProvider == null) {
//            JsfUtil.addWarningFacesMessage("Kindly select a valid Provider");
//        } else {
//            try {
//                UserPaymentProvider upp = new UserPaymentProvider();
//                upp.setBusinessOwnerId(this.user.getId());
//                upp.setProvider(paymentProvider);
//                upp.setCreatedDate(new Date());
//                upp.setBusinessId(business.getId());
//                upp.setLastModifiedDate(new Date());
//
//                portalPersistence.create(upp);
//                JsfUtil.addInfoFacesMessage(ProjectConstants.OPERATION_SUCCESSFUL);
//            } catch (Exception e) {
//                System.out.println("Error is " + e.getMessage());
//                JsfUtil.addErrorFacesMessage(ProjectConstants.OPERATION_UNSUCCESSFUL);
//                e.getLocalizedMessage();
//            }
//        }
//    }
    public void updateTaxRate() {
        try {
            this.business.setTaxRate(this.business.getTaxRate() == null ? 0 : Integer.parseInt(String.valueOf(this.business.getTaxRate()).replaceAll("[^0-9]+", "")));
            portalPersistence.update(this.business);
            JsfUtil.addInfoFacesMessage(ProjectConstants.OPERATION_SUCCESSFUL);
        } catch (Exception e) {
            System.out.println("Error is " + e.getMessage());
            JsfUtil.addErrorFacesMessage(ProjectConstants.OPERATION_UNSUCCESSFUL);
            e.getLocalizedMessage();
        }
    }

    public void viewLog(Long businessId) {
        System.out.println("::::::::::::::::::::::::::: Business ID............... " + businessId);
        businessActivityLogs = portalPersistence.getBusinessActivityLog(businessId);
    }

    public void getBusinessIdentity(KioskUser ku) {
        Business b = null;
        List<Business> list = portalPersistence.getAllBusinessByUserId(ku.getId());
        if (!list.isEmpty()) {
            for (Business b1 : list) {
                b = b1;
                break;
            }
        } else {
            //List<BusinessStaff>  staffs = userPersistence.getAllBusinessStaffByBusinessOnwer(ku.getId());
            BusinessStaff bs = userPersistence.getBusinessStaffByKioskUser(ku.getId());
            KioskUser k = bs.getCreator();
            List<Business> list2 = portalPersistence.getAllBusinessByUserId(k.getId());
            for (Business b1 : list2) {
                b = b1;
                break;
            }
        }

//        BusinessStaff bs = userPersistence.getBusinessStaffByKioskUser(ku.getId());
//
//        if (bs != null) {
//            List<BusinessStaffRole> bsr = userPersistence.getBusinessStaffRoleByBusinessStaff(bs.getId());
//            if (!bsr.isEmpty()) {
//                for (BusinessStaffRole bsr1 : bsr) {
//                    b = bsr1.getBusiness();
//                    break;
//                }
//            }
//        }
        if (b != null) {
            businessName = b.getName();
            businessLogo = b.getLogo();
//            if (b.getLogoName() != null && b.getLogoPath() != null) {
//                try (FileInputStream fis = new FileInputStream(new File(b.getLogoPath(), b.getLogoName()))) {
//                    this.businessLogo = Utils.toByteArray(fis);
//                    fis.close();
//                } catch (Exception e) {
//                    System.err.println("Error occured while load file ::: " + e.getLocalizedMessage());
//                }
//            }
        } else {
            businessName = "Chef Tent";
            businessLogo = null;
        }

    }

    public KioskUser getUser() {
        return user;
    }

    public void setUser(KioskUser user) {
        this.user = user;
    }

    public Business getBusiness() {
        return business;
    }

    public void setBusiness(Business business) {
        this.business = business;
    }

    public List<Business> getBusinesses() {
        return businesses;
    }

    public void setBusinesses(List<Business> businesses) {
        this.businesses = businesses;
    }

    public String getBquery() {
        return bquery;
    }

    public void setBquery(String bquery) {
        this.bquery = bquery;
    }

    public Part getFile() {
        return file;
    }

    public void setFile(Part file) {
        this.file = file;
    }

    List<Business> populateBusinesses() {
        try {
            return portalPersistence.getAllBusinessByUserId(user.getId());
        } catch (Exception e) {
            System.err.println("populateBusinesses :::: " + e.getLocalizedMessage());
            return Collections.EMPTY_LIST;
        }
    }

    public List<Business> getActiveBusinesses() {
        return populateBusinesses().stream().filter(i -> i.isStatus()).collect(Collectors.toList());
        //return portalPersistence.geAllActiveBusinessByUser(this.user.getId());
    }

    public List<Business> getDisabledBusinesses() {
        return populateBusinesses().stream().filter(i -> !(i.isStatus())).collect(Collectors.toList());
    }

    public List<BusinessActivityLog> getBusinessActivityLogs() {

        return businessActivityLogs;
    }

    public void setBusinessActivityLogs(List<BusinessActivityLog> businessActivityLogs) {
        this.businessActivityLogs = businessActivityLogs;
    }

    public PortalPersistence getPortalPersistence() {
        return portalPersistence;
    }

    public void setPortalPersistence(PortalPersistence portalPersistence) {
        this.portalPersistence = portalPersistence;
    }

    public byte[] getFileContent() {
        return fileContent;
    }

    public void setFileContent(byte[] fileContent) {
        this.fileContent = fileContent;
    }

    public PaymentProvider getPaymentProvider() {
        return paymentProvider;
    }

    public void setPaymentProvider(PaymentProvider paymentProvider) {
        this.paymentProvider = paymentProvider;
    }

//    public UserPaymentProvider getUserPaymentProvider() {
//        return userPaymentProvider;
//    }
//
//    public void setUserPaymentProvider(UserPaymentProvider userPaymentProvider) {
//        this.userPaymentProvider = userPaymentProvider;
//    }
    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public byte[] getBusinessLogo() {
        return businessLogo;
    }

    public void setBusinessLogo(byte[] businessLogo) {
        this.businessLogo = businessLogo;
    }

}
