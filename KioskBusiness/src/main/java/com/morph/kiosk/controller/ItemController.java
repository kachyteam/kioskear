/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.morph.kiosk.controller;

import com.morph.kiosk.util.JsfUtil;
import com.morph.kiosk.persistence.entity.Business;
import com.morph.kiosk.persistence.entity.Item;
import com.morph.kiosk.persistence.entity.ItemImage;
import com.morph.kiosk.persistence.entity.ItemSubItem;
import com.morph.kiosk.persistence.entity.ItemSubItemGroup;
import com.morph.kiosk.persistence.entity.ItemVariation;
import com.morph.kiosk.persistence.entity.KioskUser;
import com.morph.kiosk.persistence.entity.SubItem;
import com.morph.kiosk.persistence.entity.SubItemGroup;
import com.morph.kiosk.persistence.entity.SubitemgroupSubitem;
import com.morph.kiosk.persistence.entity.VariationSubItemCost;
import com.morph.kiosk.persistence.service.PortalPersistence;
import com.morph.kiosk.util.Image;
import com.morph.util.ProjectConstants;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Transparency;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Serializable;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.imageio.ImageIO;
import javax.servlet.http.Part;
import org.omnifaces.util.Faces;
import org.omnifaces.util.Utils;

/**
 *
 * @author Tobi-Morph-PC
 */
@Named(value = "itemController")
@ViewScoped
public class ItemController extends BaseController implements Serializable {

    @Resource(lookup = "java:global/KioskEJBModule-1.2/PortalPersistence!com.morph.kiosk.persistence.service.PortalPersistence")
    private PortalPersistence persistence;

    private Item item = new Item();
    private List<Item> items = new ArrayList<>();
    private Business business = new Business();

    private ItemVariation itemVariation = new ItemVariation();
    private List<ItemVariation> itemVariations = new ArrayList<>();

    private List<Item> activeItems = new ArrayList<>();
    private List<Item> disabledItems = new ArrayList<>();

    private ItemImage itemImage = new ItemImage();
    private List<ItemImage> images = new ArrayList<>();
    private VariationSubItemCost variationSubItemCost = new VariationSubItemCost();

    private SubItem subItem = new SubItem();
    private List<SubItem> subItems = new ArrayList<>();
    private List<SubItem> itemSubItems = new ArrayList<>();
    private List<SubItem> subItemsFromGroup = new ArrayList<>();
    private List<SubItem> checkedSubItem = new ArrayList<>();
    private List<VariationSubItemCost> subItemCosts = new ArrayList<>();

    private Map<Long, Boolean> availableSubItems = new HashMap<>();
    private Map<Long, Boolean> availableSubItems2 = new HashMap<>();
    private Map<Long, Boolean> selectedSubItems = new HashMap<>();
    private Map<Long, Boolean> availableSubItemGroups = new HashMap<>();
    private Map<Long, Boolean> selectedSubItemGroups = new HashMap<>();

    private SubItemGroup subItemGroup = new SubItemGroup();
    private List<SubItemGroup> subItemGroups = new ArrayList<>();
    private List<SubItemGroup> itemSubItemGroups = new ArrayList<>();
    private List<SubItemGroup> uniqueItemSubItemGroups = new ArrayList<>();

    private Part file;
    private byte[] fileContent;

    private KioskUser user = new KioskUser();

    private boolean showCreateSubItemGroup = false;
    private boolean toggleButtonDiv = true;
    private boolean showGroupSelectedButtonDiv = false;
    private boolean showImageTab = false;
    private boolean showVariationTab = false;
    private boolean showSubItemTab = false;
    private boolean showSubItemGroupTab = false;
    private boolean addSubItemTable = false;
    private boolean removeSubItemTable = false;
    private boolean addSubItemGroupTable = false;
    private boolean removeSubItemGroupTable = false;
    private boolean updateSubItemGroup = false;

    /**
     * business query parameter
     */
    private String bquery;
    /**
     * kiosk query parameter
     */
    private String kquery;
    /**
     * item query parameter
     */
    private String iquery;
    /**
     * subitem query parameter
     */
    private String squery;

    private List<byte[]> itemImageContents = new ArrayList<>();
    private List<Image> fileImages = new ArrayList<>();

    public ItemController() {
    }

    @Override
    void initUser(KioskUser user) {
        this.user = user;
    }

    @PostConstruct
    public void initialize() {
        Map<String, String> map = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        for (Map.Entry<String, String> entry : map.entrySet()) {
            String key = entry.getKey();
            Object value = entry.getValue();
        }
        iquery = (String) map.get(ProjectConstants.PAGE_QUERY_PARAM_ITEM);

        squery = (String) map.get(ProjectConstants.PAGE_QUERY_PARAM_SUBITEM);
    }

    public void reset() {
        this.item = new Item();
        this.subItem = new SubItem();
        this.subItemGroup = new SubItemGroup();
    }

    private boolean checkBusiness(Business b) {

        if (null == bquery || "".equals(bquery)) {
            return false;
        } else {
            b = persistence.getActiveBusinessByUserIdAndBusinessId(this.user.getId(), Long.valueOf(bquery));
            if (null == b) {
                return false;
            } else {
                this.business = b;
                return true;
            }
        }
    }

    public void populateSubItemGroups() {
        this.subItemGroups.clear();
        this.subItemGroups = persistence.getSubItemGroupByOwner(this.user.getId());
    }

    public void populateActiveItems() {
        if (checkBusiness(business) != false) {
            this.activeItems = persistence.getActiveItemByBusiness(this.business.getId());
        } else {
            this.activeItems = persistence.getActiveItemByUser(this.user.getId());
        }

        //System.out.println("printing Active Items User ................... " + user);
        //this.activeItems = persistence.getActiveItemByUser(user.getId());
    }

    public void populateDisabledItems() {
        if (checkBusiness(business) != false) {
            this.disabledItems = persistence.getDisabledItemByBusiness(this.business.getId());
        } else {
            this.disabledItems = persistence.getDisableItemByUser(this.user.getId());
        }
        //this.disabledItems = persistence.getDisableItemByUser(this.user.getId());
    }

    public void populateActiveSubItems() {
        //not need to check business any more as business paramater is not pass to the 
        //request page
//        if (checkBusiness(business) != false) {
//            this.activeSubItems = persistence.getActiveSubItemByBusiness(this.business.getId());
//        } else {
//            this.activeSubItems = persistence.getActiveSubItemByUser(this.user.getId());
//        }
        this.subItems.clear();
        this.subItems = persistence.getActiveSubItemByUser(this.user.getId());
    }

    public void populateDisabledSubItems() {
        //        if (checkBusiness(business) != false) {
//            this.disabledSubItems = persistence.getDisabledSubItemByBusiness(this.business.getId());
//        } else {
//            this.disabledSubItems = persistence.getDisableSubItemByUser(this.user.getId());
//        }
        this.itemSubItems.clear();
        this.itemSubItems = persistence.getDisableSubItemByUser(this.user.getId());
    }

    public void fetchSubItems(SubItemGroup group) {
        this.subItemsFromGroup.clear();
        this.subItemsFromGroup = persistence.getSubItemsFromSubItemGroup(group.getId());
        this.subItemGroup = group;
    }

    public void populateSubItemsByBusiness(AjaxBehaviorEvent e) {
        this.subItems.clear();
        this.subItems = persistence.getActiveSubItemByBusiness(this.business.getId());
    }

    public void checkBooleanCheckBox(AjaxBehaviorEvent e) {
        this.showGroupSelectedButtonDiv = false;
        for (Map.Entry<Long, Boolean> entrySet : availableSubItems.entrySet()) {
            Long key = entrySet.getKey();
            Boolean value = entrySet.getValue();
            if (value) {
                this.showGroupSelectedButtonDiv = value;
            }
        }
    }

    public void selectedSubItemTable(AjaxBehaviorEvent e) {
        this.removeSubItemTable = false;
        this.addSubItemTable = false;
        // this.subItemGroup = new SubItemGroup();

        for (Map.Entry<Long, Boolean> entrySet : selectedSubItems.entrySet()) {
            Long key = entrySet.getKey();

            Boolean value = entrySet.getValue();
            if (value) {
                this.removeSubItemTable = value;
            }
        }

        for (Map.Entry<Long, Boolean> entrySet : availableSubItems.entrySet()) {
            Long key = entrySet.getKey();

            Boolean value = entrySet.getValue();
            if (value) {
                this.addSubItemTable = value;
            }
        }

        for (Map.Entry<Long, Boolean> entrySet : availableSubItems2.entrySet()) {
            Long key = entrySet.getKey();

            Boolean value = entrySet.getValue();
            if (value) {
                this.addSubItemTable = value;
            }
        }
    }

    public void selectedSubItemGroupTable(AjaxBehaviorEvent e) {
        this.removeSubItemGroupTable = false;
        this.addSubItemGroupTable = false;
        for (Map.Entry<Long, Boolean> entrySet : selectedSubItemGroups.entrySet()) {
            Long key = entrySet.getKey();

            Boolean value = entrySet.getValue();
            if (value) {
                this.removeSubItemGroupTable = value;
            }
        }

        for (Map.Entry<Long, Boolean> entrySet : availableSubItemGroups.entrySet()) {
            Long key = entrySet.getKey();

            Boolean value = entrySet.getValue();
            if (value) {
                this.addSubItemGroupTable = value;
            }
        }
    }

    public void showCreateSubItemGroupDiv(AjaxBehaviorEvent e) {
        this.subItems.clear();
        this.availableSubItems.clear();
        if (this.business != null) {
            this.subItems = persistence.getActiveSubItemByBusiness(this.business.getId());
        }
        this.subItemGroup = new SubItemGroup();
        this.toggleButtonDiv = false;
        this.showCreateSubItemGroup = true;
        this.updateSubItemGroup = false;
    }

    public void showUpdateSubItemGroupDiv(SubItemGroup sig) {
        this.itemSubItems.clear();
        this.subItems.clear();
        this.selectedSubItems.clear();
        this.availableSubItems.clear();
        this.toggleButtonDiv = true;
        this.showCreateSubItemGroup = false;
        this.updateSubItemGroup = true;

        this.itemSubItems = persistence.getSubItemsFromSubItemGroup(sig.getId());
        this.subItems = persistence.getAllBusinessSubItems(sig.getBusiness().getId());
        this.subItems.removeAll(itemSubItems);
        this.subItemGroup = sig;
    }

    public void showDetailDiv(AjaxBehaviorEvent e) {
        this.showImageTab = false;
        this.showSubItemGroupTab = false;
        this.showSubItemTab = false;
        this.showVariationTab = false;
    }

    public void showImageDiv(AjaxBehaviorEvent e) {
        this.showImageTab = true;
        this.showSubItemGroupTab = false;
        this.showSubItemTab = false;
        this.showVariationTab = false;
    }

    public void showVariationDiv(AjaxBehaviorEvent e) {
        this.showImageTab = false;
        this.showSubItemGroupTab = false;
        this.showSubItemTab = false;
        this.showVariationTab = true;
    }

    public void showSubItemDiv(AjaxBehaviorEvent e) {
        this.showImageTab = false;
        this.showSubItemGroupTab = false;
        this.showSubItemTab = true;
        this.showVariationTab = false;
    }

    public void showSubItemGroupDiv(AjaxBehaviorEvent e) {
        this.showImageTab = false;
        this.showSubItemGroupTab = true;
        this.showSubItemTab = false;
        this.showVariationTab = false;
    }

    public void populateItemSubItems() {
        this.itemSubItems.clear();
        this.itemSubItems = persistence.getAllItemSubItems(this.item.getId());
    }

    public void populateItemSubItemGroups() {
        this.itemSubItemGroups.clear();
        this.uniqueItemSubItemGroups.clear();
        this.itemSubItemGroups = persistence.getAllItemSubItemGroups(this.item.getId());
        this.uniqueItemSubItemGroups = persistence.getAllUniqueItemSubItemGroups(this.item.getId());
        itemSubItemGroups.removeAll(uniqueItemSubItemGroups);
    }

    public void deleteSubItemGroup(SubItemGroup sig) {
        try {
            if (sig != null) {
                ItemSubItemGroup group = persistence.geItemSubItemGroupByItemSubItem(item.getId(), sig.getId());
                if (group != null) {
                    persistence.delete(group);
                }

                List<SubitemgroupSubitem> sses = persistence.getSubitemgroupSubitemsBySubItemGroup(sig);
                for (SubitemgroupSubitem ss : sses) {
                    persistence.delete(ss);
                    disableVariableSubItemCost(ss.getSubItem(), item);
                }
                persistence.delete(sig);

                this.availableSubItems.clear();
                populateItemSubItems();
                this.subItems.clear();
                this.subItems = persistence.getActiveSubItemByBusiness(this.business.getId());
                this.subItems.removeAll(itemSubItems);
                for (SubItemGroup isig2 : this.itemSubItemGroups) {
                    List<SubItem> sis = persistence.getSubItemsFromSubItemGroup(isig2.getId());
                    this.subItems.removeAll(sis);
                }
                this.subItemGroup = new SubItemGroup();
                this.addSubItemTable = false;
                JsfUtil.addInfoFacesMessage(ProjectConstants.ITEM_REMOVED);
            }
        } catch (Exception e) {
            System.out.println("EXception >>>>" + e);
            this.subItemGroup = new SubItemGroup();
            this.addSubItemTable = false;
            JsfUtil.addErrorFacesMessage(ProjectConstants.OPERATION_UNSUCCESSFUL);
        }
    }

    public void addSingleSubItemGroup(SubItemGroup sig) {
        try {
            if (sig != null) {
                List<SubItem> itemz = persistence.getSubItemsFromSubItemGroup(sig.getId());
                for (SubItem subItemz : itemz) {
                    if (this.itemSubItems.contains(subItemz)) {
                        JsfUtil.addErrorFacesMessage(subItemz.getName() + " has already been selected as a SubItem for this Item");
                        return;
                    }
                }

                for (SubItemGroup sigg : uniqueItemSubItemGroups) {
                    List<SubItem> itemzz = persistence.getSubItemsFromSubItemGroup(sigg.getId());
                    for (SubItem si1 : itemzz) {
                        if (itemz.contains(si1)) {
                            JsfUtil.addErrorFacesMessage(si1.getName() + " in " + sig.getName() + " Group has already been selected in " + sigg.getName());
                            return;
                        }
                    }
                }

                ItemSubItemGroup isi = new ItemSubItemGroup();
                isi.setMainItem(this.item);
                isi.setSubItemGroup(sig);
                persistence.create(isi);

                for (SubItem subItemz : itemz) {
                    enableVariableSubItemCost(subItemz, item);
                }
            }
            this.availableSubItemGroups.clear();
            populateItemSubItemGroups();
            this.subItemGroups.clear();
            this.subItemGroups = persistence.getActiveSubItemGroupByBusiness(this.business.getId());
            for (SubItemGroup sigg : this.itemSubItemGroups) {
                List<SubItem> itemz = persistence.getSubItemsFromSubItemGroup(sigg.getId());
                this.subItems.removeAll(itemz);
            }
            this.subItemGroups.removeAll(itemSubItemGroups);
            JsfUtil.addInfoFacesMessage(ProjectConstants.ITEM_ADDED);
        } catch (Exception e) {
            System.out.println("EXception >>>>" + e);
            JsfUtil.addErrorFacesMessage(ProjectConstants.OPERATION_UNSUCCESSFUL);
        }
        this.addSubItemGroupTable = false;

    }

    public void addSubItemGroup() {
        try {
            List<SubItemGroup> checkedSubItems = new ArrayList<>();

            for (SubItemGroup s : this.subItemGroups) {
                if (this.availableSubItemGroups.get(s.getId())) {
                    checkedSubItems.add(s);
                }
            }

            for (SubItemGroup si : checkedSubItems) {
                List<SubItem> itemz = persistence.getSubItemsFromSubItemGroup(si.getId());
                for (SubItem subItemz : itemz) {
                    if (this.itemSubItems.contains(subItemz)) {
                        JsfUtil.addErrorFacesMessage(subItemz.getName() + " in " + si.getName() + " Group has already been selected as a SubItem for this Item");
                        return;
                    }
                }

                for (SubItemGroup sig : uniqueItemSubItemGroups) {
                    List<SubItem> itemzz = persistence.getSubItemsFromSubItemGroup(sig.getId());
                    for (SubItem si1 : itemzz) {
                        if (itemz.contains(si1)) {
                            JsfUtil.addErrorFacesMessage(si1.getName() + " in " + si.getName() + " Group has already been selected in " + sig.getName());
                            return;
                        }
                    }
                }

                ItemSubItemGroup isi = new ItemSubItemGroup();
                isi.setMainItem(this.item);
                isi.setSubItemGroup(si);
                persistence.create(isi);

                for (SubItem subItemz : itemz) {
                    enableVariableSubItemCost(subItemz, item);
                }
            }

            this.availableSubItemGroups.clear();
            populateItemSubItemGroups();
            this.subItemGroups.clear();
            this.subItemGroups = persistence.getActiveSubItemGroupByBusiness(this.business.getId());
            this.subItemGroups.removeAll(itemSubItemGroups);
            JsfUtil.addInfoFacesMessage(ProjectConstants.ITEM_ADDED);
        } catch (Exception e) {
            System.out.println("EXception >>>>" + e);
            JsfUtil.addErrorFacesMessage(ProjectConstants.OPERATION_UNSUCCESSFUL);
        }
        this.addSubItemGroupTable = false;
    }

    public void removeSingleSubItemGroup(Long id) {
        try {
            ItemSubItemGroup isi = persistence.geItemSubItemGroupByItemSubItem(this.item.getId(), id);
            if (isi != null) {
                persistence.delete(isi);
                List<SubItem> itemz = persistence.getSubItemsFromSubItemGroup(isi.getSubItemGroup().getId());
                for (SubItem subItemz : itemz) {
                    disableVariableSubItemCost(subItemz, item);
                }
            }

            this.selectedSubItemGroups.clear();
            populateItemSubItemGroups();
            this.subItemGroups.clear();
            this.subItemGroups = persistence.getActiveSubItemGroupByBusiness(this.business.getId());
            this.subItemGroups.removeAll(itemSubItemGroups);
            JsfUtil.addInfoFacesMessage(ProjectConstants.ITEM_REMOVED);
        } catch (Exception e) {
            System.out.println("EXception >>>>" + e);
            JsfUtil.addErrorFacesMessage(ProjectConstants.OPERATION_UNSUCCESSFUL);
        }
        this.removeSubItemGroupTable = false;
    }

    public void removeSubItemGroup() {
        try {
            List<SubItemGroup> checkedSubItemGroups = new ArrayList<>();
            for (SubItemGroup s : this.itemSubItemGroups) {
                if (this.selectedSubItemGroups.get(s.getId())) {
                    checkedSubItemGroups.add(s);
                }
            }

            for (SubItemGroup si : checkedSubItemGroups) {
                ItemSubItemGroup isi = persistence.geItemSubItemGroupByItemSubItem(this.item.getId(), si.getId());
                if (isi != null) {
                    persistence.delete(isi);

                    List<SubItem> itemz = persistence.getSubItemsFromSubItemGroup(si.getId());
                    for (SubItem subItemz : itemz) {
                        disableVariableSubItemCost(subItemz, item);
                    }

                }
            }

            this.selectedSubItemGroups.clear();
            populateItemSubItemGroups();
            this.subItemGroups.clear();
            this.subItemGroups = persistence.getActiveSubItemGroupByBusiness(this.business.getId());
            this.subItemGroups.removeAll(itemSubItemGroups);
            JsfUtil.addInfoFacesMessage(ProjectConstants.ITEM_REMOVED);
        } catch (Exception e) {
            System.out.println("EXception >>>>" + e);
            JsfUtil.addErrorFacesMessage(ProjectConstants.OPERATION_UNSUCCESSFUL);
        }
        this.removeSubItemGroupTable = false;
    }

    public void removeSubItemUniqueItem() {
        try {
            for (SubItem s : this.subItemsFromGroup) {
                if (this.selectedSubItems.get(s.getId())) {
                    SubitemgroupSubitem ss = persistence.getSubitemgroupSubitem(this.subItemGroup.getId(), s.getId());
                    if (ss != null) {
                        persistence.delete(ss);
                        disableVariableSubItemCost(s, item);
                    }
                }
            }
        } catch (Exception e) {
            System.out.println("EXception >>>>" + e);

            JsfUtil.addErrorFacesMessage(ProjectConstants.OPERATION_UNSUCCESSFUL);
            this.removeSubItemTable = false;
            return;
        }

        this.selectedSubItems.clear();
        this.subItems.clear();
        this.subItems = persistence.getActiveSubItemByBusiness(this.business.getId());
        this.subItems.removeAll(itemSubItems);
        for (SubItemGroup isig : this.itemSubItemGroups) {
            List<SubItem> sis = persistence.getSubItemsFromSubItemGroup(isig.getId());
            this.subItems.removeAll(sis);
        }
        JsfUtil.addInfoFacesMessage(ProjectConstants.ITEM_REMOVED);
        this.removeSubItemTable = false;
    }

    public void removeSubItem() {
        try {
            for (SubItem s : this.itemSubItems) {
                if (this.selectedSubItems.get(s.getId())) {
                    ItemSubItem isi = persistence.geItemSubItemByItemSubItem(this.item.getId(), s.getId());
                    if (isi != null) {
                        persistence.delete(isi);
                        disableVariableSubItemCost(s, item);
                    }
                }
            }

            this.selectedSubItems.clear();
            populateItemSubItems();
            this.subItems.clear();
            this.subItems = persistence.getActiveSubItemByBusiness(this.business.getId());
            this.subItems.removeAll(itemSubItems);
            for (SubItemGroup isig : this.itemSubItemGroups) {
                List<SubItem> sis = persistence.getSubItemsFromSubItemGroup(isig.getId());
                this.subItems.removeAll(sis);
            }
            JsfUtil.addInfoFacesMessage(ProjectConstants.ITEM_REMOVED);
        } catch (Exception e) {
            System.out.println("EXception >>>>" + e);
            JsfUtil.addErrorFacesMessage(ProjectConstants.OPERATION_UNSUCCESSFUL);
        }
        this.removeSubItemTable = false;
    }

    public void removeSingleSubItem(SubItem subItem) {
        try {
            ItemSubItem isi = persistence.geItemSubItemByItemSubItem(this.item.getId(), subItem.getId());
            if (isi != null) {
                persistence.delete(isi);
                disableVariableSubItemCost(subItem, item);
            }

            this.selectedSubItems.clear();
            populateItemSubItems();
            this.subItems.clear();
            this.subItems = persistence.getActiveSubItemByBusiness(this.business.getId());
            this.subItems.removeAll(itemSubItems);
            for (SubItemGroup isig : this.itemSubItemGroups) {
                List<SubItem> sis = persistence.getSubItemsFromSubItemGroup(isig.getId());
                this.subItems.removeAll(sis);
            }
            JsfUtil.addInfoFacesMessage(ProjectConstants.ITEM_REMOVED);
        } catch (Exception e) {
            System.out.println("EXception >>>>" + e);
            JsfUtil.addErrorFacesMessage(ProjectConstants.OPERATION_UNSUCCESSFUL);
        }
        this.removeSubItemTable = false;
    }

    public void registerItemSubItemGroup() {
        try {
            this.subItemGroup.setActive(true);
            this.subItemGroup.setBusiness(business);
            this.subItemGroup.setOwner(user);
            this.subItemGroup.setItem(this.item);
            persistence.create(this.subItemGroup);

            ItemSubItemGroup isig = new ItemSubItemGroup();
            isig.setMainItem(this.item);
            isig.setSubItemGroup(this.subItemGroup);
            persistence.create(isig);

            for (SubItem s : this.subItems) {
                if (this.availableSubItems.get(s.getId())) {
                    persistence.mergeSubItem(s);
                    SubitemgroupSubitem ss = new SubitemgroupSubitem();
                    ss.setSubItem(s);
                    ss.setSubItemGroup(subItemGroup);
                    persistence.create(ss);
                }
            }

            this.availableSubItems.clear();
            populateItemSubItems();
            this.subItems.clear();
            this.subItems = persistence.getActiveSubItemByBusiness(this.business.getId());
            this.subItems.removeAll(itemSubItems);
            for (SubItemGroup isig2 : this.itemSubItemGroups) {
                List<SubItem> sis = persistence.getSubItemsFromSubItemGroup(isig2.getId());
                this.subItems.removeAll(sis);
            }
            this.subItemGroup = new SubItemGroup();
            this.addSubItemTable = false;
            JsfUtil.addInfoFacesMessage(ProjectConstants.SUBITEM_GROUP_CREATED);

        } catch (Exception e) {
            System.out.println("EXception >>>>" + e);
            JsfUtil.addErrorFacesMessage(ProjectConstants.OPERATION_UNSUCCESSFUL);
            this.subItemGroup = new SubItemGroup();
            this.addSubItemTable = false;
        }
    }

    public void addSubItem() {
        try {
            for (SubItem s : this.subItems) {
                if (this.availableSubItems.get(s.getId())) {
                    ItemSubItem isi = new ItemSubItem();
                    isi.setMainItem(item);
                    isi.setSubItem(s);
                    persistence.create(isi);
                    enableVariableSubItemCost(s, item);
                }
            }

            this.availableSubItems.clear();
            //  populateItemSubItems();
            this.subItems.clear();
            this.subItems = persistence.getActiveSubItemByBusiness(this.business.getId());
            this.subItems.removeAll(itemSubItems);
            for (SubItemGroup isig : this.itemSubItemGroups) {
                List<SubItem> sis = persistence.getSubItemsFromSubItemGroup(isig.getId());
                this.subItems.removeAll(sis);
            }
            JsfUtil.addInfoFacesMessage(ProjectConstants.ITEM_ADDED);
        } catch (Exception e) {
            System.out.println("EXception >>>>" + e);
            JsfUtil.addErrorFacesMessage(ProjectConstants.OPERATION_UNSUCCESSFUL);
        }
        this.addSubItemTable = false;
    }

    public void addSingleSubItem(SubItem si) {
        try {
            if (si != null) {
                ItemSubItem isi = new ItemSubItem();
                isi.setMainItem(this.item);
                isi.setSubItem(si);
                persistence.create(isi);
                enableVariableSubItemCost(si, item);
            }
            this.availableSubItems.clear();
            populateItemSubItems();
            //populateActiveSubItems();
            this.subItems.clear();
            this.subItems = persistence.getActiveSubItemByBusiness(this.business.getId());
            this.subItems.removeAll(itemSubItems);
            for (SubItemGroup isig : this.itemSubItemGroups) {
                List<SubItem> sis = persistence.getSubItemsFromSubItemGroup(isig.getId());
                this.subItems.removeAll(sis);
            }
            JsfUtil.addInfoFacesMessage(ProjectConstants.ITEM_ADDED);
        } catch (Exception e) {
            System.out.println("EXception >>>>" + e);
            JsfUtil.addErrorFacesMessage(ProjectConstants.OPERATION_UNSUCCESSFUL);
        }
        this.removeSubItemTable = false;
    }

    public void populateSubItemTables() {
        populateItemSubItems();
        populateItemSubItemGroups();

        this.subItems.clear();
        this.subItems = persistence.getActiveSubItemByBusiness(this.business.getId());

        this.subItemGroups.clear();
        this.subItemGroups = persistence.getActiveSubItemGroupByBusiness(this.business.getId());

        this.subItemGroups.removeAll(itemSubItemGroups);
        this.subItems.removeAll(itemSubItems);

        for (SubItemGroup isig : this.itemSubItemGroups) {
            List<SubItem> sis = persistence.getSubItemsFromSubItemGroup(isig.getId());
            this.subItems.removeAll(sis);
        }

        for (SubItemGroup isig : this.uniqueItemSubItemGroups) {
            List<SubItem> sis = persistence.getSubItemsFromSubItemGroup(isig.getId());
            this.subItems.removeAll(sis);
        }

        //populateActiveSubItems();
        //populateSubItemGroups();
    }

    public void disableVariableSubItemCost(SubItem s, Item i) {
        VariationSubItemCost cost = persistence.getVariationCostBySubItem(s, i);
        if (cost != null) {
            cost.setActive(false);
            persistence.update(cost);
        }
    }

    public void enableVariableSubItemCost(SubItem s, Item i) {
        VariationSubItemCost c = persistence.getVariationCostBySubItem(s, i);
        if (c == null) {
            List<ItemVariation> ivs = persistence.getActiveItemVariationsByItemId(i.getId());
            for (ItemVariation iv : ivs) {
                VariationSubItemCost cost = new VariationSubItemCost();
                cost.setActive(false);
                cost.setMainItem(i);
                cost.setPrice(0.0);
                cost.setSubItem(s);
                cost.setItemVariation(iv);
                persistence.create(cost);
            }

        }
    }

    public void loadItemDetail() {
        if (null == iquery || "".equalsIgnoreCase(iquery)) {
            JsfUtil.addErrorFacesMessage(ProjectConstants.PAGE_NOT_FOUND);
            throw new IllegalArgumentException(ProjectConstants.PAGE_NOT_FOUND);
        } else {
            this.item = persistence.getItemByItemAndUserId(Long.valueOf(iquery), this.user.getId());
            if (this.item == null) {
                JsfUtil.addErrorFacesMessage(ProjectConstants.PAGE_NOT_FOUND);
            } else {
                this.business = (Business) this.item.getBusiness();
                this.itemImage = persistence.getItemImage(item);

            }
            populateSubItemTables();
        }
    }

    public void loadSubItemDetail() {
        if (null == squery || "".equalsIgnoreCase(squery)) {
            JsfUtil.addErrorFacesMessage(ProjectConstants.PAGE_NOT_FOUND);
            throw new IllegalArgumentException(ProjectConstants.PAGE_NOT_FOUND);
        } else {
            this.subItem = persistence.find(SubItem.class, Long.valueOf(squery));
            if (this.subItem == null) {
                JsfUtil.addErrorFacesMessage(ProjectConstants.PAGE_NOT_FOUND);
            } else {
                this.business = (Business) this.subItem.getBusiness();
                if ((null != business.getLogoName()) && null != business.getLogoPath()) {
                    displaySubItemImage();
                }
            }
        }
    }

    public void removeSubItemFromGroup() {
        this.subItemGroup = persistence.find(SubItemGroup.class, this.subItemGroup.getId());
        try {
            List<SubItem> checkedSubItems = new ArrayList<>();
            for (SubItem s : this.itemSubItems) {
                if (this.selectedSubItems.get(s.getId())) {
                    checkedSubItems.add(s);
                }
            }
            for (SubItem si : checkedSubItems) {
                SubitemgroupSubitem ss = persistence.getSubitemgroupSubitem(subItemGroup.getId(), si.getId());
                if (ss != null) {
                    persistence.delete(ss);
                }
            }
            this.itemSubItems.clear();
            this.subItems.clear();
            this.toggleButtonDiv = true;
            this.showCreateSubItemGroup = false;
            this.updateSubItemGroup = true;
            this.itemSubItems = persistence.getSubItemsFromSubItemGroup(subItemGroup.getId());
            this.subItems = persistence.getAllBusinessSubItems(subItemGroup.getBusiness().getId());
            this.subItems.removeAll(itemSubItems);
            selectedSubItems.clear();
            JsfUtil.addInfoFacesMessage(ProjectConstants.ITEM_REMOVED);
        } catch (Exception e) {
            System.out.println("EXception >>>>" + e);
            JsfUtil.addErrorFacesMessage(ProjectConstants.OPERATION_UNSUCCESSFUL);
        }
    }

    public void addSubItemToSubItemGroup() {
        try {
            for (SubItem s : this.subItems) {
                if (this.availableSubItems2.get(s.getId())) {
                    SubitemgroupSubitem ss = new SubitemgroupSubitem();
                    ss.setSubItem(s);
                    ss.setSubItemGroup(subItemGroup);
                    persistence.create(ss);
                    enableVariableSubItemCost(s, item);
                }
            }

            this.availableSubItems2.clear();
            //  populateItemSubItems();
            this.subItems.clear();
            this.subItems = persistence.getActiveSubItemByBusiness(this.business.getId());
            this.subItems.removeAll(itemSubItems);
            for (SubItemGroup isig : this.itemSubItemGroups) {
                List<SubItem> sis = persistence.getSubItemsFromSubItemGroup(isig.getId());
                this.subItems.removeAll(sis);
            }
            JsfUtil.addInfoFacesMessage(ProjectConstants.ITEM_ADDED);
        } catch (Exception e) {
            System.out.println("EXception >>>>" + e);
            JsfUtil.addErrorFacesMessage(ProjectConstants.OPERATION_UNSUCCESSFUL);
        }
        this.addSubItemTable = false;
    }

    public void addSubItemToGroup() {
        this.subItemGroup = persistence.find(SubItemGroup.class, this.subItemGroup.getId());
        try {
            for (SubItem s : this.subItems) {
                if (this.availableSubItems.get(s.getId())) {
                    persistence.mergeSubItem(s);
                    SubitemgroupSubitem ss = new SubitemgroupSubitem();
                    ss.setSubItem(s);
                    ss.setSubItemGroup(subItemGroup);
                    persistence.create(ss);
                }
            }
            this.itemSubItems.clear();
            this.subItems.clear();
            this.toggleButtonDiv = true;
            this.showCreateSubItemGroup = false;
            this.updateSubItemGroup = true;
            this.itemSubItems = persistence.getSubItemsFromSubItemGroup(subItemGroup.getId());
            this.subItems = persistence.getAllBusinessSubItems(subItemGroup.getBusiness().getId());
            this.subItems.removeAll(itemSubItems);
            availableSubItems.clear();
            JsfUtil.addInfoFacesMessage(ProjectConstants.ITEM_ADDED);
        } catch (Exception e) {
            System.out.println("EXception >>>>" + e);
            JsfUtil.addErrorFacesMessage(ProjectConstants.OPERATION_UNSUCCESSFUL);
        }
    }

    public void addSingleSubItemToGroup(SubItem s) {
        this.subItemGroup = persistence.find(SubItemGroup.class, this.subItemGroup.getId());
        try {
            if (item != null) {
                persistence.mergeSubItem(s);
                SubitemgroupSubitem ss = new SubitemgroupSubitem();
                ss.setSubItem(s);
                ss.setSubItemGroup(subItemGroup);
                persistence.create(ss);
                this.itemSubItems.clear();
                this.subItems.clear();
                this.toggleButtonDiv = true;
                this.showCreateSubItemGroup = false;
                this.updateSubItemGroup = true;
                this.addSubItemTable = false;
                this.itemSubItems = persistence.getSubItemsFromSubItemGroup(subItemGroup.getId());
                this.subItems = persistence.getAllBusinessSubItems(subItemGroup.getBusiness().getId());
                this.subItems.removeAll(itemSubItems);
                availableSubItems.clear();
                JsfUtil.addInfoFacesMessage(ProjectConstants.ITEM_ADDED);
            }
        } catch (Exception e) {
            System.out.println("EXception >>>>" + e);
            JsfUtil.addErrorFacesMessage(ProjectConstants.OPERATION_UNSUCCESSFUL);
        }
    }

    public void removeSingleSubItemFromGroup(Long id) {
        this.subItemGroup = persistence.find(SubItemGroup.class, this.subItemGroup.getId());
        try {
            SubitemgroupSubitem ss = persistence.getSubitemgroupSubitem(subItemGroup.getId(), id);
            if (ss != null) {
                persistence.delete(ss);
            }
            this.itemSubItems.clear();
            this.subItems.clear();
            this.toggleButtonDiv = true;
            this.showCreateSubItemGroup = false;
            this.updateSubItemGroup = true;
            removeSubItemTable = false;
            this.itemSubItems = persistence.getSubItemsFromSubItemGroup(subItemGroup.getId());
            this.subItems = persistence.getAllBusinessSubItems(subItemGroup.getBusiness().getId());
            this.subItems.removeAll(itemSubItems);
            selectedSubItems.clear();
            JsfUtil.addInfoFacesMessage(ProjectConstants.ITEM_REMOVED);
        } catch (Exception e) {
            System.out.println("EXception >>>>" + e);
            JsfUtil.addErrorFacesMessage(ProjectConstants.OPERATION_UNSUCCESSFUL);
        }
    }

    public void updateSubItemGroup() {
        try {
            this.subItemGroup.setBusiness(business);
            persistence.update(this.subItemGroup);
            this.subItemGroup = new SubItemGroup();
            this.business = new Business();
            JsfUtil.addInfoFacesMessage(ProjectConstants.UPDATE_SUCCESSFUL);
        } catch (Exception e) {
            System.out.println("EXception >>>>" + e);
            JsfUtil.addErrorFacesMessage(ProjectConstants.UPDATE_UNSUCCESSFUL);
            this.subItemGroup = new SubItemGroup();
            this.business = new Business();
        }
    }

    public void testing() {
        checkedSubItem.clear();
        this.subItemGroup = new SubItemGroup();
        for (SubItem s : this.subItems) {
            if (this.availableSubItems.get(s.getId()) != null) {
                if (this.availableSubItems.get(s.getId())) {
                    checkedSubItem.add(s);
                }
            }
        }
    }

    public void registerNewSubItemGroup() {
        if (this.subItemGroup.getName() == null) {
            JsfUtil.addErrorFacesMessage("Please Provide a SubItem Group Name");
            return;
        }

        try {
            this.subItemGroup.setActive(true);
            this.subItemGroup.setBusiness(business);
            this.subItemGroup.setOwner(user);

            persistence.create(this.subItemGroup);
        } catch (Exception e) {
            System.out.println("EXception >>>>" + e);
            JsfUtil.addErrorFacesMessage("Group Name Already Exist");
            this.subItemGroup = new SubItemGroup();
            this.addSubItemTable = false;
            return;
        }

        try {
            for (SubItem s : this.subItems) {
                if (this.availableSubItems.get(s.getId())) {
                    persistence.mergeSubItem(s);
                    SubitemgroupSubitem ss = new SubitemgroupSubitem();
                    ss.setSubItem(s);
                    ss.setSubItemGroup(this.subItemGroup);
                    persistence.create(ss);
                }
            }

        } catch (Exception e) {
            System.out.println("EXception >>>>" + e);
            JsfUtil.addErrorFacesMessage(ProjectConstants.OPERATION_UNSUCCESSFUL);
            this.subItemGroup = new SubItemGroup();
            this.addSubItemTable = false;
            return;
        }

        populateItemSubItems();
        populateItemSubItemGroups();
        this.subItems.clear();
        this.subItems = persistence.getActiveSubItemByBusiness(this.business.getId());
        this.subItems.removeAll(itemSubItems);
        for (SubItemGroup isig2 : this.itemSubItemGroups) {
            List<SubItem> sis = persistence.getSubItemsFromSubItemGroup(isig2.getId());
            this.subItems.removeAll(sis);
        }
        JsfUtil.addInfoFacesMessage(ProjectConstants.SUBITEM_GROUP_CREATED);

        this.showGroupSelectedButtonDiv = false;
        this.toggleButtonDiv = true;
        this.availableSubItems.clear();
        this.subItemGroup = new SubItemGroup();
        this.addSubItemTable = false;

    }

    public void registerSubItemGroup() {
        if (this.subItemGroup.getName() == null) {
            JsfUtil.addErrorFacesMessage("Please Provide a SubItem Group Name");
            return;
        }
        this.subItemGroup.setName(this.subItemGroup.getName() + "-" + item.getName());
        persistence.mergeItem(item);
        try {
            this.subItemGroup.setActive(true);
            this.subItemGroup.setBusiness(business);
            this.subItemGroup.setOwner(user);
            this.subItemGroup.setItem(item);

            persistence.create(this.subItemGroup);
        } catch (Exception e) {
            System.out.println("EXception >>>>" + e);
            JsfUtil.addErrorFacesMessage("Group Name Already Exist");
            this.subItemGroup = new SubItemGroup();
            // this.addSubItemTable = false;
            return;
        }

        try {
            for (SubItem s : this.checkedSubItem) {
                persistence.mergeSubItem(s);
                SubitemgroupSubitem ss = new SubitemgroupSubitem();
                ss.setSubItem(s);
                ss.setSubItemGroup(this.subItemGroup);
                persistence.create(ss);
            }
        } catch (Exception e) {
            System.out.println("EXception >>>>" + e);
            JsfUtil.addErrorFacesMessage(ProjectConstants.OPERATION_UNSUCCESSFUL);
            this.subItemGroup = new SubItemGroup();
            //this.addSubItemTable = false;
            return;
        }

        try {
            ItemSubItemGroup isig = new ItemSubItemGroup();
            isig.setMainItem(item);
            isig.setSubItemGroup(this.subItemGroup);
            persistence.create(isig);
        } catch (Exception e) {
            System.out.println("EXception >>>>" + e);
            JsfUtil.addErrorFacesMessage(ProjectConstants.OPERATION_UNSUCCESSFUL);
            this.subItemGroup = new SubItemGroup();
            // this.addSubItemTable = false;
            return;
        }

        populateItemSubItems();
        populateItemSubItemGroups();
        this.subItems.clear();
        this.subItems = persistence.getActiveSubItemByBusiness(this.business.getId());
        this.subItems.removeAll(itemSubItems);
        for (SubItemGroup isig2 : this.itemSubItemGroups) {
            List<SubItem> sis = persistence.getSubItemsFromSubItemGroup(isig2.getId());
            this.subItems.removeAll(sis);
        }
        JsfUtil.addInfoFacesMessage(ProjectConstants.SUBITEM_GROUP_CREATED);

        this.showGroupSelectedButtonDiv = false;
        this.toggleButtonDiv = true;
        this.availableSubItems.clear();
        this.subItemGroup = new SubItemGroup();
        this.addSubItemTable = false;

    }

    public String registerSubItem() {
        try {
            this.subItem.setCreatedDate(new Date());
            this.subItem.setModifiedDate(new Date());
            this.subItem.setBusiness(business);
            this.subItem.setActive(false);
            this.subItem.setOwner(this.user);
            this.subItem.setPrice(this.subItem.getPrice() == null ? 0.0 : this.subItem.getPrice());
            persistence.create(this.subItem);
            squery = String.valueOf(this.subItem.getId());
            bquery = String.valueOf(this.subItem.getBusiness().getId());
            Faces.setViewAttribute(ProjectConstants.PAGE_QUERY_PARAM_BUSINESS, bquery);
            Faces.setViewAttribute(ProjectConstants.PAGE_QUERY_PARAM_SUBITEM, squery);
            JsfUtil.addInfoFacesMessage(ProjectConstants.SUBITEM_CREATED);
            return "subItemDetail?faces-redirect=true&includeViewParams=true";
        } catch (Exception e) {
            e.getMessage();
            JsfUtil.addErrorFacesMessage(ProjectConstants.OPERATION_UNSUCCESSFUL);
            this.subItem = new SubItem();
            return null;
        }
    }

    public void updateSubItem() {
        try {
            this.subItem.setPrice(this.subItem.getPrice() == null ? 0.0 : this.subItem.getPrice());
            persistence.update(this.subItem);
            JsfUtil.addInfoFacesMessage(ProjectConstants.UPDATE_SUCCESSFUL);
        } catch (Exception e) {
            JsfUtil.addWarningFacesMessage(ProjectConstants.UPDATE_UNSUCCESSFUL);
            e.getMessage();
        }
    }

    public void disableSubItemGroup(Long id) {
        try {
            SubItemGroup group = persistence.find(SubItemGroup.class, id);
            group.setActive(false);
            persistence.update(group);
            JsfUtil.addInfoFacesMessage(ProjectConstants.ITEM_DISABLED);
        } catch (Exception e) {
            JsfUtil.addErrorFacesMessage(ProjectConstants.OPERATION_UNSUCCESSFUL);
            throw new IllegalArgumentException("Error ::::: ", e);
        }
    }

    public void enableSubItemGroup(Long id) {
        try {

            SubItemGroup group = persistence.find(SubItemGroup.class, id);
            group.setActive(true);
            persistence.update(group);
            JsfUtil.addInfoFacesMessage(ProjectConstants.ITEM_DISABLED);
        } catch (Exception e) {
            JsfUtil.addErrorFacesMessage(ProjectConstants.OPERATION_UNSUCCESSFUL);
            throw new IllegalArgumentException("Error ::::: ", e);
        }
    }

    public void disableSubItem() {
        try {
            this.subItem.setModifiedDate(new Date());
            this.subItem.setActive(false);
            persistence.update(this.subItem);
            JsfUtil.addInfoFacesMessage(ProjectConstants.SUBITEM_DISABLED);
        } catch (Exception e) {
            JsfUtil.addErrorFacesMessage(ProjectConstants.OPERATION_UNSUCCESSFUL);
            throw new IllegalArgumentException("Error ::::: ", e);
        }
    }

    public void enableSubItem() {
        try {
            this.subItem.setModifiedDate(new Date());
            this.subItem.setActive(true);
            persistence.update(this.subItem);
            JsfUtil.addInfoFacesMessage(ProjectConstants.SUBITEM_ENABLED);
        } catch (Exception e) {
            JsfUtil.addErrorFacesMessage(ProjectConstants.OPERATION_UNSUCCESSFUL);
            throw new IllegalArgumentException("Error ::::: ", e);
        }
    }

    public String registerItem() {
        try {
            this.item.setCreatedDate(new Date());
            this.item.setModifiedDate(new Date());
            this.item.setBusiness(business);
            this.item.setOwner(this.user);
            this.item.setPrice(this.item.getPrice() == null ? 0.0 : this.item.getPrice());
            persistence.create(this.item);
            iquery = String.valueOf(this.item.getId());
            bquery = String.valueOf(this.item.getBusiness().getId());
            Faces.setViewAttribute(ProjectConstants.PAGE_QUERY_PARAM_BUSINESS, bquery);
            Faces.setViewAttribute(ProjectConstants.PAGE_QUERY_PARAM_ITEM, iquery);
            JsfUtil.addInfoFacesMessage(ProjectConstants.KIOSK_CREATED);
            return "itemDetail?faces-redirect=true&includeViewParams=true";
        } catch (Exception e) {
            e.getMessage();
            JsfUtil.addErrorFacesMessage(ProjectConstants.OPERATION_UNSUCCESSFUL);
            this.item = new Item();
            return null;
        }
    }

    public void updateItem() {
        try {
            this.item.setPrice(this.item.getPrice() == null ? 0.0 : this.item.getPrice());
            persistence.update(this.item);
            JsfUtil.addInfoFacesMessage(ProjectConstants.UPDATE_SUCCESSFUL);
        } catch (Exception e) {
            JsfUtil.addWarningFacesMessage(ProjectConstants.UPDATE_UNSUCCESSFUL);
            e.getMessage();
        }
    }

    public void disableItem() {
        try {
            this.item.setModifiedDate(new Date());
            this.item.setStatus(false);
            persistence.update(this.item);
            JsfUtil.addInfoFacesMessage(ProjectConstants.ITEM_DISABLED);
        } catch (Exception e) {
            JsfUtil.addErrorFacesMessage(ProjectConstants.OPERATION_UNSUCCESSFUL);
            throw new IllegalArgumentException("Error ::::: ", e);
        }
    }

    public void enableItem() {
        try {
            this.item.setModifiedDate(new Date());
            this.item.setStatus(true);
            persistence.update(this.item);
            JsfUtil.addInfoFacesMessage(ProjectConstants.ITEM_ENABLED);
        } catch (Exception e) {
            JsfUtil.addErrorFacesMessage(ProjectConstants.OPERATION_UNSUCCESSFUL);
            throw new IllegalArgumentException("Error ::::: ", e);
        }
    }

    public void addItemVariation() {
        if (this.itemVariation.getDescription() != null) {
            try {
                Item i = persistence.find(Item.class, this.item.getId());
                this.itemVariation.setItem(i);
                this.itemVariation.setActive(true);
                this.itemVariation.setPrice(this.itemVariation.getPrice() == null ? 0.0 : this.itemVariation.getPrice());
                persistence.create(this.itemVariation);

                List<SubItem> sis = persistence.getAllItemSubItems(this.item.getId());
                List<SubItemGroup> sigs = persistence.getAllItemSubItemGroups(this.item.getId());
                for (SubItemGroup group : sigs) {
                    sis.addAll(persistence.getSubItemsFromSubItemGroup(group.getId()));
                }

                for (SubItem si : sis) {
                    VariationSubItemCost vsic = new VariationSubItemCost();
                    vsic.setActive(false);
                    vsic.setItemVariation(itemVariation);
                    vsic.setMainItem(item);
                    vsic.setPrice(0.0);
                    vsic.setSubItem(si);
                    persistence.create(vsic);
                }

                this.itemVariation = new ItemVariation();
                JsfUtil.addInfoFacesMessage(ProjectConstants.OPERATION_SUCCESSFUL);
            } catch (Exception e) {
                JsfUtil.addErrorFacesMessage(ProjectConstants.OPERATION_UNSUCCESSFUL);
                e.getMessage();
            }
        } else {
            JsfUtil.addErrorFacesMessage("Please Input a Variation Name");
        }

        populateItemVariations();
    }

    public BufferedImage scale(BufferedImage img, int targetWidth, int targetHeight) {

        int type = (img.getTransparency() == Transparency.OPAQUE) ? BufferedImage.TYPE_INT_RGB : BufferedImage.TYPE_INT_ARGB;
        BufferedImage ret = img;
        BufferedImage scratchImage = null;
        Graphics2D g2 = null;

        int w = img.getWidth();
        int h = img.getHeight();

        int prevW = w;
        int prevH = h;

        do {
            if (w > targetWidth) {
                w /= 2;
                w = (w < targetWidth) ? targetWidth : w;
            }

            if (h > targetHeight) {
                h /= 2;
                h = (h < targetHeight) ? targetHeight : h;
            }

            if (scratchImage == null) {
                scratchImage = new BufferedImage(w, h, type);
                g2 = scratchImage.createGraphics();
            }

            g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
                    RenderingHints.VALUE_INTERPOLATION_BILINEAR);
            g2.drawImage(ret, 0, 0, w, h, 0, 0, prevW, prevH, null);

            prevW = w;
            prevH = h;
            ret = scratchImage;
        } while (w != targetWidth || h != targetHeight);

        if (g2 != null) {
            g2.dispose();
        }

        if (targetWidth != ret.getWidth() || targetHeight != ret.getHeight()) {
            scratchImage = new BufferedImage(targetWidth, targetHeight, type);
            g2 = scratchImage.createGraphics();
            g2.drawImage(ret, 0, 0, null);
            g2.dispose();
            ret = scratchImage;
        }

        return ret;

    }

    public void uploadFiletoDB() throws IOException {
        System.out.println("UploadFiletoDB ::::::::: ");
        if (file != null || file.getSize() != 0) {

            String ext = "";
            int i = file.getSubmittedFileName().lastIndexOf('.');
            if (i > 0) {
                ext = file.getSubmittedFileName().substring(i + 1);
            }

            if (ext.equalsIgnoreCase("jpg") || ext.equalsIgnoreCase("png") || ext.equalsIgnoreCase("jpeg")) {
                final String path = System.getProperty("jboss.server.data.dir");
                final String pathAttache = path + File.separator + "img" + File.separator;
                final String fname = UUID.randomUUID().toString().split("-", 0)[0] + file.getSubmittedFileName();
                try {
                    File outputFile = new File(pathAttache, fname);

                    BufferedImage bImage = scale(ImageIO.read(file.getInputStream()), 448, 336);
                    try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
                        ImageIO.write(bImage, "jpg", baos);
                        baos.flush();
                        fileContent = baos.toByteArray();
                    }

                    try (OutputStream os = new FileOutputStream(outputFile, true)) {
                        os.write(fileContent);
                        os.flush();
                        os.close();
                    }

                    ItemImage ii = persistence.getItemImage(this.item);
                    if (ii != null) {
                        ii.setItem(this.item);
                        ii.setImagePath(path);
                        ii.setImageUrl(File.separator + "img" + File.separator + fname);
                        ii.setImage(fileContent);
                        persistence.update(ii);
                    } else {
                        ItemImage image = new ItemImage();
                        image.setItem(this.item);
                        image.setImagePath(path);
                        image.setImageUrl(File.separator + "img" + File.separator + fname);
                        image.setImage(fileContent);
                        persistence.create(image);
                    }

                    JsfUtil.addInfoFacesMessage(ProjectConstants.OPERATION_SUCCESSFUL);

                } catch (IOException e) {
                    e.printStackTrace();
                    JsfUtil.addErrorFacesMessage(ProjectConstants.OPERATION_UNSUCCESSFUL);
                }
            } else {
                JsfUtil.addErrorFacesMessage("Your image should be in .jpg, .jpeg or .png format");
            }
        } else {
            JsfUtil.addErrorFacesMessage("Please Select an Image to Upload");
        }
        //populateImageContent();
        populateFileImagesContent();
    }

//    public void uploadSubItemFiletoDB() throws IOException {
//        if (file.getSize() != 0) {
//            fileContent = Utils.toByteArray(file.getInputStream());
//            final String path = System.getProperty("jboss.server.data.dir");
//            final String pathAttache = path + File.separator + "img" + File.separator;
//            System.out.println("Image Path ::::: " + pathAttache);
//            try {
//                final String fname = UUID.randomUUID().toString().split("-", 0)[0] + file.getSubmittedFileName();
//                File outputFile = new File(pathAttache, fname);
//
//                try (OutputStream os = new FileOutputStream(outputFile, true)) {
//                    os.write(Utils.toByteArray(this.file.getInputStream()));
//                    os.flush();
//                    os.close();
//                }
//
//                this.subItem.setLogoPath(path);
//                this.subItem.setLogoName(File.separator + "img" + File.separator + fname);
//
//                persistence.update(this.subItem);
//                JsfUtil.addInfoFacesMessage(ProjectConstants.OPERATION_SUCCESSFUL);
//
//            } catch (IOException e) {
//                e.getLocalizedMessage();
//                JsfUtil.addErrorFacesMessage(ProjectConstants.OPERATION_UNSUCCESSFUL);
//            }
//        }
//        displaySubItemImage();
//    }
    public void removeImageFromDatabase(String imageUrl) {
        ItemImage ii = persistence.findSingleWithQuery("select o from ItemImage o where o.imgUrl = " + imageUrl);
        if (ii != null) {
            persistence.delete(ii);
            File f = new File(ii.getImagePath(), ii.getImageUrl());
            f.delete();
        } else {
            System.err.println("File is not in the DB");
        }
    }

    private void populateImageContent() {
        List<byte[]> bs = new ArrayList<>();
        //final String path = JsfUtil.getHttpServletRequest().getLocalAddr() + ":8080/KioskII";
        List<ItemImage> iis = persistence.getItemImagesByItemId(this.item.getId());
        //List<ItemImage> iis = persistence.findWithQuery("SELECT o FROM ItemImage o WHERE o.item.id =" + this.item.getId());
        if (iis.isEmpty()) {
            itemImageContents = Collections.EMPTY_LIST;
        } else {
            System.out.println("Image count ::::::: " + iis.size());
            for (ItemImage ii : iis) {
                System.out.println("Image Path :::: " + ii.getImagePath());
                System.out.println("Image URL :::: " + ii.getImageUrl());
                try (FileInputStream fis = new FileInputStream(new File(ii.getImagePath(), ii.getImageUrl()))) {
                    byte[] b = Utils.toByteArray(fis);
                    bs.add(b);
                    fis.close();
                    //itemImageContents.add(b);
//                    itemImageContents.add(Utils.toByteArray(fis));
//                    System.out.println("File was successfully uploaded count ::::::: " + itemImageContents.size());
                } catch (Exception e) {
                    System.err.println("Error occured while load file ::: " + e.getLocalizedMessage());
                }
            }
            System.out.println("Bytes List Count ::::: " + bs.size());
            itemImageContents.addAll(bs);
        }
    }

    public void uploadItemImage() {
        try {
            Item i = persistence.find(Item.class, this.item.getId());
            this.itemImage.setImage(Utils.toByteArray(file.getInputStream()));
            this.itemImage.setItem(i);
            persistence.update(this.itemImage);
            this.itemImage = new ItemImage();
            //populateItemImages();
            JsfUtil.addInfoFacesMessage(ProjectConstants.OPERATION_SUCCESSFUL);
        } catch (IOException e) {
            e.getMessage();
            JsfUtil.addErrorFacesMessage(ProjectConstants.OPERATION_UNSUCCESSFUL);
        }

        populateFileImagesContent();
    }

    public void removeImage(Long id) {
        try {
            ItemImage ii = persistence.find(ItemImage.class, id);
            persistence.delete(ii);
            populateItemImages();
            JsfUtil.addInfoFacesMessage(ProjectConstants.OPERATION_SUCCESSFUL);
        } catch (Exception e) {
            e.getLocalizedMessage();
            JsfUtil.addErrorFacesMessage(ProjectConstants.OPERATION_UNSUCCESSFUL);
        }
    }

    public void cancelVariationUpdate() {
        this.itemVariation = new ItemVariation();
        this.variationSubItemCost = new VariationSubItemCost();
    }

    public void updateItemVariation() {
        try {
            this.itemVariation.setPrice(this.itemVariation.getPrice() == null ? 0.0 : this.itemVariation.getPrice());
            persistence.update(this.itemVariation);
            this.itemVariation = new ItemVariation();
            JsfUtil.addInfoFacesMessage(ProjectConstants.UPDATE_SUCCESSFUL);
        } catch (Exception e) {
            JsfUtil.addErrorFacesMessage(ProjectConstants.UPDATE_UNSUCCESSFUL);
            e.getLocalizedMessage();
        }
        populateItemVariations();
    }

    public void updateVariationSubItemCost() {
        try {
            this.variationSubItemCost.setPrice(this.variationSubItemCost.getPrice() == null ? 0.0 : this.variationSubItemCost.getPrice());
            persistence.update(this.variationSubItemCost);
            this.variationSubItemCost = new VariationSubItemCost();
            JsfUtil.addInfoFacesMessage(ProjectConstants.UPDATE_SUCCESSFUL);
        } catch (Exception e) {
            JsfUtil.addErrorFacesMessage(ProjectConstants.UPDATE_UNSUCCESSFUL);
            e.getLocalizedMessage();
        }
        // populateItemVariations();
        this.subItemCosts.clear();
        this.subItemCosts = persistence.getSubItemCostByItemVariation(itemVariation);

    }

    public void disableItemVariation(ItemVariation iv) {
        try {
            if (null != iv) {
                iv.setActive(false);
                persistence.update(iv);
            }
            this.itemVariation = new ItemVariation();
            JsfUtil.addInfoFacesMessage(ProjectConstants.OPERATION_SUCCESSFUL);
        } catch (Exception e) {
            JsfUtil.addErrorFacesMessage("Operation failed. " + e.getLocalizedMessage());
        }
    }

    public void enableItemVariation(ItemVariation iv) {
        try {
            if (null != iv) {
                iv.setActive(true);
                persistence.update(iv);
            }
            this.itemVariation = new ItemVariation();
            JsfUtil.addInfoFacesMessage(ProjectConstants.OPERATION_SUCCESSFUL);
        } catch (Exception e) {
            JsfUtil.addErrorFacesMessage("Operation failed. " + e.getLocalizedMessage());
        }
    }

    public String registerNewItem() {
        item = new Item();
        return "ownerCreateItem";
    }

    public String viewItemList() {
        return "ownerManageItems";
    }

    public void editItemVariation(ItemVariation itemVariation) {
        this.itemVariation = itemVariation;
        this.subItemCosts = persistence.getSubItemCostByItemVariation(itemVariation);
    }

    private void populateFileImagesContent() {
        ItemImage image = persistence.getItemImage(item);
        this.itemImage = image;
////List<byte[]> bs = new ArrayList<>();
//                //final String path = JsfUtil.getHttpServletRequest().getLocalAddr() + ":8080/KioskII";
//        List<ItemImage> iis = persistence.getItemImagesByItemId(this.item.getId());
//        //List<ItemImage> iis = persistence.findWithQuery("SELECT o FROM ItemImage o WHERE o.item.id =" + this.item.getId());
//        if (iis.isEmpty()) {
//            //itemImageContents = Collections.EMPTY_LIST;
//            fileImages = Collections.EMPTY_LIST;
//        } else {
//            fileImages.clear();
//            System.out.println("Image count ::::::: " + iis.size());
//            for (ItemImage ii : iis) {
//                System.out.println("Image Path :::: " + ii.getImagePath());
//                System.out.println("Image URL :::: " + ii.getImageUrl());
//                try (FileInputStream fis = new FileInputStream(new File(ii.getImagePath(), ii.getImageUrl()))) {
//                    byte[] b = Utils.toByteArray(fis);
//                    //bs.add(b);
//
//                    Image image = new Image();
//                    image.setItemId(ii.getId());
//                    image.setContent(b);
//                    image.setFileName(ii.getImageUrl());
//                    image.setFilePath(ii.getImagePath());
//
//                    fileImages.add(image);
//
//                    fis.close();
//                    //itemImageContents.add(b);
////                    itemImageContents.add(Utils.toByteArray(fis));
////                    System.out.println("File was successfully uploaded count ::::::: " + itemImageContents.size());
//                } catch (Exception e) {
//                    System.err.println("Error occured while load file ::: " + e.getLocalizedMessage());
//                }
//            }
//            System.out.println("Bytes List Count ::::: " + fileImages.size());
//            //itemImageContents.addAll(bs);
//        }
    }

//    public void deleteSubItemImage() {
//        if (subItem != null) {
//            try {
//                SubItem si = persistence.find(SubItem.class, subItem.getId());
//                si.setLogo(null);
//                si.setLogoName(null);
//                si.setLogoPath(null);
//                persistence.update(si);
//                Path path = FileSystems.getDefault().getPath(this.subItem.getLogoPath(), this.subItem.getLogoName());
//                Files.deleteIfExists(path);
//                this.subItem = si;
//                displaySubItemImage();
//                JsfUtil.addInfoFacesMessage(ProjectConstants.OPERATION_SUCCESSFUL);
//            } catch (IOException e) {
//                System.err.println("Error deleteItemImage ::::: " + e.getMessage());
//                JsfUtil.addInfoFacesMessage(ProjectConstants.OPERATION_UNSUCCESSFUL);
//            }
//        }
//    }
    public void deleteItemImage() {
        if (null != this.itemImage) {
            try {
                ItemImage ii = persistence.find(ItemImage.class, this.itemImage.getId());
                persistence.delete(ii);
                Path path = FileSystems.getDefault().getPath(this.itemImage.getImagePath(), this.itemImage.getImageUrl());
                Files.deleteIfExists(path);
                this.itemImage = new ItemImage();
                //fileImages.remove(image);
            } catch (IOException e) {
                System.err.println("Error deleteItemImage ::::: " + e.getMessage());
            }
        }
        populateFileImagesContent();
    }

    public void populateItemVariations() {
        this.itemVariations = persistence.getAllItemVariationByItemId(this.item.getId());
    }

    public void populateItemImages() {
        this.images = persistence.getItemImagesByItemId(this.item.getId());
    }

    public void populateItems() {
        items = persistence.getAllItemByBusinessId(this.business.getId());
    }

    public void populateItemsByBusiness() {
        this.business = persistence.getBusinessByUserIdAndBusinessId(user.getId(), Long.valueOf(bquery));
        if (this.business == null) {
            System.err.println("Dispaly 404 page error");
        } else {
            this.items = persistence.getAllItemByBusinessId(business.getId());
            if (this.items.isEmpty()) {
                System.err.println("Items are empty");
            }
        }
    }

    private void displaySubItemImage() {
        System.out.println(subItem.getId() + " ::::: Image File Path :::: " + subItem.getLogoPath() + " :::: " + subItem.getLogoName());
        if (this.subItem.getLogoName() != null && this.subItem.getLogoPath() != null) {
            try (FileInputStream fis = new FileInputStream(new File(this.subItem.getLogoPath(), this.subItem.getLogoName()))) {
                byte[] b = Utils.toByteArray(fis);
                this.subItem.setLogo(b);
                fis.close();
            } catch (Exception e) {
                System.err.println("Error occured while load file ::: " + e.getLocalizedMessage());
            }
        }

    }

    public void editSubItemGroup() {
        this.updateSubItemGroup = true;
    }

    public KioskUser getUser() {
        return user;
    }

    public void setUser(KioskUser user) {
        this.user = user;
    }

    public PortalPersistence getPersistence() {
        return persistence;
    }

    public void setPersistence(PortalPersistence persistence) {
        this.persistence = persistence;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public List<Item> getItems() {
        populateItemsByBusiness();
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public Business getBusiness() {
        return business;
    }

    public void setBusiness(Business business) {
        this.business = business;
    }

    public String getBquery() {
        return bquery;
    }

    public void setBquery(String bquery) {
        this.bquery = bquery;
    }

    public String getKquery() {
        return kquery;
    }

    public void setKquery(String kquery) {
        this.kquery = kquery;
    }

    public String getIquery() {
        return iquery;
    }

    public void setIquery(String iquery) {
        this.iquery = iquery;
    }

    public ItemImage getItemImage() {
        return itemImage;
    }

    public void setItemImage(ItemImage itemImage) {
        this.itemImage = itemImage;
    }

    public Part getFile() {
        return file;
    }

    public void setFile(Part file) {
        this.file = file;
    }

    public List<ItemImage> getImages() {
        populateItemImages();
        return images;
    }

    public void setImages(List<ItemImage> images) {
        this.images = images;
    }

    public List<Item> getDisabledItems() {
        populateDisabledItems();
        return disabledItems;
    }

    public void setDisabledItems(List<Item> disabledItems) {
        this.disabledItems = disabledItems;
    }

    public byte[] getFileContent() {
        return fileContent;
    }

    public void setFileContent(byte[] fileContent) {
        this.fileContent = fileContent;
    }

    public List<byte[]> getItemImageContents() {
        itemImageContents = new ArrayList<>();
        populateImageContent();
        return itemImageContents;
    }

    public void setItemImageContents(List<byte[]> itemImageContents) {
        this.itemImageContents = itemImageContents;
    }

    public List<SubItem> getActiveSubItems() {
        populateActiveSubItems();
        return subItems;
    }

    public List<SubItem> getInactiveSubItems() {
        populateDisabledSubItems();
        return itemSubItems;
    }

    public List<Item> getActiveItems() {
        populateActiveItems();
        return activeItems;
    }

    public void setActiveItems(List<Item> activeItems) {
        this.activeItems = activeItems;
    }

    public List<Image> getFileImages() {
        if (!fileImages.isEmpty()) {
            itemImageContents.clear();
        }
        populateFileImagesContent();
        return fileImages;
    }

    public void setFileImages(List<Image> fileImages) {
        this.fileImages = fileImages;
    }

    public ItemVariation getItemVariation() {
        return itemVariation;
    }

    public void setItemVariation(ItemVariation itemVariation) {
        this.itemVariation = itemVariation;
    }

    public List<ItemVariation> getItemVariations() {
        populateItemVariations();
        return itemVariations;
    }

    public void setItemVariations(List<ItemVariation> itemVariations) {
        this.itemVariations = itemVariations;
    }

    public SubItem getSubItem() {
        return subItem;
    }

    public void setSubItem(SubItem subItem) {
        this.subItem = subItem;
    }

    public List<SubItem> getSubItems() {
        // populateActiveSubItems();
        return subItems;
    }

    public void setSubItems(List<SubItem> subItems) {
        this.subItems = subItems;
    }

    public String getSquery() {
        return squery;
    }

    public void setSquery(String squery) {
        this.squery = squery;
    }

    public Map<Long, Boolean> getAvailableSubItems() {
        return availableSubItems;
    }

    public void setAvailableSubItems(Map<Long, Boolean> availableSubItems) {
        this.availableSubItems = availableSubItems;
    }

    public Map<Long, Boolean> getSelectedSubItems() {
        return selectedSubItems;
    }

    public void setSelectedSubItems(Map<Long, Boolean> selectedSubItems) {
        this.selectedSubItems = selectedSubItems;
    }

    public List<SubItem> getItemSubItems() {
        // populateDisabledSubItems();
        return itemSubItems;
    }

    public void setItemSubItems(List<SubItem> itemSubItems) {
        this.itemSubItems = itemSubItems;
    }

    public SubItemGroup getSubItemGroup() {
        return subItemGroup;
    }

    public void setSubItemGroup(SubItemGroup subItemGroup) {
        this.subItemGroup = subItemGroup;
        this.business = this.subItemGroup.getBusiness();
    }

    public List<SubItemGroup> getSubItemGroups() {
        return subItemGroups;
    }

    public void setSubItemGroups(List<SubItemGroup> subItemGroups) {
        this.subItemGroups = subItemGroups;
    }

    public Map<Long, Boolean> getAvailableSubItemGroups() {
        return availableSubItemGroups;
    }

    public void setAvailableSubItemGroups(Map<Long, Boolean> availableSubItemGroups) {
        this.availableSubItemGroups = availableSubItemGroups;
    }

    public Map<Long, Boolean> getSelectedSubItemGroups() {
        return selectedSubItemGroups;
    }

    public void setSelectedSubItemGroups(Map<Long, Boolean> selectedSubItemGroups) {
        this.selectedSubItemGroups = selectedSubItemGroups;
    }

    public List<SubItemGroup> getItemSubItemGroups() {
        return itemSubItemGroups;
    }

    public void setItemSubItemGroups(List<SubItemGroup> itemSubItemGroups) {
        this.itemSubItemGroups = itemSubItemGroups;
    }

    public boolean isShowCreateSubItemGroup() {
        return showCreateSubItemGroup;
    }

    public void setShowCreateSubItemGroup(boolean showCreateSubItemGroup) {
        this.showCreateSubItemGroup = showCreateSubItemGroup;
    }

    public boolean isToggleButtonDiv() {
        return toggleButtonDiv;
    }

    public void setToggleButtonDiv(boolean toggleButtonDiv) {
        this.toggleButtonDiv = toggleButtonDiv;
    }

    public boolean isShowGroupSelectedButtonDiv() {
        return showGroupSelectedButtonDiv;
    }

    public void setShowGroupSelectedButtonDiv(boolean showGroupSelectedButtonDiv) {
        this.showGroupSelectedButtonDiv = showGroupSelectedButtonDiv;
    }

    public boolean isShowImageTab() {
        return showImageTab;
    }

    public void setShowImageTab(boolean showImageTab) {
        this.showImageTab = showImageTab;
    }

    public boolean isShowVariationTab() {
        return showVariationTab;
    }

    public void setShowVariationTab(boolean showVariationTab) {
        this.showVariationTab = showVariationTab;
    }

    public boolean isShowSubItemTab() {
        return showSubItemTab;
    }

    public void setShowSubItemTab(boolean showSubItemTab) {
        this.showSubItemTab = showSubItemTab;
    }

    public boolean isShowSubItemGroupTab() {
        return showSubItemGroupTab;
    }

    public void setShowSubItemGroupTab(boolean showSubItemGroupTab) {
        this.showSubItemGroupTab = showSubItemGroupTab;
    }

    public boolean isAddSubItemTable() {
        return addSubItemTable;
    }

    public void setAddSubItemTable(boolean addSubItemTable) {
        this.addSubItemTable = addSubItemTable;
    }

    public boolean isRemoveSubItemTable() {
        return removeSubItemTable;
    }

    public void setRemoveSubItemTable(boolean removeSubItemTable) {
        this.removeSubItemTable = removeSubItemTable;
    }

    public boolean isAddSubItemGroupTable() {
        return addSubItemGroupTable;
    }

    public void setAddSubItemGroupTable(boolean addSubItemGroupTable) {
        this.addSubItemGroupTable = addSubItemGroupTable;
    }

    public boolean isRemoveSubItemGroupTable() {
        return removeSubItemGroupTable;
    }

    public void setRemoveSubItemGroupTable(boolean removeSubItemGroupTable) {
        this.removeSubItemGroupTable = removeSubItemGroupTable;
    }

    public boolean isUpdateSubItemGroup() {
        return updateSubItemGroup;
    }

    public void setUpdateSubItemGroup(boolean updateSubItemGroup) {
        this.updateSubItemGroup = updateSubItemGroup;
    }

    public List<SubItem> getSubItemsFromGroup() {
        return subItemsFromGroup;
    }

    public void setSubItemsFromGroup(List<SubItem> subItemsFromGroup) {
        this.subItemsFromGroup = subItemsFromGroup;
    }

    public List<SubItemGroup> getUniqueItemSubItemGroups() {
        return uniqueItemSubItemGroups;
    }

    public void setUniqueItemSubItemGroups(List<SubItemGroup> uniqueItemSubItemGroups) {
        this.uniqueItemSubItemGroups = uniqueItemSubItemGroups;
    }

    public List<SubItem> getCheckedSubItem() {
        return checkedSubItem;
    }

    public void setCheckedSubItem(List<SubItem> checkedSubItem) {
        this.checkedSubItem = checkedSubItem;
    }

    public Map<Long, Boolean> getAvailableSubItems2() {
        return availableSubItems2;
    }

    public void setAvailableSubItems2(Map<Long, Boolean> availableSubItems2) {
        this.availableSubItems2 = availableSubItems2;
    }

    public VariationSubItemCost getVariationSubItemCost() {
        return variationSubItemCost;
    }

    public void setVariationSubItemCost(VariationSubItemCost variationSubItemCost) {
        this.variationSubItemCost = variationSubItemCost;
    }

    public List<VariationSubItemCost> getSubItemCosts() {
        return subItemCosts;
    }

    public void setSubItemCosts(List<VariationSubItemCost> subItemCosts) {
        this.subItemCosts = subItemCosts;
    }

}
