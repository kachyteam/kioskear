/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.morph.kiosk.controller;

import com.morph.kiosk.persistence.entity.KioskUser;
import com.morph.kiosk.persistence.entity.portal.PortalPage;
import com.morph.kiosk.persistence.entity.portal.PortalPermission;
import com.morph.kiosk.persistence.entity.portal.PortalUser;
import com.morph.kiosk.persistence.entity.portal.PortalUserGroup_AccessGroup;
import com.morph.kiosk.persistence.entity.portal.PortalUserGroup_User;
import com.morph.kiosk.persistence.service.UserPersistence;
import com.morph.kiosk.util.JsfUtil;
import com.morph.kiosk.util.SessionUtils;
import com.morph.util.ProjectConstants;
import java.io.Serializable;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import org.omnifaces.util.Faces;

/**
 *
 * @author Tobi-Morph-PC
 */
public abstract class BaseController implements Serializable {

    @Resource(lookup = "java:global/KioskEJBModule-1.2/UserPersistence!com.morph.kiosk.persistence.service.UserPersistence")
    private UserPersistence persistence;

    @PostConstruct
    public void init() {
        KioskUser user = (KioskUser) SessionUtils.getSession().getAttribute(ProjectConstants.SESSION_KIOSK_USER);
        PortalUser portalUser = (PortalUser) JsfUtil.getHttpServletRequest().getSession().getAttribute(ProjectConstants.SESSION_PORTAL_USER);
        //user = (KioskUser) Faces.getSessionAttribute(ProjectConstants.SESSION_KIOSK_USER);
        //portalUser = (PortalUser) Faces.getSessionAttribute(ProjectConstants.SESSION_PORTAL_USER);
        //KioskUser user = persistence.find(KioskUser.class, Long.valueOf("30"));
        //PortalUser portalUser = persistence.getPortalUserByKioskUser(user.getId());
        initUser(user);
        //System.out.println("Business Module Init Kiosk User :::::::: " + user);
        //System.out.println("Business Module Init Portal User :::::::: " + portalUser);

        //i-get user group
        //ii-get user group's access group
        //iii-get access_group permssions
        List<PortalPermission> l = new LinkedList<>();
        List<String> permissions = new LinkedList<>();
        Set<PortalPage> userPortalMenu = new HashSet<>();
        //System.out.println("Business Base Controller User ::::: " + user);
        if (null != user) {
            PortalUserGroup_User group_User = persistence.getUserGroupByPortalUser(user.getId());
            if (null != group_User) {
                List<PortalUserGroup_AccessGroup> userGroup_AccessGroups = persistence.getUserGroupAccessGroupByUserGroup(group_User.getUserGroup().getId());
                userGroup_AccessGroups.forEach((userGroup_AccessGroup) -> {
                    l.addAll(persistence.getPortalPermissionByAccessGroup(userGroup_AccessGroup.getAccessGroup().getId()));
                });
            }
            permissions = l.stream().map(PortalPermission::getPermissionName).collect(Collectors.toList());
            //System.out.println("Permissions ::::: " + permissions);
            userPortalMenu = l.stream().filter(i -> i.getPage().isMenu()).map(PortalPermission::getPage).collect(Collectors.toSet());
            
//            for (PortalPage portalPage : userPortalMenu) {
//                System.out.println("User Portal Menu List :::::: " + portalPage.getPageName() + portalPage.isMenu());
//            }
        }

        Faces.setSessionAttribute(ProjectConstants.SESSION_PERMISSION_PARAM, permissions);
        Faces.setSessionAttribute(ProjectConstants.SESSION_PORTAL_MENU_PARAM, userPortalMenu);
        //System.out.println(ProjectConstants.SESSION_PERMISSION_PARAM + " ::::: " + permissions);
        //System.out.println(ProjectConstants.SESSION_PORTAL_MENU_PARAM + " :::: " + userPortalMenu);
    }

    abstract void initUser(KioskUser user);

}
