/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.morph.kiosk.controller;

import com.morph.kiosk.persistence.entity.Kiosk;
import com.morph.kiosk.persistence.entity.KioskUser;
import com.morph.kiosk.persistence.entity.portal.BusinessStaff;
import com.morph.kiosk.persistence.entity.portal.BusinessStaffRole;
import com.morph.kiosk.persistence.entity.portal.PortalPermission;
import com.morph.kiosk.persistence.entity.portal.PortalUserGroup;
import com.morph.kiosk.persistence.entity.portal.PortalUserGroup_AccessGroup;
import com.morph.kiosk.persistence.service.PortalPersistence;
import com.morph.kiosk.persistence.service.UserPersistence;
import com.morph.kiosk.util.JsfUtil;
import com.morph.util.ProjectConstants;
import java.io.IOException;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import org.omnifaces.util.Faces;

/**
 *
 * @author Tobi-Morph-PC
 */
@Named("businessUserController")
@SessionScoped
public class BusinessUserController extends BaseController implements Serializable {

    @Resource(lookup = "java:global/KioskEJBModule-1.2/UserPersistence!com.morph.kiosk.persistence.service.UserPersistence")
    private UserPersistence userPersistence;

    @Resource(lookup = "java:global/KioskEJBModule-1.2/PortalPersistence!com.morph.kiosk.persistence.service.PortalPersistence")
    private PortalPersistence portalPersistence;

    private BusinessStaff businessStaff;

    private KioskUser staff = new KioskUser();
    private KioskUser user;

    private List<KioskUser> staffList = new ArrayList<>();
    private List<BusinessStaff> staffs = new ArrayList<>();

    private Kiosk kiosk = new Kiosk();
    private List<Kiosk> kiosks = new ArrayList<>();

    private PortalUserGroup group = new PortalUserGroup();
    private List<PortalUserGroup> userGroups = new ArrayList<>();

    private List<BusinessStaffRole> staffRoles = new ArrayList<>();
    private List<BusinessStaffRole> displayStaffRoles = new ArrayList<>();

    private String selected;
    private String result;

    private List<PortalPermission> permissions = new ArrayList<>();

    public BusinessUserController() {
    }

    @Override
    void initUser(KioskUser user) {
        this.user = user;
        businessStaff = new BusinessStaff();
        //this.staffList = userPersistence.getBusinessStaffListByBusinessOwner(this.user.getId());
        kiosks = portalPersistence.getActiveKioskByUser(user.getId());

        userGroups = portalPersistence.getAllUserGroups().stream().filter(i -> i.isStaffGroup()).collect(Collectors.toList());

        //System.out.println(this.user + "staff list Count :::: " + staffs.size());
    }

    @PostConstruct
    public void initialize() {
        System.out.println("Initialize got called ..... ");
    }

    public void registerStaff() {
        System.out.println("Registering Staff");
        try {
            KioskUser u = userPersistence.getKioskUserByEmailOrPhoneNumber(this.staff.getEmailAddress(), this.staff.getPhoneNumber());
            if (u != null) {
                String message = u.getEmailAddress().equalsIgnoreCase(this.staff.getEmailAddress()) ? ProjectConstants.EMAIL_DUPLICATE : ProjectConstants.PHONENUMBER_DUPLICATE;
                JsfUtil.addWarningFacesMessage(message);
                this.staff = new KioskUser();
            } else {
                System.out.println("No Duplicate :::::: " + this.staff);
                this.staff.setActive(true);
                this.staff.setCreatedDate(new Date());
                this.staff.setDesignation(KioskUser.Designation.STAFF);
                System.out.println("Email Address ;::::::::::::: " + this.staff.getEmailAddress());
                staff.setToken(ProjectConstants.generateSHA256Token(staff.getEmailAddress()));
                userPersistence.create(this.staff);

                System.out.println("Running Business Staff....................");
                businessStaff = new BusinessStaff();
                businessStaff.setCreator(this.user);
                businessStaff.setStaff(this.staff);
                businessStaff.setDateCreated(new Date());
                businessStaff.setActive(false);

                userPersistence.create(businessStaff);

                Faces.setSessionAttribute(ProjectConstants.SESSION_BUSINESS_STAFF, businessStaff);
                FacesContext.getCurrentInstance().getExternalContext().redirect(JsfUtil.getServletContext().getContextPath() + "/user/detail.xhtml");
                //FacesContext.getCurrentInstance().getExternalContext().redirect(JsfUtil.getServletContext().getContextPath() + "/user/dashboard.xhtml");
                JsfUtil.addInfoFacesMessage(ProjectConstants.ACCOUNT_CREATED);
            }
        } catch (IOException e) {
            JsfUtil.addErrorFacesMessage(ProjectConstants.OPERATION_UNSUCCESSFUL);
            e.getLocalizedMessage();
            this.businessStaff = new BusinessStaff();
        }

        getStaffs();
    }

    public void updateKioskStaff() {
        try {
            this.staff.setModifiedDate(new Date());
            userPersistence.update(this.staff);
            JsfUtil.addInfoFacesMessage(ProjectConstants.UPDATE_SUCCESSFUL);
        } catch (Exception e) {
            JsfUtil.addErrorFacesMessage(ProjectConstants.UPDATE_UNSUCCESSFUL);
            e.getLocalizedMessage();
        }
    }

    public void reset() {
        businessStaff = new BusinessStaff();
        this.staff = new KioskUser();
    }

    public void emptyDetailPageRedirect() throws IOException {
        if (null == this.staff || null == this.staff.getId()) {
            ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
            ec.redirect(ec.getRequestContextPath() + "/user/users.xhtml");
        }
    }

    public void updateSelectedUserGroup() {
//        System.out.println("This selected Item Got Called .........................");
//        System.out.println("Selected Kiosk ::::::" + kiosk);
        //userGroups = portalPersistence.getAllUserGroups();
    }

    public void listener(AjaxBehaviorEvent event) {
//        System.out.println("listener");
//        System.out.println("called by " + event.getSource());
//        System.out.println("Selected Kiosk :::::: " + kiosk);
    }

    public void updateBusinessKioskGroup() {
        if (null != kiosk && null != group) {
            try {
                BusinessStaff bs = userPersistence.getBusinessStaffByKioskUser(staff.getId());

                BusinessStaffRole role = userPersistence.getBusinessStaffByStaffAndKiosk(bs.getId(), kiosk.getId(), group.getId());
                if (role != null) {
                    role.setDateLastModified(new Date());
                } else {
                    role = new BusinessStaffRole();
                    role.setActive(false);
                    role.setBusiness(kiosk.getBusiness());
                    role.setDateCreated(new Date());
                    role.setBusinessStaff(userPersistence.getBusinessStaffByKioskUser(this.staff.getId()));
                }
                role.setKiosk(kiosk);
                role.setUserGroup(group);
                role.setBusiness(kiosk.getBusiness());
                role.setKiosk(kiosk);
                userPersistence.update(role);
                JsfUtil.addInfoFacesMessage(ProjectConstants.OPERATION_SUCCESSFUL);
            } catch (Exception e) {
                JsfUtil.addErrorFacesMessage(ProjectConstants.OPERATION_UNSUCCESSFUL);
                e.getLocalizedMessage();
            }
        } else {
            System.err.println("No Update was made.");
        }
    }

    public String viewStaffDetail(KioskUser staff) {
        this.staff = staff;
        return "boUserDetail";
    }

    public void removeStaff(BusinessStaffRole businessStaffRole) {
        try {
            portalPersistence.delete(portalPersistence.find(BusinessStaffRole.class, businessStaffRole.getId()));
            JsfUtil.addInfoFacesMessage(ProjectConstants.OPERATION_SUCCESSFUL);
        } catch (Exception e) {
            e.getLocalizedMessage();
            JsfUtil.addErrorFacesMessage(ProjectConstants.OPERATION_UNSUCCESSFUL);
        }
    }

    public void viewUserGroupPermission(PortalUserGroup userGroup) {
        //System.out.println("viewUserGroupPermission process.....");
        List<PortalUserGroup_AccessGroup> userGroup_AccessGroups = userPersistence.getUserGroupAccessGroupByUserGroup(userGroup.getId());
        userGroup_AccessGroups.forEach(i -> {
            //System.out.println("View User Group I collections ::::: " + userPersistence.getPortalPermissionByAccessGroup(i.getAccessGroup().getId()));
            permissions.addAll(userPersistence.getPortalPermissionByAccessGroup(i.getAccessGroup().getId()));
        });
        getPermissions();
    }

    public void fetchUserRoles(BusinessStaff staff) {
        displayStaffRoles.clear();
        displayStaffRoles = portalPersistence.getBusinessStaffRoleByStaff(staff.getStaff().getId());
    }

    public KioskUser getUser() {
        return user;
    }

    public void setUser(KioskUser user) {
        this.user = user;
    }

    public List<KioskUser> getStaffList() {
        if (!staffList.isEmpty()) {
            staffList.clear();
        }
        staffList = userPersistence.getBusinessStaffListByBusinessOwner(this.user.getId());
        return staffList;
    }

    public void setStaffList(List<KioskUser> staffList) {
        this.staffList = staffList;
    }

    public List<Kiosk> getKiosks() {
        return kiosks;
    }

    public void setKiosks(List<Kiosk> kiosks) {
        this.kiosks = kiosks;
    }

    public List<BusinessStaff> getStaffs() {
        if (!staffs.isEmpty()) {
            staffs.clear();
        }
        staffs = userPersistence.getAllBusinessStaffByBusinessOnwer(this.user.getId());
        return staffs;
    }

    public void setStaffs(List<BusinessStaff> staffs) {
        this.staffs = staffs;
    }

    public PortalUserGroup getGroup() {
        return group;
    }

    public void setGroup(PortalUserGroup group) {
        this.group = group;
    }

    public Kiosk getKiosk() {
        return kiosk;
    }

    public void setKiosk(Kiosk kiosk) {
        this.kiosk = kiosk;
    }

    public List<PortalUserGroup> getUserGroups() {
        return userGroups;
    }

    public void setUserGroups(List<PortalUserGroup> userGroups) {
        this.userGroups = userGroups;
    }

    public BusinessStaff getBusinessStaff() {
        return businessStaff;
    }

    public void setBusinessStaff(BusinessStaff businessStaff) {
        this.businessStaff = businessStaff;
    }

    public String getSelected() {
        return selected;
    }

    public void setSelected(String selected) {
        this.selected = selected;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public KioskUser getStaff() {
        return staff;
    }

    public void setStaff(KioskUser staff) {
        this.staff = staff;
    }

    public List<BusinessStaffRole> getStaffRoles() {
        staffRoles = portalPersistence.getBusinessStaffRoleByStaff(staff.getId());
        return staffRoles;
    }

    public void setStaffRoles(List<BusinessStaffRole> staffRoles) {
        this.staffRoles = staffRoles;
    }

    public List<PortalPermission> getPermissions() {
        return permissions;
    }

    public void setPermissions(List<PortalPermission> permissions) {
        this.permissions = permissions;
    }

    public List<BusinessStaffRole> getDisplayStaffRoles() {
        return displayStaffRoles;
    }

    public void setDisplayStaffRoles(List<BusinessStaffRole> displayStaffRoles) {
        this.displayStaffRoles = displayStaffRoles;
    }

}
