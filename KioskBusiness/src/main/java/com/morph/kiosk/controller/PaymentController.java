/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.morph.kiosk.controller;

import com.morph.kiosk.util.JsfUtil;
import com.morph.kiosk.persistence.entity.KioskUser;
import com.morph.kiosk.persistence.entity.payment.Interswitch;
import com.morph.kiosk.persistence.entity.payment.MorphExpress;
import com.morph.kiosk.persistence.entity.payment.PaymentProvider;
import com.morph.kiosk.persistence.service.PortalPersistence;
import com.morph.util.ProjectConstants;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.faces.event.ValueChangeEvent;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import org.omnifaces.util.Faces;

/**
 *
 * @author Tobi-Morph-PC
 */
@Named(value = "paymentController")
@ViewScoped
public class PaymentController extends BaseController implements Serializable {

    @Resource(lookup = "java:global/KioskEJBModule-1.2/PortalPersistence!com.morph.kiosk.persistence.service.PortalPersistence")
    private PortalPersistence persistence;
    
    private final Logger logger = Logger.getLogger(getClass().getName());

    private KioskUser user = new KioskUser();

    private MorphExpress morphExpress = new MorphExpress();
    private Interswitch interswitch = new Interswitch();

    private PaymentProvider paymentProvider = new PaymentProvider();

    private List<PaymentProvider> paymentProviders = new ArrayList<>();

   // private UserPaymentProvider userPaymentProvider = new UserPaymentProvider();

    private String displayPaymentPanel;

    public PaymentController() {
    }

    @Override
    void initUser(KioskUser user) {
        this.user = user;
    }

    @PostConstruct
    public void initialize() {
        System.out.println("Current user ::::: " + user);
//        this.userPaymentProvider = persistence.getUserPaymentProviderbyBusinessOwnerId(this.user.getId());
//        if(null != userPaymentProvider){
//            this.paymentProvider = userPaymentProvider.getProvider();
//        }
        this.paymentProvider = persistence.getEnabledPaymentProvider();
    }
    
//    public void loadUserPaymentProvider() {
//        this.userPaymentProvider = persistence.getUserPaymentProviderbyBusinessOwnerId(this.user.getId());
//    }

   
    /**
     * Payment Provider is tied to Business Owner
     * Subsequent version will be added to the business
     */
//    public void registerUserPaymentProvider() {
//        logger.info("Registering Payment Provider...");
//        if (paymentProvider == null) {
//            JsfUtil.addWarningFacesMessage("Kindly select a valid Provider");
//            logger.warning("No provider was selected.");
//        } else {
//            try {
//                logger.log(Level.INFO, "Initial Kiosk User Payment Provider {0}", this.userPaymentProvider);
//                logger.log(Level.INFO, "Selected Payment Provider :::: {0}", paymentProvider);
//                this.userPaymentProvider.setBusinessOwnerId(this.user.getId());
//                this.userPaymentProvider.setProvider(paymentProvider);
//                this.userPaymentProvider.setCreatedDate(new Date());
//               // this.userPaymentProvider.setBusinessId();
//                persistence.create(this.userPaymentProvider);
//                logger.log(Level.INFO, "User Payment Provide have been persisted.... {0}", this.userPaymentProvider);
//                JsfUtil.addInfoFacesMessage(ProjectConstants.OPERATION_SUCCESSFUL);
//            } catch (Exception e) {
//                System.out.println("Erroy is "+ e.getMessage());
//                JsfUtil.addErrorFacesMessage(ProjectConstants.OPERATION_UNSUCCESSFUL);
//                e.getLocalizedMessage();
//            }
//        }
//    }

    public void displayPaymentRegistrationPage(ValueChangeEvent e) {
        System.out.println("displayPaymentRegistrationPage ValueChangeEvent ------------------------------------ " + e.getNewValue().toString());

        //this.paymentProvider = (PaymentProvider) e.getNewValue();
        PaymentProvider pp = (PaymentProvider) e.getNewValue();
        System.out.println("selected paymentProvider :::: " + pp.getName());
        if (this.paymentProvider != null) {
            System.out.println("paymentProvider ::::: " + this.paymentProvider);
            if (pp.getName().equalsIgnoreCase(this.paymentProvider.getName())) {
                JsfUtil.addWarningFacesMessage("This provider have already been registered.");
            } else {
                this.paymentProvider = pp;
                displayPaymentPanel = pp.getName();
                System.out.println(displayPaymentPanel);
                System.out.println(this.paymentProvider);
            }
        }
        System.out.println("------------------------------------------------------------------------------");
    }

//    public void saveInterswitchDetail() {
//        System.out.println("Saving Visa Detail ------------------------------------------------------------------------- " + this.paymentProvider);
//        userPaymentProvider = persistence.getUserPaymentProviderbyUserIdAndPaymentProvider(String.valueOf(user.getId()), this.paymentProvider.getId());
//        if (userPaymentProvider == null || userPaymentProvider.getProvider() == null) {
//            System.err.println("User PaymentProvider is currently null.");
//            userPaymentProvider = new UserPaymentProvider();
//            userPaymentProvider.setProvider(this.paymentProvider);
//            userPaymentProvider.setUserInfoId(String.valueOf(user.getId()));
//            persistence.create(userPaymentProvider);
//
//            this.interswitch = new Interswitch();
//            this.interswitch.setCreatedDate(new Date());
//            this.interswitch.setOwnerEmail(this.user.getEmailAddress());
//            this.interswitch.setStatus(Boolean.TRUE);
//            this.interswitch.setOwnerId(String.valueOf(this.user.getId()));
//            persistence.create(this.interswitch);
//
//            //persistence.update(userPaymentProvider);
//            JsfUtil.addInfoFacesMessage("Operation was successful.");
//            displayPaymentPanel = "";
//            displayPaymentPanelCleanup();
//
//        } else {
//            //this.paymentProvider = userPaymentProvider.getProvider();
//            JsfUtil.addWarningFacesMessage("Business already have this payment provider.");
//            System.out.println("Business already have this payment provider.");
//        }
//        System.out.println("------------------------------------------------------------------------------");
//    }
//
//    public void saveMorphExpressDetail() {
//        System.out.println("Saving Visa Detail ------------------------------------------------------------------------- " + this.paymentProvider);
//        userPaymentProvider = persistence.getUserPaymentProviderbyUserIdAndPaymentProvider(String.valueOf(user.getId()), this.paymentProvider.getId());
//        if (userPaymentProvider == null || userPaymentProvider.getProvider() == null) {
//            System.err.println("User PaymentProvider is currently null.");
//            userPaymentProvider = new UserPaymentProvider();
//            userPaymentProvider.setProvider(this.paymentProvider);
//            userPaymentProvider.setUserInfoId(String.valueOf(user.getId()));
//            persistence.create(userPaymentProvider);
//
//            this.morphExpress = new MorphExpress();
//            this.morphExpress.setCreatedDate(new Date());
//            this.morphExpress.setOwnerEmail(String.valueOf(this.user.getId()));
//
//            this.morphExpress.setStatus(Boolean.TRUE);
//            persistence.create(this.morphExpress);
//
//            //persistence.update(userPaymentProvider);
//            JsfUtil.addInfoFacesMessage("Operation was successful.");
//            displayPaymentPanel = "";
//            displayPaymentPanelCleanup();
//
//        } else {
//            //this.paymentProvider = userPaymentProvider.getProvider();
//            JsfUtil.addWarningFacesMessage("Business already have this payment provider.");
//            System.out.println("Business already have this payment provider.");
//        }
//        System.out.println("------------------------------------------------------------------------------");
//    }

    private void displayPaymentPanelCleanup() {
        displayPaymentPanel = "";
    }

    
    public void nullPaymentProviderAlertDisplay(){
        String nullPaymentProviderAlert = (String) Faces.getExternalContext().getSessionMap().remove("nullPaymentProviderAlert");
        System.out.println("nullPaymentProviderAlert :::: " + nullPaymentProviderAlert);
        if(nullPaymentProviderAlert !=  null){
            JsfUtil.addInfoFacesMessage(nullPaymentProviderAlert);
        }
    }
    
    public KioskUser getUser() {
        return user;
    }

    public void setUser(KioskUser user) {
        this.user = user;
    }

    public PortalPersistence getPersistence() {
        return persistence;
    }

    public void setPersistence(PortalPersistence persistence) {
        this.persistence = persistence;
    }

    public MorphExpress getMorphExpress() {
        return morphExpress;
    }

    public void setMorphExpress(MorphExpress morphExpress) {
        this.morphExpress = morphExpress;
    }

    public PaymentProvider getPaymentProvider() {
        return paymentProvider;
    }

    public void setPaymentProvider(PaymentProvider paymentProvider) {
        this.paymentProvider = paymentProvider;
    }

    public List<PaymentProvider> getPaymentProviders() {
        paymentProviders = persistence.getAllPaymentProviders();
        return paymentProviders;
    }

    public void setPaymentProviders(List<PaymentProvider> paymentProviders) {
        this.paymentProviders = paymentProviders;
    }

    public String getDisplayPaymentPanel() {
        return displayPaymentPanel;
    }

    public void setDisplayPaymentPanel(String displayPaymentPanel) {
        this.displayPaymentPanel = displayPaymentPanel;
    }

    public Interswitch getInterswitch() {
        return interswitch;
    }

    public void setInterswitch(Interswitch interswitch) {
        this.interswitch = interswitch;
    }

}
