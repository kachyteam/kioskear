/**
 *Class Name: PromotionController
 *Project Name: KioskBusiness
 *Developer: Onyedika Okafor (ookafor@morphinnovations.com)
 *Version Info:
 *Create Date: Mar 13, 2017 4:24:16 PM
 *(C)Morph Innovations Limited 2017. Morph Innovations Limited Asserts its right to be known
 *as the author and owner of this file and its contents.
 */
package com.morph.kiosk.controller;

import com.morph.kiosk.persistence.entity.Business;
import com.morph.kiosk.persistence.entity.KioskUser;
import com.morph.kiosk.persistence.entity.order.Coupon;
import com.morph.kiosk.persistence.entity.order.CouponBatch;
import com.morph.kiosk.persistence.service.PortalPersistence;
import com.morph.kiosk.util.JsfUtil;
import com.morph.util.ProjectConstants;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

/**
 * @author Onyedika Okafor (ookafor@morphinnovations.com)
 */
@Named(value = "promotionController")
@ViewScoped
public class PromotionController extends BaseController implements Serializable {

    @Resource(lookup = "java:global/KioskEJBModule-1.2/PortalPersistence!com.morph.kiosk.persistence.service.PortalPersistence")
    private PortalPersistence persistence;

    private KioskUser user = new KioskUser();
    private Business business;
    private String iquery;
    private CouponBatch couponBatch = new CouponBatch();
    private CouponBatch editCouponBatch = new CouponBatch();
    private List<Business> businesses = new ArrayList<>();
    private boolean isPercentage = false;
    private boolean isFixedAmount = false;
    private boolean isPlatformCode = false;
    private boolean multiUser = false;
    private String codeType;
    private Integer codeGenerationCount = 0;
    private Date createDate;
    private List<Coupon> coupons = new ArrayList<>();
    private List<CouponBatch> couponBatchs = new ArrayList<>();

    @Override
    void initUser(KioskUser user) {
        this.user = user;
    }

//    public void searchPromoCode() {
//        coupons.clear();
//        if (createDate != null) {
//            try {
//                List<Coupon> cs = persistence.getAllCoupons();
//                DateFormat createdDateFormatter = new SimpleDateFormat("yyyy/MM/dd");
//
//                String sDate = createdDateFormatter.format(createDate);
//                Date searchDate = createdDateFormatter.parse(sDate);
//
//                for (Coupon c : cs) {
//                    String cDate = createdDateFormatter.format(c.getDateGenerated());
//                    Date createdDate = createdDateFormatter.parse(cDate);
//                    if (searchDate.getTime() == createdDate.getTime()) {
//                        coupons.add(c);
//                    }
//                }
//            } catch (ParseException ex) {
//                Logger.getLogger(PromotionController.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        } else {
//            coupons = persistence.getAllCoupons();
//        }
//
//    }
    public void loadCoupons(CouponBatch cb) {
        coupons.clear();
        coupons.addAll(persistence.getAllCouponsByBatch(cb));
    }

    public void loadCouponBatches() {
        couponBatchs.clear();
        List<Business> bs = persistence.geAllActiveBusinessByUser(user.getId());
        for (Business b : bs) {
            couponBatchs.addAll(persistence.getAllCouponBatchesByBusiness(b));
        }

    }

    public void updatePromoCode() {
        try {
            CouponBatch cb = this.editCouponBatch;
            cb.setCouponAmount(Double.parseDouble(String.valueOf(this.couponBatch.getCouponAmount()).replaceAll("[^0-9]+", "")));
            cb.setMaxAmount(Double.parseDouble(String.valueOf(this.couponBatch.getMaxAmount()).replaceAll("[^0-9]+", "")));
            cb.setMinAmount(Double.parseDouble(String.valueOf(this.couponBatch.getMinAmount()).replaceAll("[^0-9]+", "")));
            cb.setPercentage(Double.parseDouble(String.valueOf(this.couponBatch.getPercentage()).replaceAll("[^0-9]+", "")));
            cb.setUsageLimit(Integer.parseInt(String.valueOf(this.couponBatch.getUsageLimit()).replaceAll("[^0-9]+", "")));
            persistence.update(cb);
            JsfUtil.addInfoFacesMessage("Coupon Batch Updated");
            this.editCouponBatch = new CouponBatch();
        } catch (Exception e) {
            System.out.println("EXception >>>>" + e);
            JsfUtil.addErrorFacesMessage(ProjectConstants.OPERATION_UNSUCCESSFUL);
        }
    }

    public void createPromoCode() {
        try {
            int count = 0;
            CouponBatch cb = this.couponBatch;
            cb.setCouponAmount(Double.parseDouble(String.valueOf(this.couponBatch.getCouponAmount()).replaceAll("[^0-9]+", "")));
            cb.setMaxAmount(Double.parseDouble(String.valueOf(this.couponBatch.getMaxAmount()).replaceAll("[^0-9]+", "")));
            cb.setMinAmount(Double.parseDouble(String.valueOf(this.couponBatch.getMinAmount()).replaceAll("[^0-9]+", "")));
            cb.setPercentage(Double.parseDouble(String.valueOf(this.couponBatch.getPercentage()).replaceAll("[^0-9]+", "")));
            cb.setUsageLimit(Integer.parseInt(String.valueOf(this.couponBatch.getUsageLimit()).replaceAll("[^0-9]+", "")));
            cb.setDateGenerated(new Date());
            cb.setPlatform(false);
            persistence.create(cb);
            codeGenerationCount = Integer.parseInt(String.valueOf(codeGenerationCount).replaceAll("[^0-9]+", ""));

            while (codeGenerationCount > 0) {
                Coupon c = new Coupon();
                c.setActive(true);
                c.setCode(UUID.randomUUID().toString().substring(0, 6).toUpperCase());
                c.setUsed(false);
                c.setCouponBatch(cb);
                persistence.create(c);
                codeGenerationCount--;
                count++;
            }
            JsfUtil.addInfoFacesMessage(count + " Codes Generated");
            this.couponBatch = new CouponBatch();
            this.codeGenerationCount = 0;
        } catch (Exception e) {
            System.out.println("EXception >>>>" + e);
            JsfUtil.addErrorFacesMessage(ProjectConstants.OPERATION_UNSUCCESSFUL);
        }

    }

    public void populateBusiness() {
        businesses.clear();
        businesses = persistence.geAllActiveBusinessByUser(user.getId());
    }

    public void showPlatformType(AjaxBehaviorEvent event) {
        isPlatformCode = false;
        if (this.couponBatch.isPlatform()) {
            isPlatformCode = true;
        }
    }

    public void showUsageLimit(AjaxBehaviorEvent event) {
        multiUser = false;
        if (this.couponBatch.isMultipleUser()) {
            multiUser = true;
        }
    }

    public void showCodeType(AjaxBehaviorEvent event) {
        isFixedAmount = false;
        isPercentage = false;
        if (codeType.equalsIgnoreCase("Percentage")) {
            isPercentage = true;
        }
        if (codeType.equalsIgnoreCase("FixedAmount")) {
            isFixedAmount = true;
        }

    }

    public KioskUser getUser() {
        return user;
    }

    public void setUser(KioskUser user) {
        this.user = user;
    }

    public Business getBusiness() {
        return business;
    }

    public void setBusiness(Business business) {
        this.business = business;
    }

    public String getIquery() {
        return iquery;
    }

    public void setIquery(String iquery) {
        this.iquery = iquery;
    }

    public List<Business> getBusinesses() {
        return businesses;
    }

    public void setBusinesses(List<Business> businesses) {
        this.businesses = businesses;
    }

    public boolean isIsPercentage() {
        return isPercentage;
    }

    public void setIsPercentage(boolean isPercentage) {
        this.isPercentage = isPercentage;
    }

    public boolean isIsFixedAmount() {
        return isFixedAmount;
    }

    public void setIsFixedAmount(boolean isFixedAmount) {
        this.isFixedAmount = isFixedAmount;
    }

    public String getCodeType() {
        return codeType;
    }

    public void setCodeType(String codeType) {
        this.codeType = codeType;
    }

    public Integer getCodeGenerationCount() {
        return codeGenerationCount;
    }

    public void setCodeGenerationCount(Integer codeGenerationCount) {
        this.codeGenerationCount = codeGenerationCount;
    }

    public boolean isIsPlatformCode() {
        return isPlatformCode;
    }

    public void setIsPlatformCode(boolean isPlatformCode) {
        this.isPlatformCode = isPlatformCode;
    }

    public boolean isMultiUser() {
        return multiUser;
    }

    public void setMultiUser(boolean multiUser) {
        this.multiUser = multiUser;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public List<Coupon> getCoupons() {
        return coupons;
    }

    public void setCoupons(List<Coupon> coupons) {
        this.coupons = coupons;
    }

    public CouponBatch getCouponBatch() {
        return couponBatch;
    }

    public void setCouponBatch(CouponBatch couponBatch) {
        this.couponBatch = couponBatch;
    }

    public CouponBatch getEditCouponBatch() {
        return editCouponBatch;
    }

    public void setEditCouponBatch(CouponBatch editCouponBatch) {
        this.editCouponBatch = editCouponBatch;
    }

    public List<CouponBatch> getCouponBatchs() {
        return couponBatchs;
    }

    public void setCouponBatchs(List<CouponBatch> couponBatchs) {
        this.couponBatchs = couponBatchs;
    }

}
