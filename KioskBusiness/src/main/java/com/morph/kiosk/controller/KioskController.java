/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.morph.kiosk.controller;

import com.morph.kiosk.util.JsfUtil;
import com.morph.kiosk.persistence.entity.Business;
import com.morph.kiosk.persistence.entity.DistanceCost;
import com.morph.kiosk.persistence.entity.Item;
import com.morph.kiosk.persistence.entity.Kiosk;
import com.morph.kiosk.persistence.entity.KioskActivityLog;
import com.morph.kiosk.persistence.entity.KioskItem;
import com.morph.kiosk.persistence.entity.KioskUser;
import com.morph.kiosk.persistence.entity.order.KioskDeliveryOption;
import com.morph.kiosk.persistence.entity.order.KioskPaymentOption;
import com.morph.kiosk.persistence.service.PortalPersistence;
import com.morph.util.ProjectConstants;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.Asynchronous;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.event.ValueChangeEvent;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import org.omnifaces.util.Faces;

/**
 *
 * @author Tobi-Morph-PC
 */
@Named(value = "kioskController")
@ViewScoped
public class KioskController extends BaseController implements Serializable {

    @Resource(lookup = "java:global/KioskEJBModule-1.2/PortalPersistence!com.morph.kiosk.persistence.service.PortalPersistence")
    private PortalPersistence persistence;

    private final Logger logger = Logger.getLogger(getClass().getName());

    @Inject
    private ItemController itemController;

    private Business business = new Business();
    private KioskUser user = new KioskUser();
    private Kiosk kiosk = new Kiosk();
    private DistanceCost distanceCost = new DistanceCost();
    private List<Kiosk> activeKiosk = new ArrayList<>();
    private List<Kiosk> disabledKiosk = new ArrayList<>();
    private List<Kiosk> allKiosks = new ArrayList<>();

    private Kiosk.KioskType kioskType;

    private String bquery;
    private String kquery;

    private List<Item> selectedItems = new ArrayList<>();
    private List<Item> items = new ArrayList<>();
    private List<Item> kioskItems = new ArrayList<>();

    private KioskDeliveryOption kioskDeliveryOption = new KioskDeliveryOption();
    private List<KioskDeliveryOption> deliveryOptions = new ArrayList<>();
    private List<String> selectedOptions = new ArrayList<>();
    private List<String> selectedPaymentOptions = new ArrayList<>();
    private List<KioskPaymentOption> kioskPaymentOptions = new ArrayList<>();

    private List<KioskActivityLog> kioskActivityLog = new ArrayList<>();
    private List<DistanceCost> distanceCosts = new ArrayList<>();

    private Map<Long, Boolean> availableItems = new HashMap<>();
    private Map<Long, Boolean> selectItems = new HashMap<>();

    private boolean showSelectItemDT = false;
    private boolean showKioskItemDT = false;
    private boolean showKioskItemDiv = false;
    private boolean showDeliveryDiv = false;
    private boolean showPaymentDiv = false;
    private boolean showDistanceCostDiv = false;
    private boolean showScheduleDelivery = false;

    private Double distCost = 0.0;
    private Double dist = 0.0;

    @PostConstruct
    public void initialize() {
        Map<String, String> map = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        for (Map.Entry<String, String> entry : map.entrySet()) {
            String key = entry.getKey();
            Object value = entry.getValue();
        }
        bquery = (String) map.get(ProjectConstants.PAGE_QUERY_PARAM_BUSINESS);
        kquery = (String) map.get(ProjectConstants.PAGE_QUERY_PARAM_KIOSK);

        System.out.println("Parameter ::: "
                + ProjectConstants.PAGE_QUERY_PARAM_KIOSK + "   "
                + kquery + " " + ProjectConstants.PAGE_QUERY_PARAM_BUSINESS + " "
                + bquery + " User " + user.getId());

        if (null != bquery) {
            allKiosks = persistence.getAllKioskByBusinessId(Long.valueOf(bquery));
        }

    }

    @Override
    void initUser(KioskUser user) {
        this.user = user;

    }

    public KioskController() {
    }

    boolean checkBusiness() {
        System.out.println("check bquery ::: " + bquery);
        if (bquery == null || bquery.equalsIgnoreCase("")) {
            return false;
        } else {
            System.out.println("check business parameter ::: " + user.getId() + " " + bquery);
            this.business = persistence.getBusinessByUserIdAndBusinessId(user.getId(), Long.valueOf(bquery));
            //System.out.println("Testing :::: " + persistence.getBusinessByUserIdAndBusinessId(user.getId(), Long.valueOf(bquery)));
            //System.out.println("Testing :::: " + persistence.getActiveBusinessByUserIdAndBusinessId(user.getId(), Long.valueOf(bquery)));
            //System.out.println("check business ::: " + this.business);
            return null != this.business;
        }
    }

    public void loadKioskDetail() {
        boolean b = checkBusiness();
        if (b && ((this.kiosk = persistence.getKioskByKioskAndBusiness(Long.valueOf(kquery), this.business.getId())) != null)) {
            kioskType = this.kiosk.getType();
            logger.log(Level.INFO, "Selected Kiosk Detail {0} Kiosk Type {1}", new Object[]{this.kiosk, kioskType});
        } else {
            logger.warning("Kiosk Detail Page not found.");
            JsfUtil.addWarningFacesMessage(ProjectConstants.PAGE_NOT_FOUND);
        }
    }

    public List<Kiosk.KioskType> getKioskTypeList() {
        return Arrays.asList(Kiosk.KioskType.values());
    }

    public String registerKiosk() {
        try {
            Kiosk k = persistence.getKioskByKioskName(this.kiosk.getName(), this.business.getId());
            if (k == null) {
                this.kiosk.setCreatedDate(new Date());
                this.kiosk.setModifiedDate(new Date());
                this.kiosk.setBusiness(business);
                this.kiosk.setOwner(this.user);
                this.kiosk.setMinDeliveryTime(Integer.parseInt(String.valueOf(this.kiosk.getMinDeliveryTime()).replaceAll("[^0-9]+", "")));
                
                if (null == this.kiosk.getType()) {
                    this.kiosk.setType(Kiosk.KioskType.RESTAURANT);
                }
                //this.kiosk.setType(kioskType);
                persistence.create(this.kiosk);
                kquery = String.valueOf(this.kiosk.getId());
                bquery = String.valueOf(this.kiosk.getBusiness().getId());
                Faces.setViewAttribute(ProjectConstants.PAGE_QUERY_PARAM_BUSINESS, bquery);
                Faces.setViewAttribute(ProjectConstants.PAGE_QUERY_PARAM_KIOSK, kquery);
                JsfUtil.addInfoFacesMessage(ProjectConstants.KIOSK_CREATED);

                setUpDeliveryKioskOption(this.kiosk);
                setUpKioskPaymentOption(this.kiosk);

                return "kioskDetail?faces-redirect=true&includeViewParams=true";
            } else {
                this.kiosk = new Kiosk();
                JsfUtil.addWarningFacesMessage(ProjectConstants.DUPLICATE_ENTRY);
                return null;
            }

        } catch (Exception e) {
            e.getMessage();
            this.kiosk = new Kiosk();
            JsfUtil.addErrorFacesMessage(ProjectConstants.GENERIC_ERROR);
            return null;
        }
    }

    @Asynchronous
    private void setUpDeliveryKioskOption(Kiosk kiosk) {
        for (KioskDeliveryOption.Option k : getDeliveryOption()) {
            KioskDeliveryOption kdo = new KioskDeliveryOption();
            kdo.setActive(false);
            kdo.setCreatedDate(new Date());
            kdo.setUser(user);
            kdo.setDeliveryCost(0.0);
            kdo.setDeliveryOption(k);
            kdo.setLastModifiedDate(new Date());
            kdo.setKiosk(kiosk);
            persistence.create(kdo);
            kdo = new KioskDeliveryOption();
            //kdo.set
        }
    }

    @Asynchronous
    private void setUpKioskPaymentOption(Kiosk kiosk) {
        KioskPaymentOption kpo = new KioskPaymentOption();
        kpo.setActive(Boolean.FALSE);
        kpo.setCreatedDate(new Date());
        kpo.setPaymentOption(KioskPaymentOption.Option.ONLINE);
        kpo.setUser(user);
        kpo.setKiosk(kiosk);
        persistence.create(kpo);

        kpo = new KioskPaymentOption();
        kpo.setActive(Boolean.FALSE);
        kpo.setCreatedDate(new Date());
        kpo.setPaymentOption(KioskPaymentOption.Option.PAYONDELIVERY);
        kpo.setUser(user);
        kpo.setKiosk(kiosk);
        persistence.create(kpo);
//        for (KioskPaymentOption.Option o : getKioskPaymentOption()) {
//            KioskPaymentOption kpo = new KioskPaymentOption();
//            kpo.setActive(Boolean.FALSE);
//            kpo.setCreatedDate(new Date());
//            kpo.setPaymentOption(o);
//            kpo.setUser(user);
//            kpo.setKiosk(kiosk);
//            persistence.create(kpo);
//            kpo = new KioskPaymentOption();
//        }
    }

    public void saveDeliveryOptionUpdate(KioskDeliveryOption kdo) {
        kdo = kioskDeliveryOption;
        System.out.println("Updating KioskDeliveryOption ::: " + this.kioskDeliveryOption.getDeliveryCost());
        this.kioskDeliveryOption.setDeliveryCost(kdo.getDeliveryCost());
        this.kioskDeliveryOption.setLastModifiedDate(new Date());
        persistence.update(this.kioskDeliveryOption);
        this.kioskDeliveryOption = new KioskDeliveryOption();
        getDeliveryOptions();
    }

    public void saveDistanceCostUpdate() {
        try {
            persistence.update(this.distanceCost);
            this.distanceCost = new DistanceCost();
            loadDistanceCost(kioskDeliveryOption);
            JsfUtil.addInfoFacesMessage(ProjectConstants.OPERATION_SUCCESSFUL);
        } catch (Exception e) {
            e.getMessage();
            JsfUtil.addErrorFacesMessage(ProjectConstants.OPERATION_UNSUCCESSFUL);
        }
    }

    public void deleteDistanceCost(DistanceCost kdo) {
        try {
            persistence.delete(kdo);
            loadDistanceCost(kioskDeliveryOption);
            JsfUtil.addInfoFacesMessage(ProjectConstants.OPERATION_SUCCESSFUL);
        } catch (Exception e) {
            e.getMessage();
            JsfUtil.addErrorFacesMessage(ProjectConstants.OPERATION_UNSUCCESSFUL);
        }
    }

    public void createDistanceCost() {
        try {
            Kiosk k = null;
            if (kioskDeliveryOption != null) {
                k = kioskDeliveryOption.getKiosk();
            }
            DistanceCost dc = persistence.getDistanceCostByDistanceAndKiosk(k, dist);

            if (dc == null) {
                dc = new DistanceCost();
                dc.setKiosk(k);
                dc.setCost(distCost);
                dc.setDistance(dist);
                persistence.create(dc);
                this.distanceCost = new DistanceCost();
                distCost = 0.0;
                dist = 0.0;
                loadDistanceCost(kioskDeliveryOption);
                JsfUtil.addInfoFacesMessage(ProjectConstants.OPERATION_SUCCESSFUL);
            } else {
                JsfUtil.addErrorFacesMessage(dist + "km already exists for this kiosk.");
            }

        } catch (Exception e) {
            e.getMessage();
            JsfUtil.addErrorFacesMessage(ProjectConstants.OPERATION_UNSUCCESSFUL);
        }
    }

    public void saveDeliveryOptionUpdate() {
        System.out.println("Updating KioskDeliveryOption ::: " + this.kioskDeliveryOption.getDeliveryCost());
        this.kioskDeliveryOption.setDeliveryCost(this.kioskDeliveryOption.getDeliveryCost() == null ? 0.0 : this.kioskDeliveryOption.getDeliveryCost());
        this.kioskDeliveryOption.setLastModifiedDate(new Date());
        persistence.update(this.kioskDeliveryOption);
        this.kioskDeliveryOption = new KioskDeliveryOption();
        getDeliveryOptions();
    }

    public void cancelDeliveryOptionUpdate(KioskDeliveryOption kdo) {
        this.kioskDeliveryOption = new KioskDeliveryOption();
    }

    public void activateDeliveryOptionVariedCost(KioskDeliveryOption kdo) {
        try {
            KioskDeliveryOption option = persistence.find(KioskDeliveryOption.class, kdo.getId());
            if (option != null) {
                option.setHasVariedCost(true);
                showDistanceCostDiv = false;
                persistence.update(option);
            }
            JsfUtil.addInfoFacesMessage(ProjectConstants.OPERATION_SUCCESSFUL);
        } catch (Exception e) {
            e.getMessage();
            JsfUtil.addErrorFacesMessage(ProjectConstants.OPERATION_UNSUCCESSFUL);
        }
    }

    public void deactivateDeliveryOptionVariedCost(KioskDeliveryOption kdo) {
        try {
            KioskDeliveryOption option = persistence.find(KioskDeliveryOption.class, kdo.getId());
            if (option != null) {
                option.setHasVariedCost(false);
                showDistanceCostDiv = false;
                persistence.update(option);
            }
            JsfUtil.addInfoFacesMessage(ProjectConstants.OPERATION_SUCCESSFUL);
        } catch (Exception e) {
            e.getMessage();
            JsfUtil.addErrorFacesMessage(ProjectConstants.OPERATION_UNSUCCESSFUL);
        }
    }

    public void activateDeliveryOption(KioskDeliveryOption kdo) throws IOException {
        try {
            System.out.println("This got called :::::::: ");
            KioskDeliveryOption option = persistence.find(KioskDeliveryOption.class, kdo.getId());
            if (option != null) {
                option.setActive(true);
                showDistanceCostDiv = false;
                persistence.update(option);
            }
            JsfUtil.addInfoFacesMessage(ProjectConstants.OPERATION_SUCCESSFUL);
        } catch (Exception e) {
            e.getMessage();
            JsfUtil.addErrorFacesMessage(ProjectConstants.OPERATION_UNSUCCESSFUL);
        }
    }

    public void deactivateDeliveryOption(KioskDeliveryOption kdo) {
        try {
            KioskDeliveryOption option = persistence.find(KioskDeliveryOption.class, kdo.getId());
            if (option != null) {
                option.setActive(false);
                showDistanceCostDiv = false;
                persistence.update(option);
            }
            JsfUtil.addInfoFacesMessage(ProjectConstants.OPERATION_SUCCESSFUL);
        } catch (Exception e) {
            e.getMessage();
            JsfUtil.addErrorFacesMessage(ProjectConstants.OPERATION_UNSUCCESSFUL);
        }
    }

    public void loadDistanceCost(KioskDeliveryOption kdo) {
        distanceCosts.clear();
        showDistanceCostDiv = false;
        if (kdo.isHasVariedCost() == true && kdo.getDeliveryOption().equals(KioskDeliveryOption.Option.HOMEDELIVERY)) {
            showDistanceCostDiv = true;
            kioskDeliveryOption = kdo;
            distanceCosts.addAll(persistence.getDistanceCostsByKiosk(kdo.getKiosk()));
        }
    }

    private String messageChecker;

    public void editDeliveryOption(KioskDeliveryOption k) {
        try {
            System.out.println("Delivery Option Initial Clicked :::::::::::::::::: " + k.isEditable());
            messageChecker = "";
            messageChecker = String.valueOf(k.getId());
            kioskDeliveryOption = new KioskDeliveryOption();
            showDistanceCostDiv = false;
            k.setEditable(true);
            System.out.println("Clicked :::::::::::::::::: " + k.isEditable());
            kioskDeliveryOption = k;
            System.out.println("Kiosk elivery Option " + kioskDeliveryOption);
            JsfUtil.addInfoFacesMessage(ProjectConstants.OPERATION_SUCCESSFUL);
        } catch (Exception e) {
            e.getMessage();
            JsfUtil.addErrorFacesMessage(ProjectConstants.OPERATION_UNSUCCESSFUL);
        }
    }

    public void activateKioskPaymentOption(KioskPaymentOption kpo) {
        try {
            KioskPaymentOption option = persistence.find(KioskPaymentOption.class, kpo.getId());
            if (option != null) {
                option.setActive(true);
                persistence.update(option);
            }
            getKioskPaymentOptions();
            JsfUtil.addInfoFacesMessage(ProjectConstants.OPERATION_SUCCESSFUL);
        } catch (Exception e) {
            e.getMessage();
            JsfUtil.addErrorFacesMessage(ProjectConstants.OPERATION_UNSUCCESSFUL);
        }
    }

    public void deactivateKioskPaymentOption(KioskPaymentOption kpo) {
        try {
            KioskPaymentOption option = persistence.find(KioskPaymentOption.class, kpo.getId());
            if (option != null) {
                option.setActive(false);
                persistence.update(option);
            }
            getKioskPaymentOptions();
            JsfUtil.addInfoFacesMessage(ProjectConstants.OPERATION_SUCCESSFUL);
        } catch (Exception e) {
            e.getMessage();
            JsfUtil.addErrorFacesMessage(ProjectConstants.OPERATION_UNSUCCESSFUL);
        }
    }

    public void reset() {
        kiosk = new Kiosk();
    }

    public void updateKiosk() {
        try {
            System.err.println("Updating Kiosk ::::: " + kioskType);
            this.kiosk.setMinDeliveryTime(Integer.parseInt(String.valueOf(this.kiosk.getMinDeliveryTime()).replaceAll("[^0-9]+", "")));
            persistence.update(this.kiosk);
            this.kiosk.setType(kioskType);
            JsfUtil.addInfoFacesMessage(ProjectConstants.UPDATE_SUCCESSFUL);
        } catch (Exception e) {
            JsfUtil.addErrorFacesMessage(ProjectConstants.UPDATE_UNSUCCESSFUL);
            e.getMessage();
        }
    }

    public void disableKiosk() {
        try {
            this.kiosk.setModifiedDate(new Date());
            this.kiosk.setStatus(false);
            this.kiosk.setMinDeliveryTime(Integer.parseInt(String.valueOf(this.kiosk.getMinDeliveryTime()).replaceAll("[^0-9]+", "")));
            persistence.update(this.kiosk);
            JsfUtil.addInfoFacesMessage(ProjectConstants.KIOSK_DISABLED);
        } catch (Exception e) {
            JsfUtil.addErrorFacesMessage(ProjectConstants.UPDATE_UNSUCCESSFUL);
            throw new IllegalArgumentException("Error ::::: ", e);
        }
    }

    public void enableKiosk() {
        try {
            this.kiosk.setModifiedDate(new Date());
            this.kiosk.setStatus(true);
            this.kiosk.setMinDeliveryTime(Integer.parseInt(String.valueOf(this.kiosk.getMinDeliveryTime()).replaceAll("[^0-9]+", "")));
            persistence.update(this.kiosk);
            JsfUtil.addInfoFacesMessage(ProjectConstants.KIOSK_ENABLED);
        } catch (Exception e) {
            JsfUtil.addErrorFacesMessage(ProjectConstants.UPDATE_UNSUCCESSFUL);
            throw new IllegalArgumentException("Error ::::: ", e);
        }
    }

    public void populateKiosksByBusiness() {
        this.business = persistence.getBusinessByUserIdAndBusinessId(user.getId(), Long.valueOf(bquery));
        if (this.business == null) {
            System.err.println(ProjectConstants.PAGE_NOT_FOUND);
        } else //this.kiosks = persistence.getKioskByBusiness(business.getId());
        {
            if (this.kiosk == null) {
                JsfUtil.addWarningFacesMessage(ProjectConstants.PAGE_NOT_FOUND);
            }
        }
    }

    public void populateActiveKiosks() {
        if (checkBusiness() != false) {
            this.activeKiosk = persistence.getActiveKioskByBusiness(this.business.getId());
        } else {
            this.activeKiosk = persistence.getActiveKioskByUser(this.user.getId());
        }
    }

    public void populateDisabledKiosk() {
        if (checkBusiness() != false) {
            this.disabledKiosk = persistence.getDisabledKioskByUser(this.business.getId());
        } else {
            this.disabledKiosk = persistence.getDisabledKioskByUser(this.user.getId());
        }
    }

    public void displaySelectedBusiness(ValueChangeEvent e) {
        if (e.getNewValue() == null) {
            bquery = null;
        } else {
            Business bb = (Business) e.getNewValue();
            bquery = String.valueOf(bb.getId());
            this.business = bb;
            populateActiveKiosks();
            populateDisabledKiosk();
        }
    }

    public void populateItemsByBusiness() {
        try {
            List<Item> is = persistence.getItemFromKioskItemByKioskId(this.kiosk.getId());
            this.items.clear();
            this.items.addAll(itemController.getActiveItems());
            //itemController.getActiveItems().removeAll(is);
            this.items.removeAll(is);
            //JsfUtil.addInfoFacesMessage(ProjectConstants.OPERATION_SUCCESSFUL);
        } catch (Exception e) {
            e.getMessage();
            JsfUtil.addErrorFacesMessage(ProjectConstants.OPERATION_UNSUCCESSFUL);
        }
    }

    public void processItemSelected(ActionEvent actionEvent) {
        System.out.println("Processing Item Selection :::::::::::::::::::::::::::::::::::::: " + selectedItems);
        if (!selectedItems.isEmpty()) {
            System.out.println("selectedItems count" + selectedItems);
            System.out.println(":::::::::::::::::::::::::::::::::::::::::::::: " + kiosk);
            for (Item i : selectedItems) {
                try {
                    KioskItem kioskItem = new KioskItem();
                    kioskItem.setItem(i);
                    kioskItem.setKiosk(kiosk);
                    kioskItem.setDateAdded(new Date());
                    kioskItem.setStatus(true);
                    persistence.create(kioskItem);
                    JsfUtil.addInfoFacesMessage(i.getName() + "has been added to kiosk.");
                } catch (Exception e) {
                    e.getMessage();
                    JsfUtil.addErrorFacesMessage(ProjectConstants.OPERATION_UNSUCCESSFUL);
                }
            }
            JsfUtil.addInfoFacesMessage(ProjectConstants.UPDATE_SUCCESSFUL);
            populateItemsByBusiness();
        }
        selectedItems = new ArrayList<>();
    }

    public void registerDeliveryOption() {
        try {
            System.out.println("Delivery Collections ::::: " + selectedOptions);
            for (String k : selectedOptions) {
                System.out.println("Delivery iterator ::::: " + k);
                kioskDeliveryOption = new KioskDeliveryOption();
                kioskDeliveryOption.setActive(true);
                kioskDeliveryOption.setDeliveryOption(KioskDeliveryOption.Option.valueOf(k));
                kioskDeliveryOption.setKiosk(this.kiosk);
                kioskDeliveryOption.setCreatedDate(new Date());
                persistence.create(kioskDeliveryOption);
                kioskDeliveryOption = new KioskDeliveryOption();
            }
            getDeliveryOptions();
            JsfUtil.addInfoFacesMessage(ProjectConstants.OPERATION_SUCCESSFUL);
        } catch (Exception e) {
            e.getMessage();
            JsfUtil.addErrorFacesMessage(ProjectConstants.OPERATION_UNSUCCESSFUL);
        }
    }

    public void registerPayment() {
        try {
            System.out.println("Delivery Collections ::::: " + selectedPaymentOptions);
            for (String k : selectedPaymentOptions) {
                System.out.println("Delivery iterator :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: " + k);
                KioskPaymentOption kioskPaymentOption = new KioskPaymentOption();
                kioskPaymentOption.setActive(true);
                kioskPaymentOption.setCreatedDate(new Date());
                kioskPaymentOption.setKiosk(this.kiosk);
                kioskPaymentOption.setPaymentOption(KioskPaymentOption.Option.valueOf(k));
                persistence.create(kioskPaymentOption);
                kioskPaymentOption = new KioskPaymentOption();
            }
            getKioskPaymentOptions();
            JsfUtil.addInfoFacesMessage(ProjectConstants.OPERATION_SUCCESSFUL);
        } catch (Exception e) {
            e.getMessage();
            JsfUtil.addErrorFacesMessage(ProjectConstants.OPERATION_UNSUCCESSFUL);
        }

    }

    public void removeItem() {
        try {
            List<Item> checkedItems = new ArrayList<>();
            for (Item i : this.kioskItems) {
                if (this.selectItems.get(i.getId())) {
                    checkedItems.add(i);
                }
            }
            for (Item si : checkedItems) {
                KioskItem ki = persistence.getKioskItemByKioskAndItem(kiosk.getId(), si.getId());
                if (ki != null) {
                    persistence.delete(ki);
                }
            }
            kioskItems.clear();
            this.selectItems.clear();
            getKioskItems();
            getItems();
            JsfUtil.addInfoFacesMessage(ProjectConstants.ITEM_REMOVED);
        } catch (Exception e) {
            System.out.println("EXception >>>>" + e);
            JsfUtil.addErrorFacesMessage(ProjectConstants.OPERATION_UNSUCCESSFUL);
        }
        this.showSelectItemDT = false;
    }

    public void disableKioskItem(Long id) {
        try {
            KioskItem ki = persistence.getKioskItemByKioskAndItem(kiosk.getId(), id);
            if (ki != null) {
                ki.setStatus(false);
                persistence.update(ki);
            }
        } catch (Exception e) {
            System.out.println("EXception >>>>" + e);
            JsfUtil.addErrorFacesMessage(ProjectConstants.OPERATION_UNSUCCESSFUL);
        }
    }

    public void enableKioskItem(Long id) {
        try {
            KioskItem ki = persistence.getKioskItemByKioskAndItem(kiosk.getId(), id);
            if (ki != null) {
                ki.setStatus(true);
                persistence.update(ki);
            }
        } catch (Exception e) {
            System.out.println("EXception >>>>" + e);
            JsfUtil.addErrorFacesMessage(ProjectConstants.OPERATION_UNSUCCESSFUL);
        }
    }

    public boolean isKioskItemEnabled(Long id) {
        KioskItem ki = persistence.getKioskItemByKioskAndItem(kiosk.getId(), id);
        boolean enabled = ki.isStatus();
        return enabled;
    }

    public void removeSingleItem(Long id) {
        try {
            KioskItem ki = persistence.getKioskItemByKioskAndItem(kiosk.getId(), id);
            if (ki != null) {
                persistence.delete(ki);
            }
            kioskItems.clear();
            getKioskItems();
            getItems();
            JsfUtil.addInfoFacesMessage(ProjectConstants.ITEM_REMOVED);
        } catch (Exception e) {
            System.out.println("EXception >>>>" + e);
            JsfUtil.addErrorFacesMessage(ProjectConstants.OPERATION_UNSUCCESSFUL);
        }
        this.showSelectItemDT = false;
    }

    public void addItem() {
        try {
            List<Item> checkedItems = new ArrayList<>();
            for (Item s : this.items) {
                if (this.availableItems.get(s.getId())) {
                    checkedItems.add(s);
                }
            }

            for (Item si : checkedItems) {
                KioskItem ki = new KioskItem();
                ki.setDateAdded(new Date());
                ki.setItem(si);
                ki.setKiosk(this.kiosk);
                ki.setStatus(true);
                persistence.create(ki);
            }
            kioskItems.clear();
            this.availableItems.clear();
            getKioskItems();
            getItems();
            JsfUtil.addInfoFacesMessage(ProjectConstants.ITEM_ADDED);
        } catch (Exception e) {
            System.out.println("EXception >>>>" + e);
            JsfUtil.addErrorFacesMessage(ProjectConstants.OPERATION_UNSUCCESSFUL);
        }
        showKioskItemDT = false;
    }

    public void addSingleItem(Item si) {
        try {
            if (si != null) {
                KioskItem ki = new KioskItem();
                ki.setDateAdded(new Date());
                ki.setItem(si);
                ki.setKiosk(this.kiosk);
                ki.setStatus(true);
                persistence.create(ki);
            }
            kioskItems.clear();
            getKioskItems();
            getItems();
            JsfUtil.addInfoFacesMessage(ProjectConstants.ITEM_ADDED);
        } catch (Exception e) {
            System.out.println("EXception >>>>" + e);
            JsfUtil.addErrorFacesMessage(ProjectConstants.OPERATION_UNSUCCESSFUL);
        }
        showKioskItemDT = false;
    }

    public void selectedItemTable(AjaxBehaviorEvent e) {
        this.showSelectItemDT = false;
        this.showKioskItemDT = false;
        for (Map.Entry<Long, Boolean> entrySet : selectItems.entrySet()) {
            Long key = entrySet.getKey();

            Boolean value = entrySet.getValue();
            if (value) {
                this.showSelectItemDT = value;
            }
        }

        for (Map.Entry<Long, Boolean> entrySet : availableItems.entrySet()) {
            Long key = entrySet.getKey();

            Boolean value = entrySet.getValue();
            if (value) {
                this.showKioskItemDT = value;
            }
        }
    }

    public void showDetailTab(AjaxBehaviorEvent e) {
        this.showDeliveryDiv = false;
        this.showPaymentDiv = false;
        this.showKioskItemDiv = false;
    }

    public void showDeliveryTab(AjaxBehaviorEvent e) {
        this.showDeliveryDiv = true;
        this.showPaymentDiv = false;
        this.showKioskItemDiv = false;
    }

    public void showPaymentTab(AjaxBehaviorEvent e) {
        this.showDeliveryDiv = false;
        this.showPaymentDiv = true;
        this.showKioskItemDiv = false;
    }

    public void showKioskItemTab(AjaxBehaviorEvent e) {
        this.showDeliveryDiv = false;
        this.showPaymentDiv = false;
        this.showKioskItemDiv = true;
    }
    
    public void showScheduleTime(AjaxBehaviorEvent event) {
        showScheduleDelivery = false;
        if (this.kiosk.isAllowSchedule()) {
            showScheduleDelivery = true;
        }
    }

    public List<KioskDeliveryOption.Option> getDeliveryOption() {
        return Arrays.asList(KioskDeliveryOption.Option.values());
    }

    public List<KioskPaymentOption.Option> getKioskPaymentOption() {
        return Arrays.asList(KioskPaymentOption.Option.values());
    }

    public void viewLog(Long kioskId) {
        System.out.println("::::::::::::::::::::::::::: Kiosk ID............... " + kioskId);
        kioskActivityLog = persistence.getKioskActivityLog(kioskId);
    }

    public String viewList() {
        kiosk = new Kiosk();
        return "ownerManageKiosk";
    }

    public String registerNewKiosk() {
        kiosk = null;
        return "ownerRegisterKiosk";
    }

    public Kiosk getKiosk() {
        return kiosk;
    }

    public void setKiosk(Kiosk kiosk) {
        this.kiosk = kiosk;
    }

    public Business getBusiness() {
        return business;
    }

    public void setBusiness(Business business) {
        this.business = business;
    }

    public KioskUser getUser() {
        return user;
    }

    public void setUser(KioskUser user) {
        this.user = user;
    }

    public String getBquery() {
        return bquery;
    }

    public void setBquery(String bquery) {
        this.bquery = bquery;
    }

    public String getKquery() {
        return kquery;
    }

    public void setKquery(String kquery) {
        this.kquery = kquery;
    }

    public List<Kiosk> getActiveKiosk() {
        populateActiveKiosks();
        return activeKiosk;
    }

    public void setActiveKiosk(List<Kiosk> activeKiosk) {
        this.activeKiosk = activeKiosk;
    }

    public List<Kiosk> getDisabledKiosk() {
        populateDisabledKiosk();
        return disabledKiosk;
    }

    public void setDisabledKiosk(List<Kiosk> disabledKiosk) {
        this.disabledKiosk = disabledKiosk;
    }

    public List<Item> getSelectedItems() {
        return selectedItems;
    }

    public void setSelectedItems(List<Item> selectedItems) {
        this.selectedItems = selectedItems;
    }

    public List<Item> getItems() {
        populateItemsByBusiness();
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public List<Item> getKioskItems() {
        kioskItems = persistence.getItemFromKioskItemByKioskId(this.kiosk.getId());
        return kioskItems;
    }

    public void setKioskItems(List<Item> kioskItems) {
        this.kioskItems = kioskItems;
    }

    public KioskDeliveryOption getKioskDeliveryOption() {
        return kioskDeliveryOption;
    }

    public void setKioskDeliveryOption(KioskDeliveryOption kioskDeliveryOption) {
        this.kioskDeliveryOption = kioskDeliveryOption;
    }

    public List<KioskDeliveryOption> getDeliveryOptions() {
        deliveryOptions = persistence.getKioskDeliveryOptionsByKioskId(this.kiosk.getId());
        return deliveryOptions;
    }

    public void setDeliveryOptions(List<KioskDeliveryOption> deliveryOptions) {
        this.deliveryOptions = deliveryOptions;
    }

    public List<String> getSelectedOptions() {
        return selectedOptions;
    }

    public void setSelectedOptions(List<String> selectedOptions) {
        this.selectedOptions = selectedOptions;
    }

    public List<String> getSelectedPaymentOptions() {
        return selectedPaymentOptions;
    }

    public void setSelectedPaymentOptions(List<String> selectedPaymentOptions) {
        this.selectedPaymentOptions = selectedPaymentOptions;
    }

    public List<KioskPaymentOption> getKioskPaymentOptions() {
        kioskPaymentOptions = persistence.getKioskPaymentOptionByKioskId(this.kiosk.getId());
        return kioskPaymentOptions;
    }

    public void setKioskPaymentOptions(List<KioskPaymentOption> kioskPaymentOptions) {
        this.kioskPaymentOptions = kioskPaymentOptions;
    }

    private String result;

    public void submit(KioskDeliveryOption kdo) {
        this.kioskDeliveryOption = kdo;
        System.out.println("submit ::::: " + kioskDeliveryOption);
    }

    public void changeListener() {
        System.out.println("Value Changer");
        result = "called by changeListener";
    }

    public void listener(AjaxBehaviorEvent event) {
        System.out.println("listener");
        result = "called by " + event.getComponent().getClass().getName();
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getMessageChecker() {
        return messageChecker;
    }

    public void setMessageChecker(String messageChecker) {
        this.messageChecker = messageChecker;
    }

    public List<KioskActivityLog> getKioskActivityLog() {
        return kioskActivityLog;
    }

    public void setKioskActivityLog(List<KioskActivityLog> kioskActivityLog) {
        this.kioskActivityLog = kioskActivityLog;
    }

    public List<Kiosk> getAllKiosks() {
        return allKiosks;
    }

    public void setAllKiosks(List<Kiosk> allKiosks) {
        this.allKiosks = allKiosks;
    }

    public PortalPersistence getPersistence() {
        return persistence;
    }

    public void setPersistence(PortalPersistence persistence) {
        this.persistence = persistence;
    }

    public ItemController getItemController() {
        return itemController;
    }

    public void setItemController(ItemController itemController) {
        this.itemController = itemController;
    }

    public Kiosk.KioskType getKioskType() {
        return kioskType;
    }

    public void setKioskType(Kiosk.KioskType kioskType) {
        this.kioskType = kioskType;
    }

    public Map<Long, Boolean> getAvailableItems() {
        return availableItems;
    }

    public void setAvailableItems(Map<Long, Boolean> availableItems) {
        this.availableItems = availableItems;
    }

    public Map<Long, Boolean> getSelectItems() {
        return selectItems;
    }

    public void setSelectItems(Map<Long, Boolean> selectItems) {
        this.selectItems = selectItems;
    }

    public boolean isShowSelectItemDT() {
        return showSelectItemDT;
    }

    public void setShowSelectItemDT(boolean showSelectItemDT) {
        this.showSelectItemDT = showSelectItemDT;
    }

    public boolean isShowKioskItemDT() {
        return showKioskItemDT;
    }

    public void setShowKioskItemDT(boolean showKioskItemDT) {
        this.showKioskItemDT = showKioskItemDT;
    }

    public boolean isShowDeliveryDiv() {
        return showDeliveryDiv;
    }

    public void setShowDeliveryDiv(boolean showDeliveryDiv) {
        this.showDeliveryDiv = showDeliveryDiv;
    }

    public boolean isShowPaymentDiv() {
        return showPaymentDiv;
    }

    public void setShowPaymentDiv(boolean showPaymentDiv) {
        this.showPaymentDiv = showPaymentDiv;
    }

    public boolean isShowKioskItemDiv() {
        return showKioskItemDiv;
    }

    public void setShowKioskItemDiv(boolean showKioskItemDiv) {
        this.showKioskItemDiv = showKioskItemDiv;
    }

    public boolean isShowDistanceCostDiv() {
        return showDistanceCostDiv;
    }

    public void setShowDistanceCostDiv(boolean showDistanceCostDiv) {
        this.showDistanceCostDiv = showDistanceCostDiv;
    }

    public List<DistanceCost> getDistanceCosts() {
        return distanceCosts;
    }

    public void setDistanceCosts(List<DistanceCost> distanceCosts) {
        this.distanceCosts = distanceCosts;
    }

    public DistanceCost getDistanceCost() {
        return distanceCost;
    }

    public void setDistanceCost(DistanceCost distanceCost) {
        this.distanceCost = distanceCost;
    }

    public Double getDistCost() {
        return distCost;
    }

    public void setDistCost(Double distCost) {
        this.distCost = distCost;
    }

    public Double getDist() {
        return dist;
    }

    public void setDist(Double dist) {
        this.dist = dist;
    }

    public boolean isShowScheduleDelivery() {
        return showScheduleDelivery;
    }

    public void setShowScheduleDelivery(boolean showScheduleDelivery) {
        this.showScheduleDelivery = showScheduleDelivery;
    }
    
    

}
