/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.morph.kiosk.controller;

import com.morph.kiosk.persistence.service.PortalPersistence;
import com.morph.kiosk.persistence.service.UserPersistence;
import java.io.Serializable;
import javax.annotation.Resource;
import javax.inject.Named;
import javax.faces.view.ViewScoped;

/**
 *
 * @author Tobi-Morph-PC
 */
@Named(value = "viewCompController")
@ViewScoped
public class ViewCompController implements Serializable{
    
@Resource(lookup = "java:global/KioskEJBModule-1.2/UserPersistence!com.morph.kiosk.persistence.service.UserPersistence")
    private UserPersistence persistence;

    @Resource(lookup = "java:global/KioskEJBModule-1.2/PortalPersistence!com.morph.kiosk.persistence.service.PortalPersistence")
    private PortalPersistence portalPersistence;
    
    Long pendingRequestCount;
    
    public ViewCompController() {
    }
    
    public void init(){
        
    }
}
