/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.morph.kiosk.controller;

import com.morph.kiosk.persistence.entity.Business;
import com.morph.kiosk.persistence.entity.Kiosk;
import com.morph.kiosk.persistence.entity.KioskUser;
import com.morph.kiosk.persistence.service.PortalPersistence;
import com.morph.kiosk.persistence.service.UserPersistence;
import com.morph.kiosk.util.JsfUtil;
import com.morph.util.ProjectConstants;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import javax.annotation.Resource;

/**
 *
 * @author Tobi-Morph-PC
 */
@Named(value = "portalBusinessController")
@SessionScoped
public class PortalBusinessController extends BaseController implements Serializable {

    @Resource(lookup = "java:global/KioskEJBModule-1.2/UserPersistence!com.morph.kiosk.persistence.service.UserPersistence")
    private UserPersistence userPersistence;

    @Resource(lookup = "java:global/KioskEJBModule-1.2/PortalPersistence!com.morph.kiosk.persistence.service.PortalPersistence")
    private PortalPersistence portalPersistence;
    
    private KioskUser user;
    private Business business = new Business();
    
    
    public PortalBusinessController() {
    }

    @Override
    void initUser(KioskUser user) {
        this.user = user;
    }

    public String viewDetail(Business business){
        this.business = business;
        return "sysBusinessDetail";
    }
    
    public void reset(){
        this.business = new Business();
    }
    
    public String emptyBusinessListRedirect() {
        if (null == this.business) {
            return "sysBusinesses";
        } else {
            return null;
        }
    }
    public void disableBusiness() {
        try {
            this.business.setModifiedDate(new Date());
            this.business.setStatus(false);
            portalPersistence.update(this.business);
            JsfUtil.addInfoFacesMessage(ProjectConstants.BUSINESS_DISABLED);
        } catch (Exception e) {
            JsfUtil.addErrorFacesMessage(ProjectConstants.UPDATE_UNSUCCESSFUL);
            e.getLocalizedMessage();
        }
    }

    public void enableBusiness() {
        try {
            this.business.setModifiedDate(new Date());
            this.business.setStatus(true);
            portalPersistence.update(this.business);
            JsfUtil.addInfoFacesMessage(ProjectConstants.BUSINESS_ENABLED);
        } catch (Exception e) {
            JsfUtil.addErrorFacesMessage(ProjectConstants.UPDATE_UNSUCCESSFUL);
            e.getLocalizedMessage();
        }
    }
    
    public void updateBusiness() {
        try {
            this.business.setModifiedDate(new Date());
            portalPersistence.update(this.business);
            JsfUtil.addInfoFacesMessage(ProjectConstants.UPDATE_SUCCESSFUL);
        } catch (Exception e) {
            JsfUtil.addErrorFacesMessage(ProjectConstants.UPDATE_UNSUCCESSFUL);
            e.getLocalizedMessage();
        }
    }


    public List<Business> getActiveBusinesses() {
        return portalPersistence.geAllActiveBusiness();
    }

    public List<Business> getDisabledBusinesses() {
        return portalPersistence.geAllDeacctiveBusiness();
    }
    
    public List<Kiosk> getBusinessActiveKiosks(){
        if(null != this.business){
            final List<Kiosk> l = portalPersistence.getActiveKioskByBusiness(this.business.getId());
            return l;
        }else{
            return Collections.EMPTY_LIST;
        }
    }
    
    
    public List<Kiosk> getBusinessDeativatedKiosks(){
        return portalPersistence.getDisabledKioskByBusiness(this.business.getId());
    }
    
    public KioskUser getUser() {
        return user;
    }

    public void setUser(KioskUser user) {
        this.user = user;
    }

    public Business getBusiness() {
        return business;
    }

    public void setBusiness(Business business) {
        this.business = business;
    }
    
}
