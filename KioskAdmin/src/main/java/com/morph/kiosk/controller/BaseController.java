/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.morph.kiosk.controller;

import com.morph.kiosk.persistence.entity.KioskUser;
import com.morph.kiosk.persistence.service.UserPersistence;
import com.morph.util.ProjectConstants;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import org.omnifaces.util.Faces;

/**
 *
 * @author Tobi-Morph-PC
 */
public abstract class BaseController implements Serializable{
    
    @Resource(lookup = "java:global/KioskEJBModule-1.2/UserPersistence!com.morph.kiosk.persistence.service.UserPersistence")
    private UserPersistence persistence;
    
    @PostConstruct
    public void init(){
        KioskUser user = (KioskUser)Faces.getSessionAttribute(ProjectConstants.SESSION_KIOSK_USER);
        //user = persistence.find(KioskUser.class, Long.valueOf("4"));
        initUser(user);
        //System.out.println("Kiosk User :::: " + Faces.getSessionAttribute(ProjectConstants.SESSION_KIOSK_USER));
    }
    
    abstract void initUser(KioskUser user);
}
