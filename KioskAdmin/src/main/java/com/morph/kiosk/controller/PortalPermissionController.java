/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.morph.kiosk.controller;

import com.morph.kiosk.persistence.entity.KioskUser;
import com.morph.kiosk.persistence.entity.portal.PortalAccessGroup;
import com.morph.kiosk.persistence.entity.portal.PortalAccessGroup_Permission;
import com.morph.kiosk.persistence.entity.portal.PortalPermission;
import com.morph.kiosk.persistence.entity.portal.PortalUserGroup;
import com.morph.kiosk.persistence.entity.portal.PortalUserGroup_AccessGroup;
import com.morph.kiosk.persistence.entity.portal.PortalUserGroup_User;
import com.morph.kiosk.persistence.service.PortalPersistence;
import com.morph.kiosk.persistence.service.UserPersistence;
import com.morph.kiosk.util.JsfUtil;
import com.morph.util.ProjectConstants;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import javax.annotation.Resource;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.event.ActionEvent;

/**
 *
 * @author Tobi-Morph-PC
 */
@Named("portalPermissionController")
@SessionScoped
public class PortalPermissionController extends BaseController {

    @Resource(lookup = "java:global/KioskEJBModule-1.2/UserPersistence!com.morph.kiosk.persistence.service.UserPersistence")
    private UserPersistence userPersistence;

    @Resource(lookup = "java:global/KioskEJBModule-1.2/PortalPersistence!com.morph.kiosk.persistence.service.PortalPersistence")
    private PortalPersistence portalPersistence;

    private KioskUser user = new KioskUser();

    private List<PortalUserGroup> userGroups = new ArrayList<>();
    private PortalUserGroup userGroup = new PortalUserGroup();

    private PortalAccessGroup accessGroup = new PortalAccessGroup();
    private List<PortalAccessGroup> accessGroups = new ArrayList<>();

    private List<PortalUserGroup_AccessGroup> userGroup_AccessGroups = new ArrayList<>();
    private List<PortalUserGroup_AccessGroup> accessGroup_UserGroups = new ArrayList<>();

    private List<PortalUserGroup> selectedUserGroups = new ArrayList<>();

    private List<PortalPermission> selectedPortalPermissions = new ArrayList<>();
    private List<PortalPermission> selectedAccessGroupPermissions = new ArrayList<>();

    private List<PortalPermission> portalPermissions = new ArrayList<>();
    private List<PortalPermission> accessGroupPermissions = new ArrayList<>();

    private List<PortalUserGroup_User> userGroup_Users = new ArrayList<>();

    @Override
    void initUser(KioskUser user) {
        this.user = user;
    }

    public PortalPermissionController() {
    }

    public void registerUserGroup() {
        try {
            if (null == portalPersistence.getPortalUserGroupByUserGroupName(this.userGroup.getUserGroupName())) {
                this.userGroup.setActive(true);
                portalPersistence.create(this.userGroup);
                JsfUtil.addInfoFacesMessage(ProjectConstants.OPERATION_SUCCESSFUL);
            } else {
                JsfUtil.addErrorFacesMessage(ProjectConstants.DUPLICATE_ENTRY);
            }
        } catch (Exception e) {
            JsfUtil.addErrorFacesMessage(ProjectConstants.GENERIC_ERROR);
            System.err.println("Error :::: " + e.getLocalizedMessage());
        }
    }

    public void updateUserGroup() {
        userPersistence.update(this.userGroup);
        JsfUtil.addInfoFacesMessage(ProjectConstants.OPERATION_SUCCESSFUL);

    }

    public String viewUserGroupDetail(PortalUserGroup userGroup) {
        this.userGroup = userGroup;
        return "sysUserGroupDetail";
    }
    
    public void viewAccessGroupPermission(PortalAccessGroup accessGroup){
        this.accessGroup = accessGroup;
        System.out.println("viewAccessGroupPermission ::::::: " + this.accessGroup);
    }

//    public void emptyDetailPageRedirect() throws IOException {
//        if (null == this.accessGroup || null == this.accessGroup.getId()) {
//            FacesContext context = FacesContext.getCurrentInstance();
//            if (!context.isPostback() && context.isValidationFailed()) {
//                context.getExternalContext().redirect("accessGroups.xhtml");
//            }
//        } else {
//            System.out.println("emptyDetailPageRedirect Access Group ::::: " + accessGroup);
//        }
//    }
    public String emptyDetailPageRedirect() throws IOException {
        if (null == this.accessGroup || null == this.accessGroup.getId()) {
            return "sysAccessGroups";
        } else {
            System.out.println("emptyDetailPageRedirect Access Group ::::: " + accessGroup);
            return "";
        }
    }

    public String emptyUserGroupDetailPageRedirect() throws IOException {
        if (null == this.userGroup || null == this.userGroup.getId()) {
            return "sysUserGroups";
        } else {
            System.out.println("v Access Group ::::: " + userGroup);
            return "";
        }
    }

    public void registerAccessGroup() {
        try {
            if (null == portalPersistence.getPortalAccessGroupByName(this.accessGroup.getName())) {
                this.accessGroup.setActive(true);
                portalPersistence.create(this.accessGroup);
                JsfUtil.addInfoFacesMessage(ProjectConstants.OPERATION_SUCCESSFUL);
            } else {
                JsfUtil.addErrorFacesMessage(ProjectConstants.DUPLICATE_ENTRY);
            }
        } catch (Exception e) {
            JsfUtil.addErrorFacesMessage(ProjectConstants.GENERIC_ERROR);
            System.err.println("Error :::: " + e.getLocalizedMessage());
        }
    }

    public void updateAccessGroup() {
        userPersistence.update(this.accessGroup);
        JsfUtil.addInfoFacesMessage(ProjectConstants.OPERATION_SUCCESSFUL);

    }

    public String viewAccessGroupDetail(PortalAccessGroup accessGroup) {
        this.accessGroup = accessGroup;
        return "sysAccessGroupDetail";
    }

    public String viewUserGroupDetail(PortalAccessGroup accessGroup) {
        this.accessGroup = accessGroup;
        return "sysAccessGroupDetail";
    }

    public void reset() {
        this.userGroup = new PortalUserGroup();
        this.accessGroup = new PortalAccessGroup();
    }

    public void removeUserGroupFromAccessGroup(PortalUserGroup_AccessGroup pug) {
        try {
            userPersistence.delete(pug);
            JsfUtil.addInfoFacesMessage(ProjectConstants.OPERATION_SUCCESSFUL);
        } catch (Exception e) {
            e.getLocalizedMessage();
            JsfUtil.addErrorFacesMessage(ProjectConstants.OPERATION_UNSUCCESSFUL);
        }
        getUserGroup_AccessGroups();
        getAccessGroup_UserGroups();
    }

    public void assignSelectedUserGroups(ActionEvent actionEvent) {

        //add the selected user group to usergroup_accessgroup
        //but first check if the record already exist
        System.out.println("Selected User Group :::::     ::::: " + selectedUserGroups);

        selectedUserGroups.forEach(i -> {
            PortalUserGroup_AccessGroup pugag = new PortalUserGroup_AccessGroup();
            pugag.setUserGroup(i);
            pugag.setAccessGroup(this.accessGroup);
            System.out.println("::::::::::: " + pugag);
            userPersistence.create(pugag);
        });

        selectedUserGroups.clear();

        getAccessGroup_UserGroups();
        getUserGroup_AccessGroups();

    }

    public List<PortalUserGroup> getUserGroups() {
        userGroups = portalPersistence.getAllUserGroups();
        return userGroups;
    }

    public void setUserGroups(List<PortalUserGroup> userGroups) {
        this.userGroups = userGroups;
    }

    public PortalUserGroup getUserGroup() {
        return userGroup;
    }

    public void setUserGroup(PortalUserGroup userGroup) {
        this.userGroup = userGroup;
    }

    public PortalAccessGroup getAccessGroup() {
        return accessGroup;
    }

    public void setAccessGroup(PortalAccessGroup accessGroup) {
        this.accessGroup = accessGroup;
    }

    public List<PortalAccessGroup> getAccessGroups() {
        accessGroups = portalPersistence.getAllAccessGroups();
        return accessGroups;
    }

    public void setAccessGroups(List<PortalAccessGroup> accessGroups) {
        this.accessGroups = accessGroups;
    }

    public KioskUser getUser() {
        return user;
    }

    public void setUser(KioskUser user) {
        this.user = user;
    }

    public List<PortalUserGroup_AccessGroup> getUserGroup_AccessGroups() {
        if (null != this.userGroup) {
            userGroup_AccessGroups.clear();
            userGroup_AccessGroups = userPersistence.getUserGroupAccessGroupByUserGroup(this.userGroup.getId());
        } else {
            userGroup_AccessGroups = Collections.EMPTY_LIST;
        }
        return userGroup_AccessGroups;
    }

    public void setUserGroup_AccessGroups(List<PortalUserGroup_AccessGroup> userGroup_AccessGroups) {
        this.userGroup_AccessGroups = userGroup_AccessGroups;
    }

    public List<PortalUserGroup_AccessGroup> getAccessGroup_UserGroups() {
        if (null != this.accessGroup) {
            accessGroup_UserGroups.clear();
            accessGroup_UserGroups = userPersistence.getUserGroupAccessGroupByAccessGroup(this.accessGroup.getId());
        } else {
            accessGroup_UserGroups = Collections.EMPTY_LIST;
        }
        return accessGroup_UserGroups;
    }

    public void setAccessGroup_UserGroups(List<PortalUserGroup_AccessGroup> accessGroup_UserGroups) {
        this.accessGroup_UserGroups = accessGroup_UserGroups;
    }

    public List<PortalUserGroup> availableUserGroups() {
        List<PortalUserGroup> l = getUserGroups();
        getAccessGroup_UserGroups().forEach((accessGroup_UserGroup) -> {
            l.remove(accessGroup_UserGroup.getUserGroup());
        });
        return l;
    }

    public List<PortalUserGroup> getSelectedUserGroups() {
        return selectedUserGroups;
    }

    public void setSelectedUserGroups(List<PortalUserGroup> selectedUserGroups) {
        this.selectedUserGroups = selectedUserGroups;
    }

    public void addSelectedPermissionToAccessGroup() {
        System.out.println("Selected Permissions :::: " + selectedPortalPermissions);
        selectedPortalPermissions.stream().forEach((PortalPermission _item) -> {
            PortalAccessGroup_Permission pagp = new PortalAccessGroup_Permission();
            pagp.setAccessGroup(PortalPermissionController.this.accessGroup);
            pagp.setPermission(_item);
            pagp.setDateCreated(new Date());
            portalPersistence.create(pagp);
        });
        getAccessGroupPermissions();
        getPortalPermissions();
    }

    public void removeSelectedPermissionFromAccessGroup() {

    }

    public List<PortalPermission> getSelectedPortalPermissions() {
        return selectedPortalPermissions;
    }

    public void setSelectedPortalPermissions(List<PortalPermission> selectedPortalPermissions) {
        this.selectedPortalPermissions = selectedPortalPermissions;
    }

    public List<PortalPermission> getSelectedAccessGroupPermissions() {
        return selectedAccessGroupPermissions;
    }

    public void setSelectedAccessGroupPermissions(List<PortalPermission> selectedAccessGroupPermissions) {
        this.selectedAccessGroupPermissions = selectedAccessGroupPermissions;
    }

    public List<PortalPermission> getPortalPermissions() {
        portalPermissions = portalPersistence.getPortalAccessGroupPermissions()
                .stream().filter(i -> !getAccessGroupPermissions().contains(i.getPermission()))
                .map((PortalAccessGroup_Permission i) -> i.getPermission()).collect(Collectors.toList());
        return portalPermissions;
    }

    public void setPortalPermissions(List<PortalPermission> portalPermissions) {
        this.portalPermissions = portalPermissions;
    }

    public List<PortalPermission> getAccessGroupPermissions() {
        //System.out.println("getAccessGroupPermissions  :::: " + this.accessGroup);
        accessGroupPermissions = portalPersistence.getPortalAccessGroupPermissionByAccessGroup(this.accessGroup.getId())
                .stream().map(c -> c.getPermission()).collect(Collectors.toList());
        return accessGroupPermissions;
    }

    public void setAccessGroupPermissions(List<PortalPermission> accessGroupPermissions) {
        this.accessGroupPermissions = accessGroupPermissions;
    }

    public List<PortalUserGroup_User> getUserGroup_Users() {
        if (!userGroup_Users.isEmpty()) {
            userGroup_Users.clear();
        }
        userGroup_Users = userPersistence.getPortalUserGroupByUserGroup(this.userGroup.getId());
        return userGroup_Users;
    }

    public void setUserGroup_Users(List<PortalUserGroup_User> userGroup_Users) {
        this.userGroup_Users = userGroup_Users;
    }

}
