/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.morph.kiosk.controller;

import com.morph.kiosk.persistence.entity.Bank;
import com.morph.kiosk.persistence.entity.Business;
import com.morph.kiosk.persistence.entity.BusinessAccount;
import com.morph.kiosk.persistence.entity.KioskUser;
import com.morph.kiosk.persistence.entity.order.Cart;
import com.morph.kiosk.persistence.entity.order.KioskPaymentOption;
import com.morph.kiosk.persistence.entity.order.OrderedItem;
import com.morph.kiosk.persistence.entity.order.PaymentTransaction;
import com.morph.kiosk.persistence.entity.order.PaymentTransaction.PaymentTractionStatus;
import com.morph.kiosk.persistence.entity.order.SubItemCart;
import com.morph.kiosk.persistence.entity.payment.PaymentProvider;
import com.morph.kiosk.persistence.entity.payment.Paystack;
import com.morph.kiosk.persistence.entity.payment.Refund;
import com.morph.kiosk.persistence.entity.payment.Settlement;
import com.morph.kiosk.persistence.entity.payment.Split;
import com.morph.kiosk.persistence.entity.payment.SplitLog;
import com.morph.kiosk.persistence.service.PortalPersistence;
import com.morph.kiosk.persistence.service.UserPersistence;
import com.morph.kiosk.util.JsfUtil;
import com.morph.paystack.CreateTransferRecepientRequest;
import com.morph.paystack.CreateTransferRecepientResponse;
import com.morph.paystack.InitiateTransferRequest;
import com.morph.util.ProjectConstants;
import java.lang.reflect.Field;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import org.omnifaces.util.Faces;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.WebResource;
import java.util.HashMap;
import javax.faces.event.AjaxBehaviorEvent;

/**
 *
 * @author Tobi-Morph-PC
 */
@Named(value = "portalPaymentController")
@ViewScoped
public class PortalPaymentController extends BaseController {

    @Resource(lookup = "java:global/KioskEJBModule-1.2/UserPersistence!com.morph.kiosk.persistence.service.UserPersistence")
    private UserPersistence userPersistence;

    @Resource(lookup = "java:global/KioskEJBModule-1.2/PortalPersistence!com.morph.kiosk.persistence.service.PortalPersistence")
    private PortalPersistence portalPersistence;

    @Inject
    private PortalUserController userController;

    private KioskUser user;
    private Business business;
    private BusinessAccount businessAccount = new BusinessAccount();
    private PaymentProvider paymentProvider = new PaymentProvider();
    private Paystack paystack = new Paystack();
    private Split split;
    private Bank bank;
    private PaymentTransaction paymentTransaction;
    private PaymentProviders providers;
    private Settlement settlement;
    private Refund refund = new Refund();
    private boolean isPaystack = false;
    private LazyDataModel<PaymentTransaction> lazyDataModelPaymentTransaction;

    private List<PaymentProvider> paymentProviders = new ArrayList<>();
    private List<Split> splits = new ArrayList<>();
    private Long[] selectedSplits;
    private List<PortalPaymentController.PaymentProviders> pps = new ArrayList<>();
    private String iquery;
    private String paid;
    private List<PaymentTransaction> paymentTransactions = new ArrayList<>();
    private List<PaymentTransaction> onlineTranx = new ArrayList<>();
    private List<PaymentTransaction> offlineTranx = new ArrayList<>();
    private List<PaymentTransaction> selectedOnlineTranx = new ArrayList<>();
    private List<PaymentTransaction> selectedOfflineTranx = new ArrayList<>();
    private List<Cart> carts = new ArrayList<>();
    private List<SubItemCart> subItemCarts = new ArrayList<>();
    private List<Refund> refunds = new ArrayList<>();
    private List<Refund> refundHistory = new ArrayList<>();
    private List<Settlement> settlements = new ArrayList<>();
    private List<Settlement> selectedSettlements = new ArrayList<>();
    private List<SplitLog> splitLogs = new ArrayList<>();
    private List<BusinessAccount> businessAccounts = new ArrayList<>();
    private List<Bank> banks = new ArrayList<>();
    private Date tranxDate1;
    private Date tranxDate2;
    private OrderedItem orderedItem;

    private Map<Long, Boolean> selectedTranx = new HashMap<>();
    private Map<Long, Boolean> selectedSettlement = new HashMap<>();
    private boolean enableSettleButton = false;
    private boolean enablePayButton = false;
    private boolean enableApplySettlementButton = false;

    public PortalPaymentController() {
    }

    @Override
    void initUser(KioskUser user) {
        this.user = user;
    }

    @PostConstruct
    public void initialize() {

        lazyDataModelPaymentTransaction = new LazyDataModel<PaymentTransaction>() {
            private static final long serialVersionUID = -6074599821435569629L;
            List<PaymentTransaction> paymentTransactions;
            //Long rc = portalPersistence.getPortalPaymentTransactionsCount(null);

//            @Override
//            public List<PaymentTransaction> load(int first, int pageSize, List<SortMeta> multiSortMeta, Map<String, Object> filters) {
//                paymentTransactions = portalPersistence.getPortalPaymentTransactions(null, first, pageSize);
//                List<PaymentTransaction> list = new ArrayList<>();
//                
//                setRowCount(portalPersistence.getPortalPaymentTransactionsCount(null).intValue());
//                
//                return paymentTransactions;
//            }
//            
            @Override
            public List<PaymentTransaction> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
                //return load(first, pageSize, null, filters);
                paymentTransactions = portalPersistence.getPortalPaymentTransactions(null, first, pageSize);
                setPageSize(pageSize);
                setRowIndex(pageSize - 1);
                setRowCount(portalPersistence.getPortalPaymentTransactionsCount(null).intValue());

                //portalPersistence.findWithNamedQuery(sortField, filters, first)
                if (sortField != null) {
                    Collections.sort(paymentTransactions, new LazySorter(sortField, sortOrder));
                }

                if (filters != null) {
                    paymentTransactions = portalPersistence.getLazyPaymentTransactions(null, first, pageSize, filters);
                }

//                for (PaymentTransaction paymentTransaction : paymentTransactions) {
//                    if (filters != null) {
//                        for (Iterator<String> iterator = filters.keySet().iterator(); iterator.hasNext();) {
//                            try {
//                                String filterProperty = iterator.next();
//                                System.out.println("filterPropertyValue ::: " + filterProperty);
//                                Object filterValue = filters.get(filterProperty);
//
//                                System.out.println("filterValue ::::" + filterValue);
//
//                                Class c = paymentTransaction.getClass();
//
////                                filterProperty = filterProperty.contains(".") ? filterProperty.split(".")[filterProperty.split(".").length - 1]
////                                        : filterProperty;
//                                String[] s = filterProperty.split("\\.");
////                                System.out.println("Array s :::: " + s);
////                                for (String string : s) {
////                                    System.out.println("SSSSSS ::: " + string);
////                                }
//                                   String[] s = filterProperty.split("\\.");
////                                String r = (s.length != 0) ? s[s.length - 1] : filterProperty;
////                                
////                                
//                                Field f = c.getDeclaredField(s[s.length - 2]);
//                                f.setAccessible(true);
//
//                                Class<?> fc = f.getType();
//                                
//                                Field nested = fc.getDeclaredField(r);
//                                nested.setAccessible(true);
//                                //Object j = nested.get(String.class).toString();
//                                Class<?> cl = nested.getDeclaringClass();
//                                System.out.println(cl);
//                                
//                                Object value1 = (Object)nested.get(OrderedItem.class.newInstance());
//                                System.out.println("Value ::: "  + value1);
//                                
//                                //System.out.println("Nested J :::: " + String.valueOf(nested.get(fc.newInstance())));
////                                Field[] fields = paymentTransaction.getClass().getDeclaredFields();
////                                for (Field field : fields) {
////                                    //System.out.println("Field ::: " + field);
////                                    //System.out.println(field.getDeclaringClass().getDeclaredField(filterProperty));  
////                                    field.setAccessible(true);
////                                    System.out.println("Type ::: " + field.getGenericType());
////                                    
////                                    System.out.println(String.valueOf(field.getDeclaringClass().getField(filterProperty).get(field.getType())));  
////                                }
////                                Field field = paymentTransaction.getClass().getField(r);
////                                field.setAccessible(true);
////                                System.out.println(field);
//
//                            } catch (Exception e) {
//                                System.err.println("Error :::: " + e);
//                            }
//                        }
//                    }
//                }
                return paymentTransactions;
            }

            @Override
            public PaymentTransaction getRowData(String rowKey) {
                Long id = Long.valueOf(rowKey);
                for (PaymentTransaction b : paymentTransactions) {
                    if (id.equals(b.getId())) {
                        return b;
                    }
                }
                return null;
            }

            @Override
            public Object getRowKey(PaymentTransaction object) {
                return object.getId();
            }
        };
    }

    public void loadPaymentProvider() {
        this.paymentProviders = portalPersistence.getAllPaymentProviders();
    }

    public void loadSplits() {
        this.splits.clear();
        List<Split> ses = portalPersistence.getAllSplits();
        for (Split se : ses) {
            if (se.isStatus()) {
                this.splits.add(se);
            }
        }
    }

    public void filterSettlement() {
        settlements.clear();
        if (business == null) {
            this.settlements = portalPersistence.getAllSettlements();
        } else {
            this.settlements = portalPersistence.getAllSettlementsByBusiness(business);
        }

        if (paid != null) {
            List<Settlement> filtered = new ArrayList<>();
            boolean paidBoolean = Boolean.valueOf(paid);
            for (Settlement s : this.settlements) {
                if (!s.getPaid().equals(paidBoolean)) {
                    filtered.add(s);
                }
            }
            this.settlements.removeAll(filtered);
        }

        if (this.tranxDate1 != null && this.tranxDate2 != null) {
            List<Settlement> filtered = new ArrayList<>();
            Long date1 = this.tranxDate1.getTime();
            Long date2 = this.tranxDate2.getTime();
            for (Settlement s : this.settlements) {
                if (s.getSettledDate() != null) {
                    Long sDate = dateConverter(s.getSettledDate().toString()).getTime();
                    if (sDate >= date1 && sDate <= date2) {
                        filtered.add(s);
                    }
                }
            }
            this.settlements.clear();
            this.settlements.addAll(filtered);
        }

    }

    public void loadActiveBankAccounts() {
        businessAccounts.clear();
        if (null == iquery || "".equalsIgnoreCase(iquery)) {
            JsfUtil.addErrorFacesMessage(ProjectConstants.PAGE_NOT_FOUND);
            throw new IllegalArgumentException(ProjectConstants.PAGE_NOT_FOUND);
        } else {
            this.business = portalPersistence.find(Business.class, Long.valueOf(iquery));

            if (this.business == null) {
                JsfUtil.addErrorFacesMessage(ProjectConstants.PAGE_NOT_FOUND);
                return;
            }
            // this.businessAccounts = portalPersistence.getActiveBusinessAccountsByBusiness(business);
            this.businessAccount = portalPersistence.getActiveBusinessAccountsByBusiness(business).get(0);

        }
    }

    public void loadBankAccounts() {
        businessAccounts.clear();
        if (null == iquery || "".equalsIgnoreCase(iquery)) {
            JsfUtil.addErrorFacesMessage(ProjectConstants.PAGE_NOT_FOUND);
            throw new IllegalArgumentException(ProjectConstants.PAGE_NOT_FOUND);
        } else {
            this.business = portalPersistence.find(Business.class, Long.valueOf(iquery));

            if (this.business == null) {
                JsfUtil.addErrorFacesMessage(ProjectConstants.PAGE_NOT_FOUND);
                return;
            }
            this.businessAccounts = portalPersistence.getBusinessAccountsByBusiness(business);

        }
    }

    public void loadBanks() {
        this.banks = portalPersistence.findWithQuery("SELECT o FROM Bank o");
    }

    public void loadRefundHistory() {
        refundHistory.clear();
        if (null == iquery || "".equalsIgnoreCase(iquery)) {
            JsfUtil.addErrorFacesMessage(ProjectConstants.PAGE_NOT_FOUND);
            throw new IllegalArgumentException(ProjectConstants.PAGE_NOT_FOUND);
        } else {
            this.business = portalPersistence.find(Business.class, Long.valueOf(iquery));

            if (this.business == null) {
                JsfUtil.addErrorFacesMessage(ProjectConstants.PAGE_NOT_FOUND);
                return;
            }

            List<Refund> rs = portalPersistence.getRefundsByBusiness(business);
            if (this.tranxDate1 != null && this.tranxDate2 != null) {
                Long date1 = this.tranxDate1.getTime();
                Long date2 = this.tranxDate2.getTime();
                for (Refund r : rs) {
                    if (r.getRefundedDate() != null) {
                        Long rDate = dateConverter(r.getRefundedDate().toString()).getTime();
                        if (rDate >= date1 && rDate <= date2) {
                            refundHistory.add(r);
                        }
                    }
                }

            } else {
                Calendar c = Calendar.getInstance();
                c.set(Calendar.HOUR_OF_DAY, 0);
                c.set(Calendar.MINUTE, 0);
                c.set(Calendar.SECOND, 0);
                c.set(Calendar.MILLISECOND, 0);
                Calendar c2 = Calendar.getInstance();
                c2.set(Calendar.HOUR_OF_DAY, 23);
                c2.set(Calendar.MINUTE, 59);
                c2.set(Calendar.SECOND, 59);
                c2.set(Calendar.MILLISECOND, 59);

                Long date1 = c.getTimeInMillis();
                Long date2 = c2.getTimeInMillis();
                for (Refund r : rs) {
                    if (r.getPaid()) {
                        if (r.getRefundedDate() != null) {
                            Long sDate = r.getRefundedDate().getTime();
                            if (sDate >= date1 && sDate <= date2) {
                                refundHistory.add(r);
                            }
                        }
                    }
                }
            }
        }
    }

    public void loadEligibleForRefund() {
        refunds.clear();
        if (null == iquery || "".equalsIgnoreCase(iquery)) {
            JsfUtil.addErrorFacesMessage(ProjectConstants.PAGE_NOT_FOUND);
            throw new IllegalArgumentException(ProjectConstants.PAGE_NOT_FOUND);
        } else {
            this.business = portalPersistence.find(Business.class, Long.valueOf(iquery));

            if (this.business == null) {
                JsfUtil.addErrorFacesMessage(ProjectConstants.PAGE_NOT_FOUND);
                return;
            }

            List<Refund> rs = portalPersistence.getRefundsByBusiness(business);
            for (Refund r : rs) {
                if (!r.getPaid()) {
                    refunds.add(r);
                }
            }
        }
    }

    public List<Cart> getCarts() {
        if (!carts.isEmpty()) {
            carts.clear();
        }
        populateCarts();
        return carts;
    }

    private void populateCarts() {
        if (this.orderedItem != null) {
            final List<Cart> s = portalPersistence.getAllCartsByBatchId(this.orderedItem.getBatchId());
            carts = s;
        } else {
            carts = Collections.EMPTY_LIST;
        }
    }

    public void loadTransactionHistory() {
        paymentTransactions.clear();
        if (null == iquery || "".equalsIgnoreCase(iquery)) {
            JsfUtil.addErrorFacesMessage(ProjectConstants.PAGE_NOT_FOUND);
            throw new IllegalArgumentException(ProjectConstants.PAGE_NOT_FOUND);
        } else {
            this.business = portalPersistence.find(Business.class, Long.valueOf(iquery));

            if (this.business == null) {
                JsfUtil.addErrorFacesMessage(ProjectConstants.PAGE_NOT_FOUND);
                return;
            }

            List<PaymentTransaction> pts = portalPersistence.getTransactionsByBusiness(business);
            if (this.tranxDate1 != null && this.tranxDate2 != null) {
                Long date1 = this.tranxDate1.getTime();
                Long date2 = this.tranxDate2.getTime();
                for (PaymentTransaction r : pts) {
                    if (r.getTranxDate() != null) {
                        Long rDate = r.getTranxDate().getTime();
                        if (rDate >= date1 && rDate <= date2) {
                            paymentTransactions.add(r);
                        }
                    }
                }

            } else {
                Calendar c = Calendar.getInstance();
                c.set(Calendar.HOUR_OF_DAY, 0);
                c.set(Calendar.MINUTE, 0);
                c.set(Calendar.SECOND, 0);
                c.set(Calendar.MILLISECOND, 0);
                Calendar c2 = Calendar.getInstance();
                c2.set(Calendar.HOUR_OF_DAY, 23);
                c2.set(Calendar.MINUTE, 59);
                c2.set(Calendar.SECOND, 59);
                c2.set(Calendar.MILLISECOND, 59);

                Long date1 = c.getTimeInMillis();
                Long date2 = c2.getTimeInMillis();
                for (PaymentTransaction r : pts) {
                    if (r.getTractionStatus() == PaymentTractionStatus.PAYMENYSUCCESSFUL) {
                        if (r.getTranxDate() != null) {
                            Long sDate = r.getTranxDate().getTime();
                            if (sDate >= date1 && sDate <= date2) {
                                paymentTransactions.add(r);
                            }
                        }
                    }
                }
            }
        }
    }

    public void loadTransaction() {
        paymentTransactions.clear();
        onlineTranx.clear();
        offlineTranx.clear();
        selectedOnlineTranx.clear();
        selectedOfflineTranx.clear();
        if (null == iquery || "".equalsIgnoreCase(iquery)) {
            JsfUtil.addErrorFacesMessage(ProjectConstants.PAGE_NOT_FOUND);
            throw new IllegalArgumentException(ProjectConstants.PAGE_NOT_FOUND);
        } else {
            this.business = portalPersistence.find(Business.class, Long.valueOf(iquery));

            if (this.business == null) {
                JsfUtil.addErrorFacesMessage(ProjectConstants.PAGE_NOT_FOUND);
                return;
            }

            List<PaymentTransaction> pts = portalPersistence.getTransactionsByBusiness(business);
            for (PaymentTransaction pt : pts) {
                if (pt.getSettlement() == null) {
                    if (pt.getTractionStatus().equals(PaymentTransaction.PaymentTractionStatus.PAYMENYSUCCESSFUL)) {
                        if (pt.getOrderItem().getPaymentMethod() != null) {
                            if (pt.getOrderItem().getPaymentMethod().equals(KioskPaymentOption.Option.ONLINE)) {
                                if ((pt.getOrderItem().getOrderStatus().equals(OrderedItem.Status.RECEIVED)) || (pt.getOrderItem().getOrderStatus().equals(OrderedItem.Status.PROCESSING))
                                        || (pt.getOrderItem().getOrderStatus().equals(OrderedItem.Status.SHIPPED)) || (pt.getOrderItem().getOrderStatus().equals(OrderedItem.Status.DELIVERED))) {
                                    onlineTranx.add(pt);
                                }
                            }
                            if (pt.getOrderItem().getPaymentMethod().equals(KioskPaymentOption.Option.CASH) || pt.getOrderItem().getPaymentMethod().equals(KioskPaymentOption.Option.POS)) {
                                offlineTranx.add(pt);
                            }
                        }
                    }
                }
            }
        }
    }

    public void loadProviderDetail() {
        if (null == iquery || "".equalsIgnoreCase(iquery)) {
            JsfUtil.addErrorFacesMessage(ProjectConstants.PAGE_NOT_FOUND);
            throw new IllegalArgumentException(ProjectConstants.PAGE_NOT_FOUND);
        } else {
            this.paymentProvider = portalPersistence.find(PaymentProvider.class, Long.valueOf(iquery));

            if (this.paymentProvider == null) {
                JsfUtil.addErrorFacesMessage(ProjectConstants.PAGE_NOT_FOUND);
                return;
            }
            if (this.paymentProvider.getClassName().equalsIgnoreCase(PaymentProviders.PAYSTACK.getPaymentProvider())) {
                this.paystack = portalPersistence.find(Paystack.class, this.paymentProvider.getProviderId());
                this.isPaystack = true;
            }
        }

    }

    public void loadSplitDetail() {
        if (null == iquery || "".equalsIgnoreCase(iquery)) {
            //JsfUtil.addErrorFacesMessage(ProjectConstants.PAGE_NOT_FOUND);
            //throw new IllegalArgumentException(ProjectConstants.PAGE_NOT_FOUND);
            this.split = new Split();
        } else {
            this.split = portalPersistence.find(Split.class, Long.valueOf(iquery));
            if (this.paymentProvider == null) {
                JsfUtil.addErrorFacesMessage(ProjectConstants.PAGE_NOT_FOUND);
            }
        }

    }

    public void loadBusinessPendingPayment() {
        selectedSettlements.clear();
        settlements.clear();
        List<Settlement> ses = portalPersistence.getAllPendingSettlements();
        for (Settlement se : ses) {
            if (se.getBusiness().equals(this.business)) {
                settlements.add(se);
            }
        }
    }

    public String todaySettlement() {
        List<Settlement> ses = portalPersistence.getAllSettlementsByBusiness(business);
        Integer count = 0;

        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        Calendar c2 = Calendar.getInstance();
        c2.set(Calendar.HOUR_OF_DAY, 23);
        c2.set(Calendar.MINUTE, 59);
        c2.set(Calendar.SECOND, 59);
        c2.set(Calendar.MILLISECOND, 59);

        Long date1 = c.getTimeInMillis();
        Long date2 = c2.getTimeInMillis();

        for (Settlement s : ses) {
            if (s.getCreatedDate() != null) {
                Long sDate = s.getCreatedDate().getTime();
                if (sDate >= date1 && sDate <= date2) {
                    count++;
                }
            }
        }

        return String.valueOf(count);

    }

    public String pendingSettlement() {
        List<Settlement> ses = portalPersistence.getAllPendingSettlementsByBusiness(this.business);
        Integer count = ses.size();
        return String.valueOf(count);
    }

    public String pendingPayment() {
        List<Settlement> ses = portalPersistence.getAllPendingSettlementsByBusiness(this.business);
        Double amount = 0.0;
        for (Settlement se : ses) {
            amount += se.getAmountAfterSplit();
        }
        return formatCurrency(amount);
    }

    public void fetchSplitLog(Settlement s) {
        this.splitLogs.clear();
        this.splitLogs = portalPersistence.getSplitLogBySettlement(s);
    }

    public String paidToday() {
        List<Settlement> ses = portalPersistence.getAllSettlementsByBusiness(this.business);
        Double amount = 0.0;

        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        Calendar c2 = Calendar.getInstance();
        c2.set(Calendar.HOUR_OF_DAY, 23);
        c2.set(Calendar.MINUTE, 59);
        c2.set(Calendar.SECOND, 59);
        c2.set(Calendar.MILLISECOND, 59);

        Long date1 = c.getTimeInMillis();
        Long date2 = c2.getTimeInMillis();

        for (Settlement s : ses) {
            if (s.getSettledDate() != null) {
                Long sDate = s.getSettledDate().getTime();
                if (sDate >= date1 && sDate <= date2) {
                    if (s.getPaid()) {
                        amount += s.getAmountAfterSplit();
                    }
                }
            }
        }

        return formatCurrency(amount);

    }

    public String todayIncome() {

        List<PaymentTransaction> pts = portalPersistence.getAllSuccessfulTransactionByBusiness(this.business);
        Double totalAmount = 0.0;

        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        Calendar c2 = Calendar.getInstance();
        c2.set(Calendar.HOUR_OF_DAY, 23);
        c2.set(Calendar.MINUTE, 59);
        c2.set(Calendar.SECOND, 59);
        c2.set(Calendar.MILLISECOND, 59);

        Long date1 = c.getTimeInMillis();
        Long date2 = c2.getTimeInMillis();

        for (PaymentTransaction pt : pts) {
            if (pt.getTranxDate() != null) {
                Long ptDate = pt.getTranxDate().getTime();
                if (ptDate >= date1 && ptDate <= date2) {
                    totalAmount += pt.getAmount();
                }
            }
        }

        return formatCurrency(totalAmount);
    }

    public String formatCurrency(Double amount) {
        double currency = 0;
        if (amount != null) {
            currency = amount;
        }
        return new DecimalFormat("###,###.###").format(currency);
    }

    public static Date dateConverter(String sDate) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        try {
            date = formatter.parse(sDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public List<SubItemCart> populateSubItemCart(Cart cart) {
        subItemCarts.clear();
        if (cart != null) {
            subItemCarts = portalPersistence.getAllSubItemCartByCartId(cart.getId());
        }
        return subItemCarts;
    }

    public PortalUserController getUserController() {
        return userController;
    }

    public void setUserController(PortalUserController userController) {
        this.userController = userController;
    }

    public KioskUser getUser() {
        return user;
    }

    public void setUser(KioskUser user) {
        this.user = user;
    }

    public LazyDataModel<PaymentTransaction> getLazyDataModelPaymentTransaction() {
        return lazyDataModelPaymentTransaction;
    }

    public void setLazyDataModelPaymentTransaction(LazyDataModel<PaymentTransaction> lazyDataModelPaymentTransaction) {
        this.lazyDataModelPaymentTransaction = lazyDataModelPaymentTransaction;
    }

    public List<PaymentProvider> getPaymentProviders() {
        return paymentProviders;
    }

    public void setPaymentProviders(List<PaymentProvider> paymentProviders) {
        this.paymentProviders = paymentProviders;
    }

    public PaymentProvider getPaymentProvider() {
        return paymentProvider;
    }

    public void setPaymentProvider(PaymentProvider paymentProvider) {
        this.paymentProvider = paymentProvider;
    }

    public Paystack getPaystack() {
        return paystack;
    }

    public void setPaystack(Paystack paystack) {
        this.paystack = paystack;
    }

    public List<PaymentProviders> getPps() {
        return pps;
    }

    public void setPps(List<PaymentProviders> pps) {
        this.pps = pps;
    }

    public PaymentProviders getProviders() {
        return providers;
    }

    public void setProviders(PaymentProviders providers) {
        this.providers = providers;
    }

    public boolean isIsPaystack() {
        return isPaystack;
    }

    public void setIsPaystack(boolean isPaystack) {
        this.isPaystack = isPaystack;
    }

    public String getIquery() {
        return iquery;
    }

    public void setIquery(String iquery) {
        this.iquery = iquery;
    }

    public Business getBusiness() {
        return business;
    }

    public void setBusiness(Business business) {
        this.business = business;
    }

    public List<Split> getSplits() {
        return splits;
    }

    public void setSplits(List<Split> splits) {
        this.splits = splits;
    }

    public Split getSplit() {
        return split;
    }

    public void setSplit(Split split) {
        this.split = split;
    }

    public List<PaymentTransaction> getPaymentTransactions() {
        return paymentTransactions;
    }

    public void setPaymentTransactions(List<PaymentTransaction> paymentTransactions) {
        this.paymentTransactions = paymentTransactions;
    }

    public List<PaymentTransaction> getOnlineTranx() {
        return onlineTranx;
    }

    public void setOnlineTranx(List<PaymentTransaction> onlineTranx) {
        this.onlineTranx = onlineTranx;
    }

    public List<PaymentTransaction> getOfflineTranx() {
        return offlineTranx;
    }

    public void setOfflineTranx(List<PaymentTransaction> offlineTranx) {
        this.offlineTranx = offlineTranx;
    }

    public List<PaymentTransaction> getSelectedOnlineTranx() {
        return selectedOnlineTranx;
    }

    public void setSelectedOnlineTranx(List<PaymentTransaction> selectedOnlineTranx) {
        this.selectedOnlineTranx = selectedOnlineTranx;
    }

    public List<PaymentTransaction> getSelectedOfflineTranx() {
        return selectedOfflineTranx;
    }

    public void setSelectedOfflineTranx(List<PaymentTransaction> selectedOfflineTranx) {
        this.selectedOfflineTranx = selectedOfflineTranx;
    }

    public Long[] getSelectedSplits() {
        return selectedSplits;
    }

    public void setSelectedSplits(Long[] selectedSplits) {
        this.selectedSplits = selectedSplits;
    }

    public List<Settlement> getSettlements() {
        return settlements;
    }

    public void setSettlements(List<Settlement> settlements) {
        this.settlements = settlements;
    }

    public List<Settlement> getSelectedSettlements() {
        return selectedSettlements;
    }

    public void setSelectedSettlements(List<Settlement> selectedSettlements) {
        this.selectedSettlements = selectedSettlements;
    }

    public String getPaid() {
        return paid;
    }

    public void setPaid(String paid) {
        this.paid = paid;
    }

    public Date getTranxDate1() {
        return tranxDate1;
    }

    public void setTranxDate1(Date tranxDate1) {
        this.tranxDate1 = tranxDate1;
    }

    public Date getTranxDate2() {
        return tranxDate2;
    }

    public void setTranxDate2(Date tranxDate2) {
        this.tranxDate2 = tranxDate2;
    }

    public List<SplitLog> getSplitLogs() {
        return splitLogs;
    }

    public void setSplitLogs(List<SplitLog> splitLogs) {
        this.splitLogs = splitLogs;
    }

    public void filterSettlementTable() {

    }

    public Settlement getSettlement() {
        return settlement;
    }

    public void setSettlement(Settlement settlement) {
        this.settlement = settlement;
    }

    public List<BusinessAccount> getBusinessAccounts() {
        return businessAccounts;
    }

    public void setBusinessAccounts(List<BusinessAccount> businessAccounts) {
        this.businessAccounts = businessAccounts;
    }

    public BusinessAccount getBusinessAccount() {
        return businessAccount;
    }

    public void setBusinessAccount(BusinessAccount businessAccount) {
        this.businessAccount = businessAccount;
    }

    public List<Bank> getBanks() {
        return banks;
    }

    public void setBanks(List<Bank> banks) {
        this.banks = banks;
    }

    public Bank getBank() {
        return bank;
    }

    public void setBank(Bank bank) {
        this.bank = bank;
    }

    public PaymentTransaction getPaymentTransaction() {
        return paymentTransaction;
    }

    public void setPaymentTransaction(PaymentTransaction paymentTransaction) {
        this.paymentTransaction = paymentTransaction;
    }

    public Refund getRefund() {
        return refund;
    }

    public void setRefund(Refund refund) {
        this.refund = refund;
    }

    public List<Refund> getRefunds() {
        return refunds;
    }

    public void setRefunds(List<Refund> refunds) {
        this.refunds = refunds;
    }

    public List<Refund> getRefundHistory() {
        return refundHistory;
    }

    public void setRefundHistory(List<Refund> refundHistory) {
        this.refundHistory = refundHistory;
    }

    public OrderedItem getOrderedItem() {
        return orderedItem;
    }

    public void setOrderedItem(OrderedItem orderedItem) {
        this.orderedItem = orderedItem;
    }

    public Map<Long, Boolean> getSelectedTranx() {
        return selectedTranx;
    }

    public void setSelectedTranx(Map<Long, Boolean> selectedTranx) {
        this.selectedTranx = selectedTranx;
    }

    public boolean isEnableSettleButton() {
        return enableSettleButton;
    }

    public void setEnableSettleButton(boolean enableSettleButton) {
        this.enableSettleButton = enableSettleButton;
    }

    public boolean isEnableApplySettlementButton() {
        return enableApplySettlementButton;
    }

    public void setEnableApplySettlementButton(boolean enableApplySettlementButton) {
        this.enableApplySettlementButton = enableApplySettlementButton;
    }

    public Map<Long, Boolean> getSelectedSettlement() {
        return selectedSettlement;
    }

    public void setSelectedSettlement(Map<Long, Boolean> selectedSettlement) {
        this.selectedSettlement = selectedSettlement;
    }

    public boolean isEnablePayButton() {
        return enablePayButton;
    }

    public void setEnablePayButton(boolean enablePayButton) {
        this.enablePayButton = enablePayButton;
    }

    public void checkForSellectedTranx(AjaxBehaviorEvent e) {
        this.enableSettleButton = false;

        for (Map.Entry<Long, Boolean> entrySet : selectedTranx.entrySet()) {
            Boolean value = entrySet.getValue();
            if (value) {
                this.enableSettleButton = value;
            }
        }
    }

    public void checkForSellectedSettlement(AjaxBehaviorEvent e) {
        this.enablePayButton = false;
        for (Map.Entry<Long, Boolean> entrySet : selectedSettlement.entrySet()) {
            Boolean value = entrySet.getValue();
            if (value) {
                this.enablePayButton = value;
            }
        }
    }

    public void checkForApplySettlement(AjaxBehaviorEvent e) {
        this.enableApplySettlementButton = false;

        if (selectedSplits.length > 0) {
            this.enableApplySettlementButton = true;
        }
    }

    public void checkeSelecetedProvider() {
        this.isPaystack = false;
        if (this.providers.equals(PaymentProviders.PAYSTACK)) {
            this.isPaystack = true;
        }

        if (this.providers == null) {
            this.isPaystack = false;
        }

    }

    public List<Business> getActiveBusinesses() {
        return portalPersistence.geAllActiveBusiness();
    }

    public void prepareForSettlement(Settlement s) {
        this.splitLogs.clear();
        setSettlement(s);
        fetchSplitLog(s);
    }

    public void prepareForProviderModal(Business business) {
        this.business = business;
        this.paymentProvider = null;
        loadPaymentProvider();
        this.paymentProvider = portalPersistence.getEnabledPaymentProvider();
//        UserPaymentProvider upp = portalPersistence.getUserPaymentProviderbyBusiness(business.getId());
//        if (upp != null) {
//            this.paymentProvider = upp.getProvider();
//        }
    }

    public String businessPaymentProvider(Business b) {
        //String provider = "";
//        UserPaymentProvider upp = portalPersistence.getUserPaymentProviderbyBusiness(b.getId());
//        if (upp != null) {
//            provider = upp.getProvider().getName();
//        }
        String provider = portalPersistence.getEnabledPaymentProvider().getName();
        return provider;
    }

    public void updateProvider() {
        if (isPaystack) {
            try {
                this.paystack.setName(paymentProvider.getName());
                portalPersistence.update(this.paystack);

                this.paymentProvider.setModifiedDate(new Date());
                portalPersistence.update(this.paymentProvider);
                JsfUtil.addInfoFacesMessage("Payment Provider Updated Successfully");
            } catch (Exception e) {
                e.getMessage();
                JsfUtil.addErrorFacesMessage("Payment Provider Upate Unsucessful");
            }
        }
    }

//    public void configureBusinessProvider() {
//        if (this.paymentProvider != null) {
//            try {
//                UserPaymentProvider upp = portalPersistence.getUserPaymentProviderbyBusiness(this.business.getId());
//                if (upp != null) {
//                    upp.setLastModifiedDate(new Date());
//                    upp.setProvider(this.paymentProvider);
//                    portalPersistence.update(upp);
//                    JsfUtil.addInfoFacesMessage("Payment Provider Assigned Successfully");
//                } else {
//                    UserPaymentProvider upp1 = new UserPaymentProvider();
//                    upp1.setBusinessId(this.business.getId());
//                    upp1.setBusinessOwnerId(this.business.getOwner().getId());
//                    upp1.setCreatedDate(new Date());
//                    upp1.setLastModifiedDate(new Date());
//                    upp1.setProvider(this.paymentProvider);
//                    portalPersistence.create(upp1);
//                    JsfUtil.addInfoFacesMessage("Payment Provider Assigned Successfully");
//                }
//            } catch (Exception e) {
//                e.getMessage();
//                JsfUtil.addErrorFacesMessage("Payment Provider Assignment was Unsuccessful");
//            }
//        } else {
//            JsfUtil.addErrorFacesMessage("Please Select a Payment Provider");
//        }
//    }
    public void registerProvider() {
        if (isPaystack) {
            try {
                this.paystack.setName(paymentProvider.getName());
                this.paystack.setStatus(true);
                Paystack p = portalPersistence.create(this.paystack);

                this.paymentProvider.setClassName(PaymentProviders.PAYSTACK.getPaymentProvider());
                this.paymentProvider.setCreatedDate(new Date());
                this.paymentProvider.setStatus(true);
                this.paymentProvider.setModifiedDate(new Date());
                this.paymentProvider.setProviderId(p.getId());

                portalPersistence.create(this.paymentProvider);
                JsfUtil.addInfoFacesMessage("Payment Provider Created Successfully");
                this.paystack = new Paystack();
                this.paymentProvider = new PaymentProvider();
                this.isPaystack = false;
                this.providers = null;

            } catch (Exception e) {
                e.getMessage();
                JsfUtil.addErrorFacesMessage("Payment Provider Creation Unsucessful");
            }
        }
    }

    public String applyRefund() {
        Refund r = portalPersistence.find(Refund.class, refund.getId());
        if (r == null) {
            iquery = String.valueOf(this.business.getId());
            Faces.setViewAttribute(ProjectConstants.PAGE_QUERY_PARAM_ITEM, iquery);
            JsfUtil.addErrorFacesMessage("Please Select a Refund to Pay");
            return null;
        }

        try {
            //UserPaymentProvider upp = portalPersistence.getUserPaymentProviderbyBusiness((this.business.getId()));
            PaymentProvider pp = portalPersistence.getEnabledPaymentProvider();
            String secretKey = "";

            if (pp.getClassName().equalsIgnoreCase("Paystack")) {
                Paystack pt = portalPersistence.find(Paystack.class, pp.getProviderId());
                secretKey = pt.getSecretKey();
            }

            CreateTransferRecepientRequest ctrr = new CreateTransferRecepientRequest();
            ctrr.setAccount_number(refund.getBankAccountNumber());
            ctrr.setBank_code(refund.getBank().getShortCode());
            ctrr.setCurrency("NGN");
            ctrr.setName("Refund Payment for " + r.getOrderedItem().getBatchId());
            ctrr.setType("nuban");
            CreateTransferRecepientResponse ctrr1 = createTransferReceipient(ctrr, secretKey);

            if (ctrr1.isStatus()) {
                InitiateTransferRequest itr = new InitiateTransferRequest();
                itr.setAmount(r.getAmount().intValue());
                itr.setReason("Refund Payment for " + r.getOrderedItem().getBatchId());
                itr.setRecipient(ctrr1.getData().getRecipient_code());
                itr.setSource("balance");
                CreateTransferRecepientResponse ctrr2 = initateTransfer(itr, secretKey);

                if (ctrr2.isStatus()) {
                    ClientResponse response = finalizeTransfer(ctrr2.getData().getTransfer_code(), secretKey);
                    if (response.getStatus() == 200) {
                        r.setBank(refund.getBank());
                        r.setBankAccountNumber(refund.getBankAccountNumber());
                        r.setPaid(true);
                        r.setPaidBy(user);
                        r.setPaymentCode(ctrr2.getData().getTransfer_code());
                        r.setRefundedDate(new Date());
                        portalPersistence.update(r);
                    } else {
                        iquery = String.valueOf(this.business.getId());
                        Faces.setViewAttribute(ProjectConstants.PAGE_QUERY_PARAM_ITEM, iquery);
                        JsfUtil.addErrorFacesMessage("Error in Payment. Please Try Again Later");
                        return null;
                    }

                } else {
                    iquery = String.valueOf(this.business.getId());
                    Faces.setViewAttribute(ProjectConstants.PAGE_QUERY_PARAM_ITEM, iquery);
                    JsfUtil.addErrorFacesMessage(ctrr2.getMessage());
                    return null;
                }

            } else {
                iquery = String.valueOf(this.business.getId());
                Faces.setViewAttribute(ProjectConstants.PAGE_QUERY_PARAM_ITEM, iquery);
                JsfUtil.addErrorFacesMessage(ctrr1.getMessage());
                return null;
            }

        } catch (Exception e) {
            e.getMessage();
            e.printStackTrace();
            JsfUtil.addErrorFacesMessage(ProjectConstants.OPERATION_UNSUCCESSFUL);
            return null;
        }

        iquery = String.valueOf(this.business.getId());
        Faces.setViewAttribute(ProjectConstants.PAGE_QUERY_PARAM_ITEM, iquery);
        JsfUtil.addInfoFacesMessage(ProjectConstants.OPERATION_SUCCESSFUL);
        return "refund?faces-redirect=true&includeViewParams=true";

    }

    public String applyPayment() {
        double amount = 0;
        if (this.selectedSettlement.isEmpty()) {
            iquery = String.valueOf(this.business.getId());
            Faces.setViewAttribute(ProjectConstants.PAGE_QUERY_PARAM_ITEM, iquery);
            JsfUtil.addErrorFacesMessage("Please Select a Settlement to Pay");
            return null;
        }
        try {
            for (Map.Entry<Long, Boolean> entry : selectedSettlement.entrySet()) {
                Long key = entry.getKey();
                Boolean value = entry.getValue();
                if (value) {
                    this.selectedSettlements.add(portalPersistence.find(Settlement.class, key));
                }
            }

            for (Settlement s : this.selectedSettlements) {
                amount += s.getAmountAfterSplit();
            }
            PaymentProvider pp = portalPersistence.getEnabledPaymentProvider();
            String secretKey = "";

            if (pp.getClassName().equalsIgnoreCase("Paystack")) {
                Paystack pt = portalPersistence.find(Paystack.class, pp.getProviderId());
                secretKey = pt.getSecretKey();
            }

            CreateTransferRecepientRequest ctrr = new CreateTransferRecepientRequest();
            ctrr.setAccount_number(businessAccount.getBankAccountNumber());
            ctrr.setBank_code(businessAccount.getBank().getShortCode());
            ctrr.setCurrency("NGN");
            ctrr.setName("Chef Tent Payment to " + business.getName());
            ctrr.setType("nuban");
            CreateTransferRecepientResponse ctrr1 = createTransferReceipient(ctrr, secretKey);

            if (ctrr1.isStatus()) {
                InitiateTransferRequest itr = new InitiateTransferRequest();
                itr.setAmount((int) amount);
                itr.setReason("Chef Tent Payment to " + business.getName());
                itr.setRecipient(ctrr1.getData().getRecipient_code());
                itr.setSource("balance");
                CreateTransferRecepientResponse ctrr2 = initateTransfer(itr, secretKey);

                if (ctrr2.isStatus()) {
                    ClientResponse response = finalizeTransfer(ctrr2.getData().getTransfer_code(), secretKey);
                    if (response.getStatus() == 200) {
                        for (Settlement s : this.selectedSettlements) {
                            s.setPaymentDate(new Date());
                            s.setPaid(true);
                            s.setPaymentCode(ctrr2.getData().getTransfer_code());
                            s.setPaidBy(user);
                            portalPersistence.update(s);

                        }
                    } else {
                        iquery = String.valueOf(this.business.getId());
                        Faces.setViewAttribute(ProjectConstants.PAGE_QUERY_PARAM_ITEM, iquery);
                        JsfUtil.addErrorFacesMessage("Error in Payment. Please Try Again Later");
                        return null;
                    }

                } else {
                    iquery = String.valueOf(this.business.getId());
                    Faces.setViewAttribute(ProjectConstants.PAGE_QUERY_PARAM_ITEM, iquery);
                    JsfUtil.addErrorFacesMessage(ctrr2.getMessage());
                    return null;
                }

            } else {
                iquery = String.valueOf(this.business.getId());
                Faces.setViewAttribute(ProjectConstants.PAGE_QUERY_PARAM_ITEM, iquery);
                JsfUtil.addErrorFacesMessage(ctrr1.getMessage());
                return null;
            }

        } catch (Exception e) {
            e.getMessage();
            e.printStackTrace();
            JsfUtil.addErrorFacesMessage(ProjectConstants.OPERATION_UNSUCCESSFUL);
            return null;
        }
        iquery = String.valueOf(this.business.getId());
        Faces.setViewAttribute(ProjectConstants.PAGE_QUERY_PARAM_ITEM, iquery);
        JsfUtil.addInfoFacesMessage(ProjectConstants.OPERATION_SUCCESSFUL);
        return "settlement?faces-redirect=true&includeViewParams=true";
    }

    private ClientResponse finalizeTransfer(String transferCode, String secretKey) {
        ClientResponse response = null;
        try {
            Client client = Client.create();
            WebResource webResource = client.resource("https://api.paystack.co/transfer/finalize_transfer");
            Map<String, String> dataBody = new HashMap<>();
            dataBody.put("transfer_code", transferCode);
            dataBody.put("otp", "");
            webResource.entity(dataBody);
            response = webResource.header("authorization", "Bearer " + secretKey)
                    .header("content-type", "application/json")
                    .type("application/json")
                    .post(ClientResponse.class, dataBody);

        } catch (UniformInterfaceException | ClientHandlerException e) {
            e.printStackTrace();

        }
        return response;
    }

    private CreateTransferRecepientResponse initateTransfer(InitiateTransferRequest itr, String secretKey) {
        try {
            Client client = Client.create();
            WebResource webResource = client.resource("https://api.paystack.co/transfer");
            ClientResponse response = webResource.header("authorization", "Bearer " + secretKey)
                    .header("content-type", "application/json")
                    .type("application/json")
                    .post(ClientResponse.class, itr);
            return response.getEntity(CreateTransferRecepientResponse.class);
        } catch (UniformInterfaceException | ClientHandlerException e) {
            e.printStackTrace();
            return new CreateTransferRecepientResponse();
        }
    }

//    public String applyPayment() {
//        try {
//            for (Settlement s : this.selectedSettlements) {
//                s.setModifiedDate(new Date());
//                s.setPaid(true);
//                portalPersistence.update(s);
//            }
//            iquery = String.valueOf(this.business.getId());
//            Faces.setViewAttribute(ProjectConstants.PAGE_QUERY_PARAM_ITEM, iquery);
//            JsfUtil.addInfoFacesMessage(ProjectConstants.OPERATION_SUCCESSFUL);
//            return "settlement?faces-redirect=true&includeViewParams=true";
//        } catch (Exception e) {
//            e.getMessage();
//            JsfUtil.addErrorFacesMessage(ProjectConstants.OPERATION_UNSUCCESSFUL);
//            return null;
//        }
//    }
    private CreateTransferRecepientResponse createTransferReceipient(CreateTransferRecepientRequest ctrr, String secretKey) {
        try {
            Client client = Client.create();
            WebResource webResource = client.resource("https://api.paystack.co/transferrecipient");
            ClientResponse response = webResource.header("authorization", "Bearer " + secretKey)
                    .header("content-type", "application/json")
                    .type("application/json")
                    .post(ClientResponse.class, ctrr);
            return response.getEntity(CreateTransferRecepientResponse.class);
        } catch (UniformInterfaceException | ClientHandlerException e) {
            e.printStackTrace();
            return new CreateTransferRecepientResponse();
        }
    }

    public String applySplit() {
        try {
            Settlement s = new Settlement();
            Double amonuntBefore = 0.0;
            Double splitAmount = 0.0;
            Double amountAfter;

            s.setBusiness(business);
            s.setCreatedDate(new Date());
            s.setSettledBy(user);
            s.setSettledDate(new Date());
            s.setPaid(false);

            s = portalPersistence.create(s);

            if (!selectedOnlineTranx.isEmpty()) {
                for (PaymentTransaction pt : selectedOnlineTranx) {
                    amonuntBefore += pt.getAmount();
                    pt.setSettlement(s);
                    portalPersistence.update(pt);
                }
            } else {
                for (PaymentTransaction pt : selectedOfflineTranx) {
                    amonuntBefore += pt.getAmount();
                    pt.setSettlement(s);
                    portalPersistence.update(pt);
                }
            }

            for (Long l : selectedSplits) {
                Split splitSmall = portalPersistence.find(Split.class, l);
                if (splitSmall != null) {
                    Double amtPercent = (splitSmall.getSplitRate().doubleValue() / 100.0);
                    Double amt = amtPercent * amonuntBefore;
                    splitAmount += amt;
                    SplitLog log = new SplitLog();
                    log.setCreatedDate(new Date());
                    log.setSettlement(s);
                    log.setSplit(splitSmall);
                    log.setSplitAmount(amt);
                    portalPersistence.create(log);
                }
            }
            amountAfter = amonuntBefore - splitAmount;
            s.setAmountBeforeSplit(amonuntBefore);
            s.setAmountAfterSplit(amountAfter);
            s.setTotalSplitAmount(splitAmount);
            portalPersistence.update(s);
            iquery = String.valueOf(this.business.getId());
            Faces.setViewAttribute(ProjectConstants.PAGE_QUERY_PARAM_ITEM, iquery);
            JsfUtil.addInfoFacesMessage(ProjectConstants.OPERATION_SUCCESSFUL);
            selectedSplits = new Long[0];
            return "pendingSettlement?faces-redirect=true&includeViewParams=true";
        } catch (Exception e) {
            e.getMessage();
            JsfUtil.addErrorFacesMessage(ProjectConstants.OPERATION_UNSUCCESSFUL);
            return null;
        }
    }

    public void registerBusinessAccount() {
        try {
            BusinessAccount ba = portalPersistence.getBusinessAccountByBankAndAccountNumber(this.businessAccount.getBankAccountNumber(), this.bank);
            if (ba != null) {
                JsfUtil.addErrorFacesMessage("Bank Account Already Exist!");
                return;
            }
            this.businessAccount.setActive(false);
            this.businessAccount.setBusiness(this.business);
            this.businessAccount.setBank(this.bank);
            this.businessAccount.setCreatedDate(new Date());
            this.businessAccount.setModifiedDate(new Date());
            portalPersistence.create(this.businessAccount);
            this.businessAccount = new BusinessAccount();
            this.bank = null;
            JsfUtil.addInfoFacesMessage("Account Registerd Successfully");
        } catch (Exception e) {
            e.getMessage();
            JsfUtil.addErrorFacesMessage("Account Registration Unsucessful");
        }
    }

    public void registerSplit() {
        try {
            Split s = portalPersistence.getSplitByName(this.split.getName());
            if (s != null) {
                JsfUtil.addErrorFacesMessage("Split Already Exist!");
                return;
            }

            this.split.setCreatedDate(new Date());
            this.split.setModifiedDate(new Date());
            this.split.setStatus(true);
            this.split.setSplitRate(this.split.getSplitRate() == null ? 0 : Integer.parseInt(String.valueOf(this.split.getSplitRate()).replaceAll("[^0-9]+", "")));
            portalPersistence.create(this.split);
            JsfUtil.addInfoFacesMessage("Split Created Successfully");
        } catch (Exception e) {
            e.getMessage();
            JsfUtil.addErrorFacesMessage("Split Creation Unsucessful");
        }
    }

    public void updateSplit() {
        try {
            Split ss = portalPersistence.find(Split.class, this.split.getId());
            ss.setStatus(false);
            ss.setModifiedDate(new Date());
            portalPersistence.update(ss);

            Split s = new Split();
            s.setCreatedDate(new Date());
            s.setModifiedDate(new Date());
            s.setDescription(this.split.getDescription());
            s.setName(this.split.getName());
            s.setSplitRate(this.split.getSplitRate() == null ? 0 : Integer.parseInt(String.valueOf(this.split.getSplitRate()).replaceAll("[^0-9]+", "")));
            s.setStatus(true);
            portalPersistence.create(s);
            JsfUtil.addInfoFacesMessage("Split Updated Successfully");
        } catch (Exception e) {
            e.getMessage();
            JsfUtil.addErrorFacesMessage("Split Update Unsucessful");
        }
    }

    public void enablePaymentProvider(PaymentProvider p) {
        try {
            p.setModifiedDate(new Date());
            p.setStatus(true);
            portalPersistence.update(p);

            List<PaymentProvider> ppz = portalPersistence.getAllPaymentProviders();
            ppz.remove(p);
            for (PaymentProvider ps : ppz) {
                if (ps.getStatus()) {
                    ps.setStatus(false);
                    ps.setModifiedDate(new Date());
                    portalPersistence.update(ps);
                }
            }
            JsfUtil.addInfoFacesMessage("Payment Provider Enabled");
        } catch (Exception e) {
            e.getMessage();
            JsfUtil.addErrorFacesMessage(ProjectConstants.OPERATION_UNSUCCESSFUL);
        }
    }

    public void enableBankAccount(BusinessAccount b) {
        try {
            b.setModifiedDate(new Date());
            b.setActive(true);
            portalPersistence.update(b);
            this.businessAccount = new BusinessAccount();
            this.bank = null;

            List<BusinessAccount> bas = portalPersistence.getActiveBusinessAccountsByBusiness(business);
            bas.remove(b);
            for (BusinessAccount ba : bas) {
                ba.setActive(false);
                ba.setModifiedDate(new Date());
                portalPersistence.update(ba);
            }

            JsfUtil.addInfoFacesMessage("Business Account Enabled");
        } catch (Exception e) {
            e.getMessage();
            JsfUtil.addErrorFacesMessage(ProjectConstants.OPERATION_UNSUCCESSFUL);
        }
    }

    public void disableBankAccount(BusinessAccount b) {
        try {
            b.setModifiedDate(new Date());
            b.setActive(false);
            portalPersistence.update(b);
            this.businessAccount = new BusinessAccount();
            this.bank = null;
            JsfUtil.addInfoFacesMessage("Business Account Disabled");
        } catch (Exception e) {
            e.getMessage();
            JsfUtil.addErrorFacesMessage(ProjectConstants.OPERATION_UNSUCCESSFUL);
        }
    }

    public void disableSplit() {
        try {
            this.split.setModifiedDate(new Date());
            this.split.setStatus(false);
            portalPersistence.update(this.split);
            JsfUtil.addInfoFacesMessage("Split Disabled");
        } catch (Exception e) {
            e.getMessage();
            JsfUtil.addErrorFacesMessage(ProjectConstants.OPERATION_UNSUCCESSFUL);
        }
    }

    public void enableSplit() {
        try {
            this.split.setModifiedDate(new Date());
            this.split.setStatus(true);
            portalPersistence.update(this.split);
            JsfUtil.addInfoFacesMessage("Split Enabled");
        } catch (Exception e) {
            e.getMessage();
            JsfUtil.addErrorFacesMessage(ProjectConstants.OPERATION_UNSUCCESSFUL);
        }
    }

    public void loadPaymentProviderList() {
        pps = Arrays.asList(PaymentProviders.values());
    }

    public enum PaymentProviders {

        PAYSTACK("Paystack"),
        MORPHEXPRESS("Morph Express"),
        INTERSWITCH("Interswitch");

        private final String paymentProvider;

        private PaymentProviders(String paymentProvider) {
            this.paymentProvider = paymentProvider;
        }

        public String getPaymentProvider() {
            return paymentProvider;
        }

    }
}

class LazySorter implements Comparator<PaymentTransaction> {

    private String sortField;
    private SortOrder sortOrder;

    public LazySorter(String sortField, SortOrder sortOrder) {
        this.sortField = sortField;
        this.sortOrder = sortOrder;
    }

    @Override
    public int compare(PaymentTransaction o1, PaymentTransaction o2) {
        try {

            Field field = PaymentTransaction.class.getDeclaredField(sortField);
            field.setAccessible(true);
            Object value1 = field.get(o1);

//            Field field2 = PaymentTransaction.class.getDeclaredField(sortField);
//            field.setAccessible(true);
            Object value2 = field.get(o2);

//             String[] s = field.toString().split("\\.");
//             String r = (s.length != 0) ? s[s.length - 1] : field.toString();
//            
            System.out.println("Compare variables :::: " + value1 + " :::::: " + value2);

            int value = ((Comparable) value1).compareTo(value2);
            return SortOrder.ASCENDING.equals(sortOrder) ? value : -1 * value;

        } catch (IllegalAccessException | IllegalArgumentException | NoSuchFieldException | SecurityException ex) {
            System.err.println("Error ::::: " + ex);
            return 0;
        }
    }

    public String getSortField() {
        return sortField;
    }

    public void setSortField(String sortField) {
        this.sortField = sortField;
    }

    public SortOrder getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(SortOrder sortOrder) {
        this.sortOrder = sortOrder;
    }

}
