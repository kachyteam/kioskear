/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.morph.kiosk.controller;

import com.morph.kiosk.persistence.entity.KioskUser;
import com.morph.kiosk.persistence.entity.portal.PortalAccessGroup_Permission;
import com.morph.kiosk.persistence.entity.portal.PortalPermission;
import com.morph.kiosk.persistence.entity.portal.PortalUser;
import com.morph.kiosk.persistence.entity.portal.PortalUserGroup;
import com.morph.kiosk.persistence.entity.portal.PortalUserGroup_AccessGroup;
import com.morph.kiosk.persistence.entity.portal.PortalUserGroup_User;
import com.morph.kiosk.persistence.service.PortalPersistence;
import com.morph.kiosk.persistence.service.UserPersistence;
import com.morph.kiosk.util.JsfUtil;
import com.morph.util.ProjectConstants;
import java.io.IOException;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import javax.annotation.Resource;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

/**
 *
 * @author Tobi-Morph-PC
 */
@Named
@SessionScoped
public class PortalUserController extends BaseController implements Serializable {

    @Resource(lookup = "java:global/KioskEJBModule-1.2/UserPersistence!com.morph.kiosk.persistence.service.UserPersistence")
    private UserPersistence userPersistence;

    @Resource(lookup = "java:global/KioskEJBModule-1.2/PortalPersistence!com.morph.kiosk.persistence.service.PortalPersistence")
    private PortalPersistence portalPersistence;

    @Inject
    private PortalPermissionController permissionController;

    private KioskUser user;
    private KioskUser managedUser;
    private PortalUser portalUser;

    private List<KioskUser> activateKioskUsers;
    private List<KioskUser> deactivateKioskUsers;
    private List<PortalUser> activateBusinessPortalUsers;
    private List<PortalUser> deactivateBusinessPortalUsers;

    private List<PortalPermission> permissions = new ArrayList<>();

    private PortalUserGroup_User group_User = new PortalUserGroup_User();
    
    private PortalUserGroup userGroup = new PortalUserGroup();
    
    private Long businessOwnerCount;
    private Long businessStaffCount;
    private Long activeUserCount;
    private Long deactivateUserCount;

    public PortalUserController() {
    }

    @Override
    void initUser(KioskUser user) {
        this.user = user;
    }

    public String viewDetail(KioskUser ku) {
        this.managedUser = ku;
        return "sysPortalUserDetail";
    }

    public String viewBusinessOwnerDetail(PortalUser portalUser) {
        this.portalUser = portalUser;
        this.managedUser = portalUser.getUser();
        return "sysPortalUserDetail";
    }

    public void reset() {
        this.managedUser = new KioskUser();
        this.portalUser = new PortalUser();
    }

    public void updateManagedUser() {
        try {
            this.managedUser.setModifiedDate(new Date());
            portalPersistence.update(this.managedUser);
            JsfUtil.addInfoFacesMessage(ProjectConstants.UPDATE_SUCCESSFUL);
        } catch (Exception e) {
            JsfUtil.addErrorFacesMessage(ProjectConstants.OPERATION_UNSUCCESSFUL);
            e.getLocalizedMessage();
        }
    }

    public void enableManagedUser() {
        try {
            this.managedUser.setModifiedDate(new Date());
            this.managedUser.setActive(true);
            portalPersistence.update(this.managedUser);
            JsfUtil.addInfoFacesMessage(ProjectConstants.UPDATE_SUCCESSFUL);
        } catch (Exception e) {
            JsfUtil.addErrorFacesMessage(ProjectConstants.UPDATE_UNSUCCESSFUL);
            throw new IllegalArgumentException("Error ::::: ", e);
        }
    }

    public void disableManagedUser() {
        try {
            this.managedUser.setModifiedDate(new Date());
            this.managedUser.setActive(false);
            portalPersistence.update(this.managedUser);
            JsfUtil.addInfoFacesMessage(ProjectConstants.UPDATE_SUCCESSFUL);
        } catch (Exception e) {
            JsfUtil.addErrorFacesMessage(ProjectConstants.UPDATE_UNSUCCESSFUL);
            e.getLocalizedMessage();
        }
    }

    public void emptyDetailPageRedirect() throws IOException {
        if (null == this.managedUser || null == this.managedUser.getId()) {
            ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
            ec.redirect(ec.getRequestContextPath() + "/user/portalUsers.xhtml");
        }
    }

    public void logout() {
        try {
            JsfUtil.getHttpSession().removeAttribute("user");
            JsfUtil.getHttpSession().removeAttribute("userroles");
            FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
            FacesContext.getCurrentInstance().getExternalContext().redirect(JsfUtil.getServletContext().getContextPath() + "/faces/signin.xhtml");
            //FacesContext.getCurrentInstance().getExternalContext().redirect(JsfUtil.getServletContext().getContextPath() + "/signin.xhtml");
        } catch (IOException ex) {
            ex.getLocalizedMessage();
        }
    }

    public void viewUserPermissions() {
        System.out.println("viewUserPermissions PortalUserGroup_User :::: " + group_User);
        try {
            List<PortalAccessGroup_Permission> list = new ArrayList<>();
            List<PortalUserGroup_AccessGroup> portalUserGroup_AccessGroups = userPersistence.getUserGroupAccessGroupByUserGroup(group_User.getUserGroup().getId());
            System.out.println("portalUserGroup_AccessGroups ::::: " + portalUserGroup_AccessGroups);
            portalUserGroup_AccessGroups.forEach((PortalUserGroup_AccessGroup i) -> {
                List<PortalAccessGroup_Permission> l = portalPersistence.getPortalAccessGroupPermissionByAccessGroup(i.getAccessGroup().getId());
                list.addAll(l);
            });

            permissions = list.stream().map(i -> i.getPermission()).collect(Collectors.toList());
            System.out.println("User Permissions ::::: " + permissions);
        } catch (Exception e) {
            System.err.println("Error viewUserPermissions :::: " + e.getLocalizedMessage());
        }
    }

    public void updateManageUserUserGroup() {
        
        System.out.println("Selected Group :::::: " + userGroup);
        System.out.println("User to be edited :::::::: "+ this.managedUser);
        
        try {
            PortalUserGroup_User pugu = userPersistence.getPortalUserGroup_UserByUserId(this.managedUser.getId());
            
            if(null != pugu){
                pugu.setUserGroup(userGroup);
                portalPersistence.update(pugu);
                System.out.println("Updated PortalUserGroup_User ::: " + pugu.getUserGroup().getUserGroupName());
            }else{
                pugu = new PortalUserGroup_User();
                pugu.setUser(this.managedUser);
                pugu.setUserGroup(userGroup);
                portalPersistence.create(pugu);
                System.out.println("Created PortalUserGroup_User ::: " + pugu.getUserGroup().getUserGroupName());
            }
            JsfUtil.addInfoFacesMessage(ProjectConstants.OPERATION_SUCCESSFUL);
        } catch (Exception e) {
            System.err.println("updateManageUserUserGroup ::: " + e.getLocalizedMessage());
        }
        userGroup = new PortalUserGroup();
    }

    public KioskUser getUser() {
        return user;
    }

    public void setUser(KioskUser user) {
        this.user = user;
    }

    public KioskUser getManagedUser() {
        return managedUser;
    }

    public void setManagedUser(KioskUser managedUser) {
        this.managedUser = managedUser;
    }

    public PortalUser getPortalUser() {
        return portalUser;
    }

    public void setPortalUser(PortalUser portalUser) {
        this.portalUser = portalUser;
    }

    public List<PortalUser> getActivateBusinessPortalUsers() {
        final List<PortalUser> l = userPersistence.getAllActivePortalUsers();
        activateBusinessPortalUsers = l;
        return activateBusinessPortalUsers;
    }

    public void setActivateBusinessPortalUsers(List<PortalUser> activateBusinessPortalUsers) {
        this.activateBusinessPortalUsers = activateBusinessPortalUsers;
    }

    public List<PortalUser> getDeactivateBusinessPortalUsers() {
        final List<PortalUser> l = userPersistence.getAllDeactivatedPortalUsers();
        deactivateBusinessPortalUsers = l;
        return deactivateBusinessPortalUsers;
    }

    public void setDeactivateBusinessPortalUsers(List<PortalUser> deactivateBusinessPortalUsers) {
        this.deactivateBusinessPortalUsers = deactivateBusinessPortalUsers;
    }

    public List<KioskUser> getActivateKioskUsers() {
        final List<KioskUser> l = userPersistence.getAllActiveKioskUsers();
        activateKioskUsers = l;
        return activateKioskUsers;
    }

    public void setActivateKioskUsers(List<KioskUser> activateKioskUsers) {
        this.activateKioskUsers = activateKioskUsers;
    }

    public List<KioskUser> getDeactivateKioskUsers() {
        final List<KioskUser> l = userPersistence.getAllDeactivatedKioskUsers();
        deactivateKioskUsers = l;
        return deactivateKioskUsers;
    }

    public void setDeactivateKioskUsers(List<KioskUser> deactivateKioskUsers) {
        this.deactivateKioskUsers = deactivateKioskUsers;
    }

    private void populateGroup_User(){
        System.out.println("populateGroup_User ::: " + this.managedUser);
        try {
            group_User = userPersistence.getPortalUserGroup_UserByUserId(this.managedUser.getId());
        } catch (Exception e) {
            System.err.println("populateGroup_User :::: " + e.getMessage());
        }
    }
    
    public PortalUserGroup_User getGroup_User() {
        populateGroup_User();
        return group_User;
    }

    public void setGroup_User(PortalUserGroup_User group_User) {
        this.group_User = group_User;
    }

    public PortalPermissionController getPermissionController() {
        return permissionController;
    }

    public void setPermissionController(PortalPermissionController permissionController) {
        this.permissionController = permissionController;
    }

    public List<PortalPermission> getPermissions() {
        return permissions;
    }

    public void setPermissions(List<PortalPermission> permissions) {
        this.permissions = permissions;
    }

    public PortalUserGroup getUserGroup() {
        return userGroup;
    }

    public void setUserGroup(PortalUserGroup userGroup) {
        this.userGroup = userGroup;
    }

    public Long getBusinessOwnerCount() {
        return businessOwnerCount;
    }

    public void setBusinessOwnerCount(Long businessOwnerCount) {
        this.businessOwnerCount = businessOwnerCount;
    }

    public Long getBusinessStaffCount() {
        return businessStaffCount;
    }

    public void setBusinessStaffCount(Long businessStaffCount) {
        this.businessStaffCount = businessStaffCount;
    }

    public Long getActiveUserCount() {
        return activeUserCount;
    }

    public void setActiveUserCount(Long activeUserCount) {
        this.activeUserCount = activeUserCount;
    }

    public Long getDeactivateUserCount() {
        return deactivateUserCount;
    }

    public void setDeactivateUserCount(Long deactivateUserCount) {
        this.deactivateUserCount = deactivateUserCount;
    }

    public UserPersistence getUserPersistence() {
        return userPersistence;
    }

    public void setUserPersistence(UserPersistence userPersistence) {
        this.userPersistence = userPersistence;
    }

    public PortalPersistence getPortalPersistence() {
        return portalPersistence;
    }

    public void setPortalPersistence(PortalPersistence portalPersistence) {
        this.portalPersistence = portalPersistence;
    }
}
