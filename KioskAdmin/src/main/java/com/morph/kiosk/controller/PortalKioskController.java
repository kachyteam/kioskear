/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.morph.kiosk.controller;

import com.morph.kiosk.persistence.entity.Kiosk;
import com.morph.kiosk.persistence.entity.KioskUser;
import com.morph.kiosk.persistence.service.PortalPersistence;
import com.morph.kiosk.persistence.service.UserPersistence;
import com.morph.kiosk.util.JsfUtil;
import com.morph.util.ProjectConstants;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.annotation.Resource;

/**
 *
 * @author Tobi-Morph-PC
 */
@Named(value = "portalKioskController")
@SessionScoped
public class PortalKioskController extends  BaseController implements Serializable {

    @Resource(lookup = "java:global/KioskEJBModule-1.2/UserPersistence!com.morph.kiosk.persistence.service.UserPersistence")
    private UserPersistence userPersistence;

    @Resource(lookup = "java:global/KioskEJBModule-1.2/PortalPersistence!com.morph.kiosk.persistence.service.PortalPersistence")
    private PortalPersistence portalPersistence;
    
    private KioskUser user;
    
    private Kiosk kiosk;
    
    public PortalKioskController() {
    }

    @Override
    void initUser(KioskUser user) {
        this.user = user;
    }
    
    public String viewDetail(Kiosk kiosk){
        this.kiosk = kiosk;
        return "sysKioskDetail";
    }

    public String emptyKioskListRedirect(){
        if(null == this.kiosk){
            return "sysKiosks";
        }else{
            return null;
        }
    }
    
    public void reset(){
        this.kiosk = new Kiosk();
    }
    
    public void updateKiosk() {
        try {
            portalPersistence.update(this.kiosk);
            JsfUtil.addInfoFacesMessage(ProjectConstants.UPDATE_SUCCESSFUL);
        } catch (Exception e) {
            JsfUtil.addErrorFacesMessage(ProjectConstants.UPDATE_UNSUCCESSFUL);
            e.getMessage();
        }
    }

    public void disableKiosk() {
        try {
            this.kiosk.setModifiedDate(new Date());
            this.kiosk.setStatus(false);
            portalPersistence.update(this.kiosk);
            JsfUtil.addInfoFacesMessage(ProjectConstants.KIOSK_DISABLED);
        } catch (Exception e) {
            JsfUtil.addErrorFacesMessage(ProjectConstants.UPDATE_UNSUCCESSFUL);
            e.getLocalizedMessage();
        }
    }

    
    public void enableKiosk() {
        try {
            this.kiosk.setModifiedDate(new Date());
            this.kiosk.setStatus(true);
            portalPersistence.update(this.kiosk);
            JsfUtil.addInfoFacesMessage(ProjectConstants.BUSINESS_ENABLED);
        } catch (Exception e) {
            JsfUtil.addErrorFacesMessage(ProjectConstants.UPDATE_UNSUCCESSFUL);
            throw new IllegalArgumentException("Error ::::: ", e);
        }
    }
    
    public KioskUser getUser() {
        return user;
    }

    public void setUser(KioskUser user) {
        this.user = user;
    }

    public Kiosk getKiosk() {
        return kiosk;
    }

    public void setKiosk(Kiosk kiosk) {
        this.kiosk = kiosk;
    }
    
    public List<Kiosk> getActiveKiosk() {
        final List<Kiosk> k = portalPersistence.getAllActiveKiosk();
        return k;
    }

    public List<Kiosk> getDisabledKiosk() {
        final List<Kiosk> k = portalPersistence.getAllDeactivatedKiosk();
        return k;
    }
    
}
