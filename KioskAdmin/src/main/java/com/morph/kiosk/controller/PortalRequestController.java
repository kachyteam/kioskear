/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.morph.kiosk.controller;

import com.morph.kiosk.persistence.entity.KioskUser;
import com.morph.kiosk.persistence.entity.portal.BusinessStaff;
import com.morph.kiosk.persistence.entity.portal.PortalRequest;
import com.morph.kiosk.persistence.entity.portal.PortalUser;
import com.morph.kiosk.persistence.entity.portal.PortalUserGroup;
import com.morph.kiosk.persistence.entity.portal.PortalUserGroup_User;
import com.morph.kiosk.persistence.service.PortalPersistence;
import com.morph.kiosk.persistence.service.UserPersistence;
import com.morph.kiosk.util.JsfUtil;
import com.morph.util.ProjectConstants;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.Asynchronous;
import javax.faces.event.ActionEvent;

/**
 *
 * @author Tobi-Morph-PC
 */
@Named(value = "portalRequestController")
@SessionScoped
public class PortalRequestController extends BaseController implements Serializable {

    @Resource(lookup = "java:global/KioskEJBModule-1.2/UserPersistence!com.morph.kiosk.persistence.service.UserPersistence")
    private UserPersistence persistence;

    @Resource(lookup = "java:global/KioskEJBModule-1.2/PortalPersistence!com.morph.kiosk.persistence.service.PortalPersistence")
    private PortalPersistence portalPersistence;

    private KioskUser user;
    private PortalRequest request = new PortalRequest();
    private List<PortalRequest> activeRequests = new ArrayList<>();
    private List<PortalRequest> pendingRequests = new ArrayList<>();
    private List<PortalRequest> processingRequests = new ArrayList<>();
    private List<PortalRequest> selectedRequests = new ArrayList<>();
    private List<PortalRequest> revokeRequests = new ArrayList<>();

    private Long activeRequestCount;
    private Long pendingRequestCount;
    private Long processingRequestCount;
    private Long revokeRequestCount;

    public PortalRequestController() {
    }

    @Override
    void initUser(KioskUser user) {
        this.user = user;

        activeRequestCount = portalPersistence.getPortalDynamicAccessRequestCount(PortalRequest.RequestStatus.APPROVED);
        pendingRequestCount = portalPersistence.getPortalDynamicAccessRequestCount(PortalRequest.RequestStatus.PENDING);
        processingRequestCount = portalPersistence.getPortalDynamicAccessRequestCount(PortalRequest.RequestStatus.PROCESSING);
        revokeRequestCount = portalPersistence.getPortalDynamicAccessRequestCount(PortalRequest.RequestStatus.REVOKED);

    }

    public void approveRequest() {
        try {
            this.request.setLastModifiedDate(new Date());
            this.request.setActive(true);
            this.request.setStatus(PortalRequest.RequestStatus.APPROVED);
            persistence.create(this.request);

            PortalUser pu = new PortalUser();

            pu.setRequest(this.request);
            pu.setCreatedDate(new Date());
            pu.setActive(true);
            System.out.println(pu);
            JsfUtil.addInfoFacesMessage(ProjectConstants.OPERATION_SUCCESSFUL);
            getActiveRequests();

        } catch (Exception e) {
            JsfUtil.addWarningFacesMessage(ProjectConstants.OPERATION_UNSUCCESSFUL);
            e.getLocalizedMessage();
        }
    }

    public void denyAccessRequest() {
        try {
            this.request.setLastModifiedDate(new Date());
            this.request.setActive(false);
            this.request.setStatus(PortalRequest.RequestStatus.DENIED);
            persistence.create(this.request);
            JsfUtil.addInfoFacesMessage(ProjectConstants.OPERATION_SUCCESSFUL);
        } catch (Exception e) {
            e.getLocalizedMessage();
            JsfUtil.addWarningFacesMessage(ProjectConstants.OPERATION_UNSUCCESSFUL);
        }
    }

    public void revokeUserPortalAccess() {

    }

    public PortalRequest.RequestStatus[] getRequestStatus() {
        return PortalRequest.RequestStatus.values();

    }

    public void activateSelectedRequest(ActionEvent actionEvent) {
        if (!selectedRequests.isEmpty()) {
            selectedRequests.forEach((PortalRequest s) -> {
                try {
                    System.out.println("Portal Request ::::: " + s);
                    s.setActive(true);
                    s.setLastModifiedDate(new Date());
                    s.setStatus(PortalRequest.RequestStatus.APPROVED);
                    persistence.update(s);

                    PortalUser pu = new PortalUser();
                    pu.setUser(s.getUser());
                    pu.setCreatedDate(new Date());
                    pu.setRequest(s);
                    pu.setActive(true);
                    persistence.create(pu);
                    processRequestPermission(s, pu.getUser());

                    BusinessStaff bs = new BusinessStaff();
                    bs.setActive(true);
                    bs.setCreator(s.getUser());
                    bs.setDateCreated(new Date());
                    bs.setDateLastModified(new Date());
                    bs.setStaff(s.getUser());
                    persistence.create(bs);

                } catch (Exception e) {
                    JsfUtil.addWarningFacesMessage(ProjectConstants.OPERATION_UNSUCCESSFUL);
                    e.getLocalizedMessage();
                }
            });
            getSelectedRequests().clear();
            getActiveRequests();
            getPendingRequests();
            getProcessingRequests();
            JsfUtil.addInfoFacesMessage(ProjectConstants.OPERATION_SUCCESSFUL);
        }
    }

    public void revokeSelectedRequest(ActionEvent actionEvent) {

        if (!selectedRequests.isEmpty()) {
            selectedRequests.forEach((PortalRequest i) -> {
                PortalRequest pr = persistence.find(PortalRequest.class, i.getId());
                if (pr != null) {
                    pr.setActive(false);
                    pr.setLastModifiedDate(new Date());
                    pr.setStatus(PortalRequest.RequestStatus.REVOKED);
                    persistence.update(pr);
                }
            });
            getActiveRequests();
            getPendingRequests();
        }
        JsfUtil.addInfoFacesMessage(ProjectConstants.OPERATION_SUCCESSFUL);
    }

    public String view(Long id) {
        this.request = persistence.find(PortalRequest.class, id);
        if (request != null) {
            return "sysRequestDetail";
        }
        return null;
    }

    public String viewList() {
        request = null;
        return "sysRequests";
    }

    @Asynchronous
    private void processRequestPermission(PortalRequest request, KioskUser user) {
        try {
            PortalUserGroup group = portalPersistence.getPortalUserGroupByUserGroupName(request.getRequestType().getUserGroup().getUserGroupName());
            PortalUserGroup_User group_User = new PortalUserGroup_User();
            group_User.setDateCreated(new Date());
            group_User.setActive(true);
            group_User.setUser(user);
            group_User.setUserGroup(group);
            portalPersistence.create(group_User);
            System.out.println("processing group_User :::: " + group_User);
        } catch (Exception e) {
            System.out.println("processRequestPermission  ::: " + e.getLocalizedMessage());
        }
    }

            //request tyoe has a group attahed to it by default
    public KioskUser getUser() {
        return user;
    }

    public void setUser(KioskUser user) {
        this.user = user;
    }

    public PortalRequest getRequest() {
        return request;
    }

    public void setRequest(PortalRequest request) {
        this.request = request;
    }

    public List<PortalRequest> getActiveRequests() {
        activeRequests = portalPersistence.getPortalDynamicAccessRequest(PortalRequest.RequestStatus.APPROVED);
        return activeRequests;
    }

    public void setActiveRequests(List<PortalRequest> activeRequests) {
        this.activeRequests = activeRequests;
    }

    public List<PortalRequest> getPendingRequests() {
        pendingRequests = portalPersistence.getPortalDynamicAccessRequest(PortalRequest.RequestStatus.PENDING);
        return pendingRequests;
    }

    public void setPendingRequests(List<PortalRequest> pendingRequests) {
        this.pendingRequests = pendingRequests;
    }

    public List<PortalRequest> getProcessingRequests() {
        processingRequests = portalPersistence.getPortalDynamicAccessRequest(PortalRequest.RequestStatus.PROCESSING);
        return processingRequests;
    }

    public void setProcessingRequests(List<PortalRequest> processingRequests) {
        this.processingRequests = processingRequests;
    }

    public List<PortalRequest> getSelectedRequests() {
        return selectedRequests;
    }

    public void setSelectedRequests(List<PortalRequest> selectedRequests) {
        this.selectedRequests = selectedRequests;
    }

    public List<PortalRequest> getRevokeRequests() {
        revokeRequests = portalPersistence.getPortalDynamicAccessRequest(PortalRequest.RequestStatus.REVOKED);
        return revokeRequests;
    }

    public void setRevokeRequests(List<PortalRequest> revokeRequests) {
        this.revokeRequests = revokeRequests;
    }

    public Long getActiveRequestCount() {
        return activeRequestCount;
    }

    public void setActiveRequestCount(Long activeRequestCount) {
        this.activeRequestCount = activeRequestCount;
    }

    public Long getPendingRequestCount() {
        return pendingRequestCount;
    }

    public void setPendingRequestCount(Long pendingRequestCount) {
        this.pendingRequestCount = pendingRequestCount;
    }

    public Long getRevokeRequestCount() {
        return revokeRequestCount;
    }

    public void setRevokeRequestCount(Long revokeRequestCount) {
        this.revokeRequestCount = revokeRequestCount;
    }

    public UserPersistence getPersistence() {
        return persistence;
    }

    public void setPersistence(UserPersistence persistence) {
        this.persistence = persistence;
    }

    public Long getProcessingRequestCount() {
        return processingRequestCount;
    }

    public void setProcessingRequestCount(Long processingRequestCount) {
        this.processingRequestCount = processingRequestCount;
    }
}
