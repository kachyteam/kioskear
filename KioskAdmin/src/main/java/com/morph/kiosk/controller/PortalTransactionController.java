/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.morph.kiosk.controller;

import com.morph.kiosk.persistence.entity.KioskUser;
import com.morph.kiosk.persistence.entity.order.Cart;
import com.morph.kiosk.persistence.entity.order.OrderedItem;
import com.morph.kiosk.persistence.entity.order.OrderedItemStatusLog;
import com.morph.kiosk.persistence.service.PortalPersistence;
import com.morph.kiosk.persistence.service.UserPersistence;
import com.morph.kiosk.util.OrderedItemLazyDataModel;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.Asynchronous;
import org.primefaces.model.LazyDataModel;

/**
 *
 * @author Tobi-Morph-PC
 */
@Named(value = "portalTransactionController")
@SessionScoped
public class PortalTransactionController extends BaseController implements Serializable {

    @Resource(lookup = "java:global/KioskEJBModule-1.2/UserPersistence!com.morph.kiosk.persistence.service.UserPersistence")
    private UserPersistence userPersistence;

    @Resource(lookup = "java:global/KioskEJBModule-1.2/PortalPersistence!com.morph.kiosk.persistence.service.PortalPersistence")
    private PortalPersistence portalPersistence;

    private List<OrderedItem> orderedItems = new ArrayList<>();
    
    private LazyDataModel<OrderedItem> lazyDataModelOrderedItem;
    
    private OrderedItem orderedItem;

    private OrderedItem.Status status;

    private KioskUser user;

    private List<Cart> carts = new ArrayList<>();

    public PortalTransactionController() {
    }

    @Override
    void initUser(KioskUser user) {
        this.user = user;
        lazyDataModelOrderedItem = new OrderedItemLazyDataModel(portalPersistence, null);
    }

    public void editOrderStatus(OrderedItem orderedItem) {
        this.orderedItem = orderedItem;
        System.out.println("Ordered Item to be edited :::: " + this.orderedItem);
    }

    public void saveOrderStatusUpdate() {
        System.out.println("Selected Status ::::: " + status.name());
        this.orderedItem.setOrderStatus(status);
        portalPersistence.update(this.orderedItem);
        logOrderedItemStatusUpdate(this.orderedItem);
    }

    public List<OrderedItem.Status> getOrderedItemOption() {
        return Arrays.asList(OrderedItem.Status.values());
    }

    @Asynchronous
    private void logOrderedItemStatusUpdate(OrderedItem item) {
        OrderedItemStatusLog log = new OrderedItemStatusLog();
        log.setItem(item);
        log.setCreatedDate(new Date());
        log.setOrderStatus(status);
        log.setUpdatedby(user);
        portalPersistence.create(log);
    }

    public List<OrderedItemStatusLog> getOrderedItemStatusLog(Long itemId) {
        if (null != itemId) {
            System.out.println("ordered Log for ::::: " + itemId);
            System.out.println("Ordered Item Log ::: " + portalPersistence.getOrderedItemStatusLogByItemId(itemId));
            return portalPersistence.getOrderedItemStatusLogByItemId(itemId);
        } else {
            return Collections.EMPTY_LIST;
        }
    }

    public KioskUser getUser() {
        return user;
    }

    public void setUser(KioskUser user) {
        this.user = user;
    }

    public List<OrderedItem> getOrderedItems() {
        orderedItems = portalPersistence.getAllOderedItems();
        //System.out.println("Ordered Items ::: " + portalPersistence.getOrderedItems());
        return orderedItems;
    }

    public void setOrderedItems(List<OrderedItem> orderedItems) {
        this.orderedItems = orderedItems;
    }

    public OrderedItem getOrderedItem() {
        return orderedItem;
    }

    public void setOrderedItem(OrderedItem orderedItem) {
        this.orderedItem = orderedItem;
    }

    public OrderedItem.Status getStatus() {
        return status;
    }

    public void setStatus(OrderedItem.Status status) {
        this.status = status;
    }

    public List<Cart> getCarts() {
        if (this.orderedItem != null) {
            final List<Cart> s = portalPersistence.getAllCartsByBatchId(this.orderedItem.getBatchId());
            carts = s;
        }else{
            carts = Collections.EMPTY_LIST;
        }
        System.out.println("Carts :::: " + carts);
        return carts;
    }

    public void setCarts(List<Cart> carts) {
        this.carts = carts;
    }

    public LazyDataModel<OrderedItem> getLazyDataModelOrderedItem() {
        return lazyDataModelOrderedItem;
    }

    public void setLazyDataModelOrderedItem(LazyDataModel<OrderedItem> lazyDataModelOrderedItem) {
        this.lazyDataModelOrderedItem = lazyDataModelOrderedItem;
    }
}
