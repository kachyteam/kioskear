/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *Class Name: AdminPortalFilter
 *Project Name: KioskAdmin
 *Developer: Tobi Osagbemi (tosagbemi@morphinnovations.com)
 *Version Info:
 *Create Date: Feb 23, 2017 4:08:22 PM
 *(C) Morph Innovations Limited 2017. Morph Innovations Limited Asserts its right to be known as the author and owner of this file and its contents.
 */
package com.morph.kiosk.filter;

import com.morph.kiosk.persistence.entity.KioskUser;
import com.morph.kiosk.persistence.entity.portal.PortalSuperAdminUser;
import com.morph.kiosk.persistence.entity.portal.PortalUser;
import com.morph.util.ProjectConstants;
import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Tobi-Morph-PC
 */
public class AdminPortalFilter implements Filter {

//    @Resource(lookup = "java:global/KioskEJBModule-1.2/UserPersistence!com.morph.kiosk.persistence.service.UserPersistence")
//    private UserPersistence persistence;
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        KioskUser user = (KioskUser) ((HttpServletRequest) request).getSession().getAttribute(ProjectConstants.SESSION_KIOSK_USER);

        if (null == user || !KioskUser.Designation.ADMINISTRATOR.getDesignation().equalsIgnoreCase(user.getDesignation().getDesignation())) {
            // String contextPath = ((HttpServletRequest) request).getContextPath();
            String requestURI = ((HttpServletRequest) request).getRequestURI();
            String queryString = ((HttpServletRequest) request).getQueryString();

            if (null != queryString && !queryString.isEmpty()) {
                requestURI = requestURI + "?" + queryString;
            } else {

            }
          //  System.out.println("Super Admin Redirect url :::::::::::: " + requestURI);

            HttpSession session = ((HttpServletRequest) request).getSession();
            session.setAttribute(ProjectConstants.SESSION_REDIRECT_SOURCE, requestURI);

            final String redirectUrl = (request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
                    + "/user/dashboard.xhtml");
           // System.out.println("Super Admin Filter Redirect ::::: " + redirectUrl);

            ((HttpServletResponse) response).sendRedirect(redirectUrl);
            // ((HttpServletResponse) response).sendRedirect(contextPath + "/signin.xhtml?url=" + URLEncoder.encode(requestURI));
        }
        chain.doFilter(request, response);
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void destroy() {

    }

}
